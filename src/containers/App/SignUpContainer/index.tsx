import { getUser } from '@commons/storage';
import SignUpComponent from '@components/SignUpComponent';
import { iconCheckCircleLight } from '@constants/imageAssets';
import { logInWithEmailAndPassword } from '@services/firebase';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';

const initialFormSignIn = {
    email: '',
    password: ''
};

const SignUpContainer = (props: any) => {
    useEffect(()=> {
        const useInfo = getUser();
        if(useInfo) {
            window.location.replace('/');
        }
    }, []);

    const onSubmit = (data: any)=> {
        logInWithEmailAndPassword(data);
    };

    const listPlan = [
        {icon: iconCheckCircleLight,
            title: 'Unlimited product uploads',
        },
        {icon: iconCheckCircleLight,
            title: 'Pro tips',
        },
        {icon: iconCheckCircleLight,
            title: 'Free forever',
        },
        {icon: iconCheckCircleLight,
            title: 'Full author options',
        },
    ];
    
    return (
        <SignUpComponent onSubmit={onSubmit}
            initialFormSignIn={initialFormSignIn}
            listPlan={listPlan}
            {...props}
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SignUpContainer);