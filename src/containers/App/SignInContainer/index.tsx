import { getUser } from '@commons/storage';
import SignInComponent from '@components/SignInComponent';
import { registerWithEmailAndPassword } from '@services/firebase';
import React, { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';

const initialFormSignIn = {
    email: '',
    password: '',
    name: '',
};

const SignInContainer = (props: any) => {

    useEffect(()=> {
        const useInfo = getUser();
        if(useInfo) {
            window.location.replace('/');
        }
    }, []);

    const onSubmit = useCallback((data: any)=> {
        registerWithEmailAndPassword(data);
    }, []);
    
    return (
        <SignInComponent onSubmit={onSubmit}
            initialFormSignIn={initialFormSignIn}
            {...props}
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SignInContainer);