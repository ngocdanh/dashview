import CustomerListComponent from '@components/CustomerListComponent';
import {
    messageNotification1,
    messageNotification2,
    messageNotification3,
    messageNotification4,
    messageNotification5
} from '@constants/imageAssets';
import { faker } from '@faker-js/faker';
import { customToastNotify } from '@helpers/ToastCustom';
import { createFakerList } from '@utils/index';
import _ from 'lodash';
import moment from 'moment';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { IinitialDefaultValueFilter } from './types';
const listAvatar = [
    messageNotification1,
    messageNotification2,
    messageNotification3,
    messageNotification4,
    messageNotification5,
    messageNotification2,
    messageNotification1,
    messageNotification5,
    messageNotification3,
    messageNotification4,
];

const initialDefaultValueFilter = {
    search: '',
    sort_by: 'Featured',
    price:[0,500],
    showing: [false, false, false, false, false]
};


const singleDataCustomer = ()=> {
    return {
        name: {
            fullName: faker.name.findName(),
            userName: `@${faker.internet.userName()}`,
            avatar: faker.helpers.arrayElement(listAvatar),
        },
        email: `${faker.internet.email()}`,
        purchase: `${_.random(1, 100, false)}`,
        lifetime: {
            quantity: _.round(_.random(50, 500, true), 0),
            mode: faker.datatype.boolean() ? 'down': 'up',
            numeral: `${_.round(_.random(10, 100, true), 1).toFixed(1)}%`,  
        },
        comments: `${_.random(1, 100, false)}`,
        likes:`${_.random(1, 100, false)}`,
        type:faker.helpers.arrayElements(['All Products','UI kit','Illustration','WireFrame kit','Icons'],_.random(1, 5,false)),
        timeCreateAt:faker.helpers.arrayElement([faker.date.future(),faker.date.past(),faker.date.recent()]),
    };
};


const CustomerListContainer = () => {

    const [tabActiveCustomer, setTabActiveCustomer] = useState<number>(0);
    const [searchCustomer, setSearchCustomer] = useState<string>('');
    const [openModalFilter, setOpenModalFilter] = useState<boolean>(false);
    const [progressBarPrice, setProgressBarPrice] = useState<number[]>([0, 500]);
    const [filters, setFilters] = useState<IinitialDefaultValueFilter>(initialDefaultValueFilter);

    const dataTableActiveCustomersFaker =  useMemo(()=> createFakerList(singleDataCustomer, 100), []);
    const dataTableNewCustomersFaker =  useMemo(()=> createFakerList(singleDataCustomer, 100), []);


    const [dataTableActiveCustomers, setDataTableActiveCustomers] = useState<any>(dataTableActiveCustomersFaker);
    const [dataTableNewCustomers, setDataTableNewCustomers] = useState<any>(dataTableNewCustomersFaker);
    const [selectedRow, setSelectedRow] = useState<any>();
    const debouncedSearchCustomer = _.debounce((value) => setSearchCustomer(value), 500);

    const handleChangeTabActiveCustomer = useCallback((index: number)=> {
        setTabActiveCustomer(index);
    }, [tabActiveCustomer]);

    const handleSearchCustomer = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            debouncedSearchCustomer(value);
        }
        else 
            setSearchCustomer('');
    };
    const handleSelectRows = (rows: any) => {
        setSelectedRow(rows);
    };
    const handleDeleteCustomers = (selectedRows:any) =>{
        if(_.size(selectedRows) > 0 && faker.datatype.boolean()){
            if(tabActiveCustomer==0){  
                customToastNotify('success','customers deleted 🙌');
                const customers = _.remove(dataTableActiveCustomers, (item:any)=> {
                    return selectedRows.indexOf(item) == -1;
                });  
                setDataTableActiveCustomers(customers);
            }else{
                customToastNotify('success','customers deleted 🙌');
                const customers = _.remove(dataTableNewCustomers, (item:any)=> {
                    return selectedRows.indexOf(item) == -1;
                });  
                setDataTableNewCustomers(customers);
            }
        }else{
            customToastNotify('error','customers can\'t deleted 🙌');
        }  
    };
    const handlePublishCustomers = (selectedRows:any) =>{
        if(_.size(selectedRows) > 0 && faker.datatype.boolean()){
            if(tabActiveCustomer==0){  
                customToastNotify('success','customers publish 🙌');
                const customers = _.remove(dataTableActiveCustomers, (item:any)=> {
                    return selectedRows.indexOf(item) == -1;
                });  
                setDataTableActiveCustomers(customers);
            }else{
                customToastNotify('success','customers publish 🙌');
                const customers = _.remove(dataTableNewCustomers, (item:any)=> {
                    return selectedRows.indexOf(item) == -1;
                });  
                setDataTableNewCustomers(customers);
            }
        }else{
            customToastNotify('error','customers can\'t publish 🙌');
        }  
    };



    const filterArrayData = (datas: any, search: string) => {
        const result = !_.isEmpty(datas) ?
            datas?.filter((item: { name: { fullName: string | string[]; }; email: string | string[];})=> {
                const fullName = item?.name?.fullName as string;
                const email = item?.email as string;
                return fullName?.toLowerCase()?.includes(search.trim().toLowerCase()) 
                || email?.toLowerCase()?.includes(search.trim().toLowerCase());
            })  : [];
        return result;
    };    

    useEffect(()=> {
        switch(tabActiveCustomer) {
            case 0: 
                // eslint-disable-next-line no-case-declarations
                const findActiveCustomers = filterArrayData(dataTableActiveCustomersFaker, searchCustomer);
                setDataTableActiveCustomers(findActiveCustomers);
                break;
            case 1: 
                // eslint-disable-next-line no-case-declarations
                const findNewCustomers = filterArrayData(dataTableNewCustomersFaker, searchCustomer);
                setDataTableNewCustomers(findNewCustomers);
                break;

        }
    }, [searchCustomer]);

    useEffect(()=> {
        const showingString= `${filters.showing[0] ? 'All Products':''}, ${filters.showing[1] ? 'UI kit': ''}, ${filters.showing[2] ? 'Illustration': ''}, ${filters.showing[3] ? 'WireFrame kit': ''}, ${filters.showing[4] ? 'Icons': ''}`;
        if(tabActiveCustomer==0){
        
            if(_.isEmpty(filters.search)){
                setDataTableActiveCustomers(dataTableActiveCustomersFaker);
            }

            if(filters.price[0] == 0&& filters.price[1] ==500){
                setDataTableActiveCustomers(dataTableActiveCustomersFaker);
            }
            if(_.isEmpty(showingString.replaceAll(',', '').trim())){
                setDataTableActiveCustomers(dataTableActiveCustomersFaker);
            }  
            if(filters.sortBy='New'){
                setDataTableActiveCustomers(dataTableActiveCustomersFaker);
            }
            if(filters.sortBy='Featured'){
                const ActiveCustomersByShort= _.orderBy(dataTableActiveCustomers, (item:any) => moment(item.timeCreateAt), ['desc']);
                setDataTableActiveCustomers(ActiveCustomersByShort);
            }
            if(filters.sortBy='Last'){
                const ActiveCustomersByShort= _.orderBy(dataTableActiveCustomers, (item:any) => moment(item.timeCreateAt), ['asc']);
                setDataTableActiveCustomers(ActiveCustomersByShort);
            }
            if(filters.price[0] != 0&& filters.price[1] !=500){
                const findActiveCustomersByprice =_.filter(dataTableActiveCustomers,(item:any)=>{
                    return item.lifetime.quantity >= filters.price[0] && item.lifetime.quantity<= filters.price[1];
                });
                setDataTableActiveCustomers(findActiveCustomersByprice);
            }

            if(!_.isEmpty(filters.search)){
                const findActiveCustomers = filterArrayData(dataTableActiveCustomers,filters.search);
                setDataTableActiveCustomers(findActiveCustomers);
            } 
            if(!_.isEmpty(showingString.replaceAll(',', '').trim())){
                const findActiveCustomersByShow =_.filter(dataTableActiveCustomers,(item:any)=>{
                    return _.join(item.type,' ').includes(`${filters.showing[0] ? 'All Products':'   '}`)
                || _.join(item.type,',').includes(`${filters.showing[1] ? 'UI kit':'   '}`)
                || _.join(item.type,',').includes(`${filters.showing[2] ? 'Illustration':'   '}`)
                || _.join(item.type,',').includes(`${filters.showing[3] ? 'WireFrame kit':'   '}`)
                || _.join(item.type,',').includes(`${filters.showing[4] ? 'Icons':'   '}`);
                });
                setDataTableActiveCustomers(findActiveCustomersByShow);
            }

        }else{

            if(filters.price[0] == 0&& filters.price[1] ==500){
                setDataTableNewCustomers(dataTableNewCustomersFaker);
            }
            if(_.isEmpty(showingString.replaceAll(',', '').trim())){
                setDataTableNewCustomers(dataTableNewCustomersFaker);
            }  
            if(filters.sortBy='New'){
                setDataTableNewCustomers(dataTableNewCustomersFaker);
            }
            if(filters.sortBy='Featured'){
                const ActiveCustomersByShort= _.orderBy(dataTableNewCustomers, (item:any) => moment(item.timeCreateAt), ['desc']);
                setDataTableNewCustomers(ActiveCustomersByShort);
            }
            if(filters.sortBy='Last'){
                const ActiveCustomersByShort= _.orderBy(dataTableNewCustomers, (item:any) => moment(item.timeCreateAt), ['asc']);
                setDataTableNewCustomers(ActiveCustomersByShort);
            }
            if(filters.price[0] != 0&& filters.price[1] !=500){
                const findActiveCustomersByprice =_.filter(dataTableNewCustomers,(item:any)=>{
                    return item.lifetime.quantity >= filters.price[0] && item.lifetime.quantity<= filters.price[1];
                });
                setDataTableNewCustomers(findActiveCustomersByprice);
            }

            if(!_.isEmpty(filters.search)){
                const findActiveCustomers = filterArrayData(dataTableNewCustomers,filters.search);
                setDataTableNewCustomers(findActiveCustomers);
            } 
            if(!_.isEmpty(showingString.replaceAll(',', '').trim())){
                const findActiveCustomersByShow =_.filter(dataTableNewCustomers,(item:any)=>{
                    return _.join(item.type,' ').includes(`${filters.showing[0] ? 'All Products':'   '}`)
              || _.join(item.type,',').includes(`${filters.showing[1] ? 'UI kit':'   '}`)
              || _.join(item.type,',').includes(`${filters.showing[2] ? 'Illustration':'   '}`)
              || _.join(item.type,',').includes(`${filters.showing[3] ? 'WireFrame kit':'   '}`)
              || _.join(item.type,',').includes(`${filters.showing[4] ? 'Icons':'   '}`);
                });
                setDataTableNewCustomers(findActiveCustomersByShow);
            }
        }
          
    },[filters]);
    const handleOpenModalFilter = () => {
        setOpenModalFilter(true);
    };

    const handleCloseModalFilter = () => {
        setOpenModalFilter(false);
    };

    const handleChangeProgressBarPrice = (event: Event, newValue:number[]) => {
        setProgressBarPrice(newValue);
    };

    const handleResetProgressBarPrice = () => {
        setProgressBarPrice([30, 50]);
    };

    const handleChangeFilters = (data: IinitialDefaultValueFilter) => {
        setFilters(data);
    };

    return (
        <CustomerListComponent title='Customer List'
            tabActiveCustomer={tabActiveCustomer}
            handleChangeTabActiveCustomer={handleChangeTabActiveCustomer}
            dataTableActiveCustomers={dataTableActiveCustomers}
            dataTableNewCustomers={dataTableNewCustomers}
            handleSearchCustomer={handleSearchCustomer}
            searchCustomer={searchCustomer}
            selectedRow={selectedRow}
            handleDeleteCustomers={handleDeleteCustomers}
            handlePublishCustomers={handlePublishCustomers}
            handleSelectRows={handleSelectRows}
            titlePage='Customer List'
            openModalFilter={openModalFilter}
            handleOpenModalFilter={handleOpenModalFilter}
            handleCloseModalFilter={handleCloseModalFilter}
            initialDefaultValueFilter={initialDefaultValueFilter}
            handleChangeProgressBarPrice ={handleChangeProgressBarPrice}
            progressBarPrice={progressBarPrice}
            handleResetProgressBarPrice={handleResetProgressBarPrice}
            filters={filters}
            handleChangeFilters={handleChangeFilters}
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerListContainer);