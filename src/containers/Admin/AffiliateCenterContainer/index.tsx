import AffiliateCenterComponent from '@components/AffiliateCenterComponent';

import { COLORS } from '@constants/colors';
import { iconActivityFilled, iconShoppingBag } from '@constants/imageAssets';
import faker from '@faker-js/faker';
import _ from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';


const overviewDumydata =()=>
    [
        {
            title:'Clicks',
            icon:iconShoppingBag,
            quantity:`${_.round(_.random(1000, 3000, true))}`,
            numeral: `${_.round(_.random(10, 100, true), 1)}%`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            color:COLORS.violetLight,
        },
        {
            title:'Payouts',
            icon:iconActivityFilled,
            quantity:`${_.round(_.random(1000, 3000, true))}K`,
            numeral: `${_.round(_.random(10, 100, true), 1)}%`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            color:COLORS.blueLight,
        }
    ];


const AffiliateCenterContainer = () => {

    const [fillterOverviewSelect,setFillterOverviewSelect] = useState<string>('All time');
    const dataOverViewFaker = useMemo(()=> overviewDumydata,[fillterOverviewSelect] );
    const [dataOverView, setDataOverView] = useState<any>(dataOverViewFaker);
    const [fillterProductViewsSelect,setFillterProductViewsSelect] = useState<string>('Last 7 days');
    const [selectTabOverView, setSelectTabOverView] = useState(1);

    const handleChangeSelectTabOverview = (value: number) => {
        setSelectTabOverView(value);
    };

    const handleSelectProductViews = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterProductViewsSelect(value);
    };
    const handleSelectOverview = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterOverviewSelect(value);
    };

    useEffect(() => {
        setDataOverView(dataOverViewFaker);
    },[fillterOverviewSelect]);
    return (
        <AffiliateCenterComponent title='Affiliate center'
            titlePage={'Affiliate center'}
            dataOverView={dataOverView}
            fillterOverviewSelect={fillterOverviewSelect}
            handleChangeSelectTabOverview={handleChangeSelectTabOverview}
            fillterProductViewsSelect={fillterProductViewsSelect}
            handleSelectProductViews={handleSelectProductViews}
            handleSelectOverview={handleSelectOverview}
            selectTabOverView={selectTabOverView}

        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AffiliateCenterContainer);