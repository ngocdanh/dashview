import CreateComponent from '@components/CreateComponent';
import { yupResolver } from '@hookform/resolvers/yup';
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { connect } from 'react-redux';
import * as yup from 'yup';

const keyFeatureSchema = yup.object({
    feature: yup.string().required()
});

const tagSchema = yup.object({
    tag: yup.string()
});

const schema = yup.object({
    description: yup.string().max(100),
    productTitle: yup.string().max(100).required(),
    keyFeature: yup.array().of(keyFeatureSchema).required(),
    coverImage: yup.string().required(),
    purchase: yup.string().optional(),
    price: yup.object({
        amount: yup.number().required().positive().integer(),
        minimumAmount: yup.number().required().positive().integer(),
        suggestedAmount: yup.number().required().positive().integer(),
    }).required(),
    category: yup.string().optional(),
    compatibility: yup.array().required(),
    tags: yup.array().of(tagSchema).optional(),
    discussion: yup.string().max(100).required(),
});

const CreateContainer = () => {
    const methods = useForm({
        resolver: yupResolver(schema)
    });

    // const methods = useForm();
    const { register, handleSubmit } = methods;
    return (
        <FormProvider {...methods}>
            <form
                id='form-create'
                onSubmit={handleSubmit((data) => console.log(data))}>
                <CreateComponent titlePage='New product'/>
            </form>
        </FormProvider>
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CreateContainer);