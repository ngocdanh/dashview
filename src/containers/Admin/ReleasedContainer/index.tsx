import ReleasedComponent from '@components/ReleasedComponent';
import { COLORS } from '@constants/colors';
import { imageProduct1,
    imageProduct10,
    imageProduct2,
    imageProduct3, 
    imageProduct4, 
    imageProduct5,
    imageProduct6,
    imageProduct7,
    imageProduct8, 
    imageProduct9 } from '@constants/imageAssets';
import faker from '@faker-js/faker';
import { createFakerList } from '@utils/index';
import _ from 'lodash';
import moment from 'moment';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

import {
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9
} from '@constants/imageAssets';
import { customToastNotify } from '@helpers/ToastCustom';
const listImageProduct = [
    imageProduct1,
    imageProduct2,
    imageProduct3,
    imageProduct4,
    imageProduct5,
    imageProduct6,
    imageProduct7,
    imageProduct8,
    imageProduct9,
    imageProduct10,
];
const listImage = [
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9,
];
const listColor = [COLORS.greenLight, COLORS.violetLight, COLORS.blueLight, COLORS.yellowLight];

const singleDataProduct = () =>{
    return {
        id:uuidv4(),
        products: {
            title: faker.lorem.sentence(4),
            link:faker.internet.url(),
            previewsm:faker.helpers.arrayElement(listImageProduct),
            previewxl:faker.helpers.arrayElement(listImage),
        },
        price: `${_.round(_.random(0, 100, true), 0)}`,
        status: faker.datatype.boolean() ? 'Active': 'Deactive',
        rating: `${_.random(3,5,true).toFixed(1)}`,
        totalRating: `${_.random(0, 100)}`,
        time: `${moment(faker.date.past()).format('MMM D, YYYY')} at ${moment(faker.date.past()).format('h:mm A')}`,
        sales:{
            quantity: `$${_.round(_.random(100,800,true)).toFixed(0)}`,
            mode: faker.datatype.boolean() ? 'up':'down',
            numeral: `${_.round(_.random(10,100,true),2).toFixed(2)} %`,
        },
        views:{
            quantily: `${_.round(_.random(10,50,true)).toFixed(0)}k`,
            width: _.random(10,50,false),
        },
        color: faker.helpers.arrayElement(listColor),
        
    };
};
const ReleasedContainer = () => {
    const dataTableProductFaker =  useMemo(()=> createFakerList(singleDataProduct, 100), []);
    const [dataTableProducts, setDataTableProducts] = useState<any>(dataTableProductFaker);
    const [isLoadingMore, setIsLoadingMore] = useState<boolean>(false);
    const [tabActiveProduct, setTabActiveProduct] = useState<number>(0);
    const [searchProduct, setSearchProduct] = useState<string>('');
    const [pagination, setPagination] = useState<number>(1);
    const [selectedRow, setSelectedRow] = useState<any>();
    const debouncedSearchProduct = _.debounce((value) => setSearchProduct(value), 500);
  
    const [openModalProductDetail, setOpenModalProductDetail] = useState<boolean>(false);
    const handleOpenModalProductDetail = () => {
        setOpenModalProductDetail(true);
    };
    const handleCloseModalProductDetail = () => {
        setOpenModalProductDetail(false);
    };

    const handleSearchProduct = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            debouncedSearchProduct(value);
        }
        else 
            setSearchProduct('');
    };

    const handleChangeTabActiveProduct = useCallback((index: number)=> {
        setTabActiveProduct(index);
    }, [tabActiveProduct]);

    const handleDeleteProducts = (selectedRow:any) =>{
        if(_.size(selectedRow) > 0 && faker.datatype.boolean()){
            customToastNotify('success','Products deleted 🙌');
            const Products = _.remove(dataTableProducts, (item:any)=> {
                return selectedRow.indexOf(item) == -1;
            });  
            setDataTableProducts(Products);
        }else{
            customToastNotify('error','Products can\'t deleted 🙌');
        }  
    };
    const handlePublishProducts = (selectedRow:any) =>{
        if(_.size(selectedRow) > 0 && faker.datatype.boolean()){
            customToastNotify('success','Products publish complete 🙌');
            const Products = _.remove(dataTableProducts, (item:any)=> {
                return selectedRow.indexOf(item) == -1;
            });  
            setDataTableProducts(Products);
        }else{
            customToastNotify('error','Products can\'t publish 🙌');
        }  
    };
    const handleChangePaginationProducts = async () => {
        await setIsLoadingMore(true);
        await setTimeout(()=> {
            setIsLoadingMore(false);
            setPagination((value)=> value + 1);
        }, 800);
    };
    const handleSelectRows = (rows: any) => {
        setSelectedRow(rows);
    };

    const filterArrayData = (datas: any, search: string) => {
        const result = !_.isEmpty(datas) ?
            datas?.filter((item: { products: { title: string | string[]; }; })=> {
                const title = item?.products?.title as string;
                return title?.toLowerCase()?.includes(search.trim().toLowerCase());
            })  : [];
        return result;
    };
    

    useEffect(()=> {
        switch(tabActiveProduct) {
            case 0: 
                // eslint-disable-next-line no-case-declarations
                const findProducts = filterArrayData(dataTableProductFaker, searchProduct);
                setDataTableProducts(findProducts);
                break;
            case 1: 
                // eslint-disable-next-line no-case-declarations
                const findProduct = filterArrayData(dataTableProductFaker , searchProduct);
                setDataTableProducts(findProduct);
                break;
        }
    }, [searchProduct]);

    return (
        <ReleasedComponent title='Released Page'
            dataTableProducts={dataTableProducts}
            handleChangeTabActiveProduct={handleChangeTabActiveProduct}
            handleSearchProduct={handleSearchProduct}
            handleChangePaginationProducts={handleChangePaginationProducts}
            handleSelectRows={handleSelectRows}
            handleDeleteProducts={handleDeleteProducts}
            handlePublishProducts={handlePublishProducts}
            searchProduct={searchProduct}
            selectedRow={selectedRow}
            pagination={pagination}
            tabActiveProduct={tabActiveProduct}
            isLoadingMore={isLoadingMore}
            openModalProductDetail={openModalProductDetail}
            handleOpenModalProductDetail={handleOpenModalProductDetail}
            handleCloseModalProductDetail={handleCloseModalProductDetail}
            titlePage={'Released'} />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ReleasedContainer);