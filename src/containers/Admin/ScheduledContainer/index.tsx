import ScheduledComponent from '@components/ScheduledComponent';
import { imageProduct1, imageProduct10, imageProduct2, imageProduct3, imageProduct4, imageProduct5, imageProduct6, imageProduct7, imageProduct8, imageProduct9 } from '@constants/imageAssets';
import faker from '@faker-js/faker';
import { customToastNotify } from '@helpers/ToastCustom';
import { createFakerList } from '@utils/index';
import _ from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';

const listImageProduct = [
    imageProduct1,
    imageProduct2,
    imageProduct3,
    imageProduct4,
    imageProduct5,
    imageProduct6,
    imageProduct7,
    imageProduct8,
    imageProduct9,
    imageProduct10,
];
const singleDataProduct = () =>{

    return{
        products:{ 
            title: faker.lorem.sentence(4),
            link: faker.internet.url(),
            previewsm:faker.helpers.arrayElement(listImageProduct),
        },
        price: `${_.round(_.random(0, 100, true), 0)}`,
        scheduledFor: faker.date.past(),
    };
};
const ScheduledContainer = () => {
    const [searchProduct, setSearchProduct] = useState<string>('');
    const dataTableProductFaker =  useMemo(()=> createFakerList(singleDataProduct, 100), []);
    const [dataTableProducts, setDataTableProducts] = useState<any>(dataTableProductFaker);
    const debouncedSearchProduct = _.debounce((value) => setSearchProduct(value), 500);
    const [selectedRow, setSelectedRow] = useState<any>();
    const [dateSelected, setDateSelected] = useState<any>(new Date());
    const [selectedItem, setSelectedItem] = useState<any>();
    const [openModalRechedule, setOpenModalRechedule] = useState<boolean>(false);
    const [openModalSelectDate, setOpenModalSelectDate] = useState<boolean>(false);
    const [openModalSelectTime, setOpenModalSelectTime] = useState<boolean>(false);
    const [openModalProductDetail, setOpenModalProductDetail] = useState<boolean>(false);
    const handleOpenModalProductDetail = () => {
        setOpenModalProductDetail(true);
    };
    const handleCloseModalProductDetail = () => {
        setOpenModalProductDetail(false);
    };
    
    const handleSearchProduct = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            debouncedSearchProduct(value);
        }
        else 
            setSearchProduct('');
    };
    const handleOpenModalRechedule = () => {
        setOpenModalRechedule(true);
    };

    const handleCloseModalRechedule = () => {
        setOpenModalRechedule(false);
    };
    const handleOpenSelectDate = () => {
        setOpenModalSelectDate(true);
    };

    const handleCloseSelectDate = () => {
        setOpenModalSelectDate(false);
    };
    const handleOpenSelectTime = () => {
        setOpenModalSelectTime(true);
    };

    const handleCloseSelectTime = () => {
        setOpenModalSelectTime(false);
    };
    const handleDeleteProducts = (selectedRows:any) =>{
        if(_.size(selectedRows) > 0 && faker.datatype.boolean()){
            customToastNotify('success','Products deleted 🙌');
            const Products = _.remove(dataTableProducts, (item:any)=> {
                return selectedRows.indexOf(item) == -1;
            });  
            setDataTableProducts(Products);
         
        }else{
            customToastNotify('error','Products can\'t deleted 🙌');
        }  
    };


    const handlePublishProducts = (selectedRow:any) =>{
        if(_.size(selectedRow) > 0 && faker.datatype.boolean()){
            customToastNotify('success','Products publish complete 🙌');
            const Products = _.remove(dataTableProducts, (item:any)=> {
                return selectedRow.indexOf(item) == -1;
            });  
            setDataTableProducts(Products);
        }else{
            customToastNotify('error','Products can\'t publish 🙌');
        }  
    };

    const handleSelectRows = (rows: any) => {
        setSelectedRow(rows);
    };
    const handleSetDate = (date: any) => {
        setDateSelected(date);
    };

    const handleSelectedItem = (item: any) => {
        setSelectedItem(item);
        handleSetDate(item.scheduledFor);
    };

    const handlReschedule = () =>{
        if(selectedItem.lastEdited != dateSelected){
            const listData =_.map(dataTableProducts,(item: any) =>{
                if(item.id == selectedItem.id ){
                    item.scheduledFor = dateSelected;
                    return item;
                }else{
                    return item;
                }
            });
            setDataTableProducts(listData);
            customToastNotify('success','Products change date complete 🙌');
        }
    };
    const filterArrayData = (datas: any, search: string) => {
        const result = !_.isEmpty(datas) ?
            datas?.filter((item: { products: { title: string | string[]; }; })=> {
                const title = item.products.title as string;
                return title?.toLowerCase()?.includes(search.trim().toLowerCase());
            })  : [];
        return result;
    };  
    useEffect(()=> {
        const findProducts = filterArrayData(dataTableProductFaker, searchProduct);
        setDataTableProducts(findProducts);
    }, [searchProduct]);
    return (
        <ScheduledComponent
            dataTableProducts={dataTableProducts}
            handleSearchProduct={handleSearchProduct}
            searchProduct={searchProduct}
            title='Scheduled Page'
            handleSelectRows={handleSelectRows}
            handleDeleteProducts={handleDeleteProducts}
            handlePublishProducts={handlePublishProducts}
            handleOpenModalRechedule={handleOpenModalRechedule}
            handleCloseModalRechedule={handleCloseModalRechedule}
            handleSelectedItem={handleSelectedItem}
            handleOpenSelectDate={handleOpenSelectDate}
            handleCloseSelectDate={handleCloseSelectDate}
            handleOpenSelectTime={handleOpenSelectTime}
            handleCloseSelectTime={handleCloseSelectTime}
            handleSetDate={handleSetDate}
            handlReschedule={handlReschedule}
            selectedRow={selectedRow}
            selectedItem={selectedItem}
            dateSelected={dateSelected}
            openModalRechedule={openModalRechedule}
            openModalSelectDate={openModalSelectDate}
            openModalSelectTime={openModalSelectTime}
            openModalProductDetail={openModalProductDetail}
            handleOpenModalProductDetail={handleOpenModalProductDetail}
            handleCloseModalProductDetail={handleCloseModalProductDetail}
            titlePage='Scheduled' />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ScheduledContainer);