import OverviewComponent from '@components/OverviewComponent';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { imageOverviewUser1,imageOverviewUser2, imageOverviewUser3 } from '@constants/imageAssets';

const OverviewContainer = () => {
    const overviewUserDummyData = [
        {
            id: 1,
            url: imageOverviewUser1,
            name: 'Gladyce',
        },
        {
            id: 2,
            url: imageOverviewUser2,
            name: 'Elbel',
        },
        {
            id: 3,
            url: imageOverviewUser3,
            name: 'Joyce',
        },
    ];
    const [fillterTotalCustomersSelect,setFillterTotalCustomersSelect] = useState<string>('Last 28 days');
    const [fillterTrafficChannelSelect,setFillterTrafficChannelSelect] = useState<string>('Last 7 days');
    const [fillterActiveCustomersSelect,setFillterActiveCustomersSelect] = useState<string>('Last 7 days');
    const [openModalProductDetail, setOpenModalProductDetail] = useState<boolean>(false);
    const handleOpenModalProductDetail = () => {
        setOpenModalProductDetail(true);
    };
    const handleCloseModalProductDetail = () => {
        setOpenModalProductDetail(false);
    };
    
    const handleSelectTotalCustomers = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterTotalCustomersSelect(value);
    };
 
    const handleSelectActiveCustomers = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterActiveCustomersSelect(value);
    };
    const handleSelectTrafficChannel = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterTrafficChannelSelect(value);
    };
    return (
        <OverviewComponent title='Overview Page'
            titlePage='Customers'
            fillterTotalCustomersSelect={fillterTotalCustomersSelect}
            fillterTrafficChannelSelect={fillterTrafficChannelSelect}
            fillterActiveCustomersSelect={fillterActiveCustomersSelect}
            handleSelectTotalCustomers={handleSelectTotalCustomers}
            handleSelectTrafficChannel={handleSelectTrafficChannel}
            handleSelectActiveCustomers={handleSelectActiveCustomers}
            overviewUserDummyData={overviewUserDummyData}
            openModalProductDetail={openModalProductDetail}
            handleOpenModalProductDetail={handleOpenModalProductDetail}
            handleCloseModalProductDetail={handleCloseModalProductDetail}
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(OverviewContainer);