import { getUser } from '@commons/storage';
import ShopComponent from '@components/ShopComponent';
import {
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9
} from '@constants/imageAssets';
import faker from '@faker-js/faker';
import { customToastNotify } from '@helpers/ToastCustom';
import { createFakerList } from '@utils/index';
import _ from 'lodash';
import moment from 'moment';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

const listImage = [
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9,
];

const listUser = [
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,
];

const singleDataShop = ()=> {
    return {
        id:uuidv4(),
        title: faker.lorem.sentence(4),
        price: _.random(20, 500),
        rating: `${_.random(3, 5, true).toFixed(1)}`,
        totalRating: `${_.random(20, 100)}`,
        preview: faker.helpers.arrayElement(listImage),
        url: '',
        type:faker.helpers.arrayElements(['All Products','UI kit','Illustration','WireFrame kit','Icons'],_.random(1, 5,false)),
        timeCreateAt:faker.helpers.arrayElement([faker.date.future(),faker.date.past(),faker.date.recent()]),
    };
};

const singleDataFollower = ()=> {
    return {
        id:uuidv4(),
        name: faker.name.findName(),
        totalProduct: _.random(1, 30),
        isMessage: faker.datatype.boolean(),
        isFollowing: faker.datatype.boolean(),
        totalFollower: _.random(100, 1000),
        preview: faker.helpers.arrayElement(listUser),
        listProduct: faker.helpers.arrayElements(listImage,3),
    };
};
const singleDataFollowing = ()=> {
    return {
        id:uuidv4(),
        name: faker.name.findName(),
        totalProduct: _.random(1, 30),
        isMessage: faker.datatype.boolean(),
        isFollowing: true,
        totalFollower: _.random(100, 1000),
        preview: faker.helpers.arrayElement(listUser),
        listProduct: faker.helpers.arrayElements(listImage,3),
    };
};

const ShopContainer = () => {
    const [fillterShop,setFillterShop]=useState<string>('mostRecent');
    const [fillterFollowing,setFillterFollowing]=useState<string>('mostRecent');
    const [fillterFollower,setFillterFollower]=useState<string>('mostRecent');
    const dataListShopFaker = useMemo(()=> createFakerList(singleDataShop, 100), [fillterShop]);
    const dataListFollowerFaker = useMemo(()=> createFakerList(singleDataFollower, 100), [fillterFollower]);
    const dataListFollowingFaker = useMemo(()=> createFakerList(singleDataFollowing, 100), [fillterFollowing]);
    const [dataListShop, setDataListShop]= useState(dataListShopFaker);
    const [dataListFollower, setDataListFollower] =useState(dataListFollowerFaker);
    const [dataListFollowing, setDataListFollowing] = useState(dataListFollowingFaker);

    const [tabActive, setTabActive] = useState<number>(0);
    const [pagination, setPagination] = useState<number>(1);
    const [isLoadingMore, setIsLoadingMore] = useState<boolean>(false);
    const [openModalProductDetail, setOpenModalProductDetail] = useState<boolean>(false);
    const handleOpenModalProductDetail = () => {
        setOpenModalProductDetail(true);
    };
    const handleCloseModalProductDetail = () => {
        setOpenModalProductDetail(false);
    };
    
    const initialDefaultValueFilter = {
        search: '',
        sort_by: 'Featured',
        price:[0,500],
        showing: [false, false, false, false, false]
    };
    const userInfo = getUser();
    const [openModalFilter, setOpenModalFilter] = useState<boolean>(false);
    const [progressBarPrice, setProgressBarPrice] = useState<number[]>([0, 500]);
    const [searchCustomer, setSearchCustomer] = useState<string>('');
    const [filters, setFilters] = useState<IinitialDefaultValueFilter>(initialDefaultValueFilter);
    const debouncedSearchCustomer = _.debounce((value) => setSearchCustomer(value), 500);
    const handleSearchShop = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            debouncedSearchCustomer(value);
        }
        else 
            setSearchCustomer('');
    };
    const handleChangeTabActive = (index: number)=> {
        setTabActive(index);
        setPagination(1);
    };

    const handleChangePaginationProducts = async () => {
        await setIsLoadingMore(true);
        await setTimeout(()=> {
            setIsLoadingMore(false);
            setPagination((value)=> value + 1);
        }, 800);
    };
    const handleSelectFillterShop = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        if(tabActive==0){
            setFillterShop(value);
        }
        if(tabActive==1){
            setFillterFollower(value);
        }
        if(tabActive==2){
            setFillterFollowing(value);
        }

    };
    const filterArrayData = (datas: any, search: string) => {
        const result = !_.isEmpty(datas) ?
            datas?.filter((item: { title:  string | string[]; })=> {
                const fullName = item?.title as string;
                return fullName?.toLowerCase()?.includes(search.trim().toLowerCase()); 
            })  : [];
        return result;
    };   
    
    const handleDeleteProducts =(id:string)=> {
        if(id && faker.datatype.boolean()){

            const newListProducts = _.remove(dataListShop, (item:any)=> {
                return item.id !=id;
            });  
            setDataListShop(newListProducts);
            customToastNotify('success','Products deleted 🙌');
        }else{
            customToastNotify('error','Products can\'t deleted 🙌');
        } 
    };
    const handleChangeStatusFLowClick =(id:string)=>{
        if(tabActive==1){
            const newListFollower=_.map(dataListFollower,(item:any)=>{
                item.id==id ? item.isFollowing=!item.isFollowing:item.isFollowing;

                return item;
            } );
            setDataListFollower(newListFollower);
        }
        if(tabActive==2){
            const newListFollowing=_.map(dataListFollowing,(item:any)=>{
                item.id==id ? item.isFollowing=!item.isFollowing:item.isFollowing;
                return item;
            } );
            setDataListFollowing(newListFollowing);
        }
    };
    useEffect(() => {
        setDataListShop(dataListShopFaker);
    },[fillterShop]);
    useEffect(() => {
        setDataListFollower(dataListFollowerFaker);
    },[fillterFollower]);
    useEffect(() => {
        setDataListFollowing(dataListFollowingFaker);
    },[fillterFollowing]);

    useEffect(()=> {
        const showingString= `${filters.showing[0] ? 'All Products':''}, ${filters.showing[1] ? 'UI kit': ''}, ${filters.showing[2] ? 'Illustration': ''}, ${filters.showing[3] ? 'WireFrame kit': ''}, ${filters.showing[4] ? 'Icons': ''}`;
        if(tabActive==0){
        
            if(_.isEmpty(filters.search)){
                setDataListShop(dataListShopFaker);
            }

            if(filters.price[0] == 0&& filters.price[1] ==500){
                setDataListShop(dataListShopFaker);
            }
            if(_.isEmpty(showingString.replaceAll(',', '').trim())){
                setDataListShop(dataListShopFaker);
            }  
            if(filters.sortBy='New'){
                setDataListShop(dataListShopFaker);
            }
            if(filters.sortBy='Featured'){
                const ActiveCustomersByShort= _.orderBy(dataListShop, (item:any) => moment(item.timeCreateAt), ['desc']);
                setDataListShop(ActiveCustomersByShort);
            }
            if(filters.sortBy='Last'){
                const ActiveCustomersByShort= _.orderBy(dataListShop, (item:any) => moment(item.timeCreateAt), ['asc']);
                setDataListShop(ActiveCustomersByShort);
            }
            if(filters.price[0] != 0|| filters.price[1] !=500){
                const findActiveCustomersByprice =_.filter(dataListShop,(item:any)=>{
                    return item.price >= filters.price[0] && item.price<= filters.price[1];
                });
                setDataListShop(findActiveCustomersByprice);
            }

            if(!_.isEmpty(filters.search)){
                const findActiveCustomers = filterArrayData(dataListShop,filters.search);
                setDataListShop(findActiveCustomers);
            } 
            if(!_.isEmpty(showingString.replaceAll(',', '').trim())){
                const findActiveCustomersByShow =_.filter(dataListShop,(item:any)=>{
                    return _.join(item.type,' ').includes(`${filters.showing[0] ? 'All Products':'   '}`)
                || _.join(item.type,',').includes(`${filters.showing[1] ? 'UI kit':'   '}`)
                || _.join(item.type,',').includes(`${filters.showing[2] ? 'Illustration':'   '}`)
                || _.join(item.type,',').includes(`${filters.showing[3] ? 'WireFrame kit':'   '}`)
                || _.join(item.type,',').includes(`${filters.showing[4] ? 'Icons':'   '}`);
                });
                setDataListShop(findActiveCustomersByShow);
            }
        }
    },[filters]);
    const handleOpenModalFilter = () => {
        setOpenModalFilter(true);
    };

    const handleCloseModalFilter = () => {
        setOpenModalFilter(false);
    };

    const handleChangeProgressBarPrice = (event: Event, newValue:number[]) => {
        setProgressBarPrice(newValue);
    };

    const handleResetProgressBarPrice = () => {
        setProgressBarPrice([30, 50]);
    };

    const handleChangeFilters = (data: IinitialDefaultValueFilter) => {
        setFilters(data);
    };
    return (
        <ShopComponent title='Shop Page'
            dataListShop={dataListShop}
            dataListFollower={dataListFollower}
            dataListFollowing={dataListFollowing}
            tabActive={tabActive}
            userInfo={userInfo}
            pagination={pagination}
            fillterShop={fillterShop}
            handleSelectFillterShop={handleSelectFillterShop}
            handleChangePaginationProducts={handleChangePaginationProducts}
            handleChangeStatusFLowClick={handleChangeStatusFLowClick}
            isLoadingMore={isLoadingMore}
            handleDeleteProducts={handleDeleteProducts}
            handleChangeTabActive={handleChangeTabActive}
            titlePage={'Shop'}
            openModalFilter={openModalFilter}
            handleOpenModalFilter={handleOpenModalFilter}
            handleCloseModalFilter={handleCloseModalFilter}
            initialDefaultValueFilter={initialDefaultValueFilter}
            handleChangeProgressBarPrice ={handleChangeProgressBarPrice}
            progressBarPrice={progressBarPrice}
            handleResetProgressBarPrice={handleResetProgressBarPrice}
            filters={filters}
            handleChangeFilters={handleChangeFilters}
            handleSearchShop={handleSearchShop}
            openModalProductDetail={openModalProductDetail}
            handleOpenModalProductDetail={handleOpenModalProductDetail}
            handleCloseModalProductDetail={handleCloseModalProductDetail}
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ShopContainer);