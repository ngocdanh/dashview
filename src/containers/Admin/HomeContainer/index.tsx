import HomeComponent from '@components/HomeComponent';
import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import imageOverviewUser1 from '@assets/images/img_overview_user_1.png';
import imageOverviewUser2 from '@assets/images/img_overview_user_2.png';
import imageOverviewUser3 from '@assets/images/img_overview_user_3.png';
import { iconActivityFilled, iconShoppingBag } from '@constants/imageAssets';
import _ from 'lodash';
import faker from '@faker-js/faker';
import { COLORS } from '@constants/colors';

const overviewDumydata =()=>
    [
        {
            title:'Customers',
            icon:iconShoppingBag,
            quantity:`${_.round(_.random(1000, 3000, true))}`,
            numeral: `${_.round(_.random(10, 100, true), 1)}%`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            color:COLORS.violetLight,
        },
        {
            title:'Income',
            icon:iconActivityFilled,
            quantity:`${_.round(_.random(1000, 3000, true))}K`,
            numeral: `${_.round(_.random(10, 100, true), 1)}%`,
            mode: faker.datatype.boolean() ? 'down': 'up',
            color:COLORS.blueLight,
        }
    ];
const listImageUser = [
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,
    imageOverviewUser1,
    imageOverviewUser2,
    imageOverviewUser3,

];
const overviewUserList =()=> [
    {
        id: 1,
        url: faker.helpers.arrayElement(listImageUser),
        name: faker.helpers.arrayElement(['Gladyce','Elbel','Joyce']),
    },
    {
        id: 2,
        url: faker.helpers.arrayElement(listImageUser),
        name: faker.helpers.arrayElement(['Gladyce','Elbel','Joyce']),
    },
    {
        id: 3,
        url: faker.helpers.arrayElement(listImageUser),
        name: faker.helpers.arrayElement(['Gladyce','Elbel','Joyce']),
    },

];
const HomeContainer = () => {
    const dataOverViewFaker = useMemo(()=> overviewDumydata,[fillterOverviewSelect] );
    const [dataOverView, setDataOverView] = useState<any>(dataOverViewFaker);
    const [fillterOverviewSelect,setFillterOverviewSelect] = useState<string>('All time');
    const [fillterProductViewsSelect,setFillterProductViewsSelect] = useState<string>('Last 7 days');
    const overviewUserListFaker = useMemo(()=> overviewUserList,[fillterOverviewSelect] );
    const [selectTabOverView, setSelectTabOverView] = useState(1);
    const [overviewUserDummyData,setOverviewUserDummyData] = useState<any>(overviewUserListFaker);
    const [openModalVideo,setOpenModalVideo] = useState<boolean>(false);

    const handleChangeSelectTabOverview = (value: number) => {
        setSelectTabOverView(value);
    };

    const handleSelectProductViews = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterProductViewsSelect(value);
    };
    const handleSelectOverview = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setFillterOverviewSelect(value);
    };
    const handleCloseModalVideo =() =>{
        setOpenModalVideo(false);
    };
    const handleOpenModalVideo =() =>{
        setOpenModalVideo(true);
    };
    useEffect(() => {
        setDataOverView(dataOverViewFaker);
        // setDataChartFaker(listChartDummyData);
        setOverviewUserDummyData(overviewUserListFaker);
    },[fillterOverviewSelect]);

    return (
        <HomeComponent title='Home page'
            titlePage={'Dashboard'}
            dataOverView={dataOverView}
            fillterOverviewSelect={fillterOverviewSelect}
            handleChangeSelectTabOverview={handleChangeSelectTabOverview}
            fillterProductViewsSelect={fillterProductViewsSelect}
            handleSelectProductViews={handleSelectProductViews}
            handleSelectOverview={handleSelectOverview}
            selectTabOverView={selectTabOverView}
            openModalVideo={openModalVideo}
            handleCloseModalVideo={handleCloseModalVideo}
            handleOpenModalVideo={handleOpenModalVideo}
            overviewUserDummyData={overviewUserDummyData}
            
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);