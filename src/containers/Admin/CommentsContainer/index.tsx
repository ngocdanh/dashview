import CommentsComponent from '@components/CommentsComponent';
// eslint-disable-next-line max-len
import { imageOverviewUser1, imageOverviewUser2, imageOverviewUser3, imageProduct1, imageProduct2, imageProduct3, imageProduct4, imageProduct5, imageProduct6 } from '@constants/imageAssets';
import faker from '@faker-js/faker';
import { customToastNotify } from '@helpers/ToastCustom';
import { createFakerList } from '@utils/index';
import _ from 'lodash';
import * as React from 'react';
import { connect } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import { IProductComment } from './types';


const listImageProduct = [
    imageProduct1,imageProduct2,imageProduct3,
    imageProduct4,imageProduct5,imageProduct6
];

const listImageUser = [
    imageOverviewUser1,imageOverviewUser2,imageOverviewUser3
];

const SingleProductComment = () =>{
    const currentProductComment:IProductComment = {
        id: uuidv4(),
        status: false,
        comment: {
            image: faker.helpers.arrayElement(listImageUser),
            name: faker.lorem.sentence(2),
            comment: faker.lorem.sentence(3),
            time: `${faker.datatype.number({
                'min':1,
                'max':24
            })} h`
        },
        product: {
            image: faker.helpers.arrayElement(listImageProduct),
            title: faker.lorem.sentence(2),
            type: faker.lorem.sentence(2)
        }
    };

    return currentProductComment;
};


const CommentsContainer = () => {
    const [keyword,setKeyword] = React.useState<string>('');
    const [selections, setSelections] = React.useState<IProductComment[]>([]);

    // const typingTimeoutRef = React.useRef<null | ReturnType<typeof setTimeout>>(null);

    const debouncedSearchComment = _.debounce((value) => setKeyword(value),500);

    const dataTable = React.useMemo<IProductComment[]>(()=>createFakerList(SingleProductComment,20),[]);


    const [dataTablePostFaker, setDataTablePostFaker] = React.useState<IProductComment[]>([...dataTable]);

    const handleSearchComment = (e: React.ChangeEvent<HTMLInputElement>) => {
        if(_.isObject(e)){
            const value = e.target.value;
            debouncedSearchComment(value);
        }else{
            setKeyword('');
        }
    };

    const onHandleSearchCommentSubmit = React.useCallback((e : React.ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        const value = e.target.value.trim();
        if(value !== ''){
            const filterValue =
            dataTablePostFaker.filter(data => data.product.title.toLowerCase().includes(value.toLowerCase()));
            setDataTablePostFaker(filterValue);
            setKeyword('');
        }else return ;
    },[]);

    React.useEffect(()=>{
        if(keyword.trim() !== ''){
            const filterValue =
            dataTable.filter(data => data.comment.comment.toLowerCase().includes(keyword.toLowerCase()));
            setDataTablePostFaker(filterValue);
        }else {
            setDataTablePostFaker(dataTable);
        }

    },[keyword]);


    const onDeleteCommentsHandle = (comments :any) => {
        if(!_.isEmpty(dataTablePostFaker)) {
            const filterData:IProductComment[] =
            dataTablePostFaker.filter((data)=> (!comments.type ? comments : selections).indexOf(data) === -1 );
            customToastNotify('success','Delete comment successfully!');
            setDataTablePostFaker(filterData);
        }
    };


    const onHandleRowSelection = (rows:IProductComment[]) => {
        setSelections(rows);
    };



    return (
        <CommentsComponent title='Comment Page'
            dataTablePost = {dataTablePostFaker}
            keyword = {keyword}
            setKeyword = {setKeyword}
            selections = {selections}
            onChangeHandleSearchComment = {handleSearchComment}
            handlerSearchComment = {onHandleSearchCommentSubmit}
            handlerDeleteComment = {onDeleteCommentsHandle}
            handlerRowSelection = {onHandleRowSelection}
            titlePage={'Comments'} />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CommentsContainer);