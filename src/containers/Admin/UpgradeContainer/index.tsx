import UpgradeComponent from '@components/UpgradeComponent';
import { COLORS } from '@constants/colors';

import { toastOption } from '@helpers/ToastCustom';
import React, { useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import { dataChoose, dataQuestions } from './dataFaker';

const UpgradeContainer = () => {
    const defaultValueCollapse = new Array(dataQuestions.length).fill(false);
    const [tabActive, setTabActive] = useState(0);
    const [collapseQuestion, setCollapseQuestion] = useState(defaultValueCollapse);

    const handleChangeTabActive = (idx: number) => {
        setTabActive(idx);
        setCollapseQuestion(defaultValueCollapse);
    };
    const handleChoosePlanLite = () => {
        // ok
    };
    const handleChoosePlanPro = () => {
        toast.success('Update Success', toastOption);
    };

    const handleChangeCollapseQuestion = (idx: number)=> {
        const collapseQuestionTemp =[...collapseQuestion];
        collapseQuestionTemp[idx] = !collapseQuestionTemp[idx];
        setCollapseQuestion(collapseQuestionTemp);
    };

    return (
        <UpgradeComponent
            titlePage='Upgrade to Pro'
            handleChangeCollapseQuestion={handleChangeCollapseQuestion}
            dataChoose={dataChoose}
            handleChoosePlanLite={handleChoosePlanLite}
            dataQuestions={dataQuestions}
            handleChoosePlanPro={handleChoosePlanPro}
            handleChangeTabActive={handleChangeTabActive}
            tabActive={tabActive}
            collapseQuestion={collapseQuestion}
        />
    );
};

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(UpgradeContainer);