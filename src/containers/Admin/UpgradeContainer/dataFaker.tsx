import { COLORS } from '@constants/colors';

export const dataChoose = [
    {
        name: 'Lite',
        colorTitle: COLORS.blueLight,
        description: 'Basic shop and tools to set up your profile',
        saleYear: '8%',
        saleYearTitle: 'of the monthly income you earn on the market',
        listOptionPlan: [
            'Basic shop profile',
            'Customer communication tools',
            '100 promotion posts',
            'Maximum 50 product uploads',
        ],
        isChoose: true,
    },
    {
        name: 'Pro',
        colorTitle: COLORS.violet,
        description: 'Pro shop and tools to set up your profile 🔥',
        saleYear: '12%',
        saleYearTitle: 'of the monthly income you earn on the market',
        listOptionPlan: [
            'Extended shop profile',
            'Customer communication tools',
            'Unlimited promotion posts',
            'Unlimited product uploads',
            'Special offers promo tool',
            'Analytics and insights',
            'Bulk message to all customers',
        ],
        isChoose: false,
    },
];

export   const dataQuestions = [
    {
        name: 'Get started',
        items: [
            {
                title: 'How to upgrade to Pro account?',
                // eslint-disable-next-line max-len
                content: 'Enjoy instant access to our vast library of 5,121 premium products and all upcoming new releases with super-fast download speeds powered by Amazon S3. Yes, you read that right. Getting $127,035 in value means you\'re saving more than 99% on all products making it the sweetest deal for premium design assets around.'
            },
            {
                title: 'I forgot my password',
                // eslint-disable-next-line max-len
                content: 'If you forgot your password or your password isn\'t being recognized, you\'ll need to reset it using the main email address on your account.\n You can reset your password on our Forgotten Password page.\n Enter the email address you used to sign up for Vimeo, and wait up to two minutes for the password reset email to be delivered. Clicking the link in that email will allow you to choose a new password on your account.'
            },
            {
                title: 'I can\' reset my password', 
                // eslint-disable-next-line max-len
                content: 'Please check that you clicked the right link in your password reset email. Consider clicking the link from an incognito window. If you are still having trouble, try clearing your browser cache and cookies, and try again.'
            },
            {
                title: 'How do I create a secure password?',
                // eslint-disable-next-line max-len
                content: 'Creating a secure, unique password for each of your online accounts is very important. If a scammer gets even one password, they can begin to access your other accounts. We suggest changing your password periodically.'
            },
            {
                title: 'How do I change and reset my password',
                // eslint-disable-next-line max-len
                content: 'If you have trouble with the above, get a hold of a staff member at your school. They can send you a reset password email (or a reset text number email) from your profile. In some cases, your school may be using a different system to manage user logins. If this is the case, the reset methods above won\'t work and a staff member will have to use the other system to reset your password.'
            },
        ]
    },
    {
        name: 'Managing customers',
        items: [
            {
                title: 'How do I update a customer\'s credit card?',
                // eslint-disable-next-line max-len
                content: 'You can change only the expiration date. If you need to change something else, you must add a new payment method. See Adding payment methods.'
            },
            {
                title: 'How do I find a charge that a customer says is wrong?',
                // eslint-disable-next-line max-len
                content: 'In Subscriber Management, select the bill that\'s in dispute, and then display the charge. See Displaying event information.'
            },
            {
                title: 'How do I close a customer\'s account on a future date?',
                // eslint-disable-next-line max-len
                content: 'You can change the status of an account in advance. In Subscriber Management, open the account and select Actions, Account, and then Account Status. You can then defer the action to a future date and manage the deferred action with a job in Business Operations. See Changing account status and Running deferred actions jobs.'
            },
            {
                title: 'How do I delete an account?',
                // eslint-disable-next-line max-len
                content: 'You can\'t delete accounts. You can rename accounts to make them easier to identify in searches, or you can update the account\'s status to closed to terminate its services.'
            },
            {
                title: 'How do I permanently terminate a customer\'s services?',
                // eslint-disable-next-line max-len
                content: 'You can permanently terminate all services for a customer\'s account by setting the account status to closed. See Changing account status'
            },
        ]
    },
    {
        name: 'Billing & payments ',
        items: [
            {
                // eslint-disable-next-line max-len
                title: 'How do I receive credit card payments?',
                content: 'Enjoy instant access to our vast library of                 // esli5,121 premium products and all upcoming new releases with super-fast download speeds powered by Amazon S3. Yes, you read that right. Getting $127,035 in value means you\'re saving more than 99% on all products making it the sweetest deal for premium design assets around.'
            },
            {
                title: 'How do I set credit limits for customers?',
                // eslint-disable-next-line max-len
                content: 'You configure credit limits when you create packages in Offer Design. When a customer purchases a package, the credit limits defined in the package apply to the customer\'s services. See Creating packages.'
            },
            {
                title: 'Can a bill carry forward a past due amount from the previous bill?',
                // eslint-disable-next-line max-len
                content: 'Yes. Oracle Monetization Cloud uses two accounting types: balance forward and open item. With balance forward accounting, a customer\'s bill includes all the charges that a customer owes, including those from previous billing cycles. With open item accounting, a customer is billed only for charges from the bill items in the current bill cycle. See About accounting types.'
            },
            {
                title: 'Can I bill a customer immediately?',
                // eslint-disable-next-line max-len
                content: 'Yes. You can bill a single customer at any time. To do so, use Bill Now in Subscriber Management. See Using bill now to create a bill.'
            },
            {
                title: 'Can one customer pay for another customer\'s bill?',
                // eslint-disable-next-line max-len
                content: 'Yes. This is called a billing hierarchy. When creating or editing a bill unit in Subscriber Management, you can select another account instead of a payment method. See Configuring a customer\'s billing settings and Managing billing hierarchies.'
            },
        ]
    },
    {
        name: 'My benefits',
        items: [
            {
                // eslint-disable-next-line max-len
                title: 'What if I am out on disability or need to go out on disability? How would this work?',
                // eslint-disable-next-line max-len
                content: 'If you are already out on disability, your benefits will continue through Liberty Mutual as long as they are approved by the Plan. If you need to file a new disability claim, contact the Benefits Office and request a Disability Packet to begin the disability process.'
            },
            {
                title: 'If I am out on a leave without pay, how do I pay my benefits premium?',
                // eslint-disable-next-line max-len
                content: 'Payments, payable to "UC Regents," should be sent to the normal location unless other instructions are announced: Business Services - Insurance Section'
            },
            {
                title: 'Can a bill carry forward a past due amount from the previous bill?',
                content: 'Yes. Oracle Monetization Cloud uses two accounting types: balance forward and open item. With balance forward accounting, a customer\'s bill includes all the charges that a customer owes, including those from previous billing cycles. With open item accounting, a customer is billed only for charges from the bill items in the current bill cycle. See About accounting types.'
            },
            {
                title: 'Will I continue receiving benefits?',
                // eslint-disable-next-line max-len
                content: 'Your benefits will continue as long as your appointment makes you eligible to receive them. New employees must enroll in the benefits package that matches their appointment status, and their benefits will be effective as of the date of hire.'
            },
            {
                title: 'How do I receive medical services?',
                // eslint-disable-next-line max-len
                content: 'Continue to use your medical plan as usual. If you need to pay out of pocket to receive service, please keep your receipts and contact your medical plan directly to file a claim for reimbursement.'
            },
        ]
    },
];