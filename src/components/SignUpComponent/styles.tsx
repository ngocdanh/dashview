import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {
        minHeight: '100vh',
    },
    pageLeft: {
        backgroundColor: COLORS.grey2,
        display: 'flex',
        alignItems: 'center',
        position: 'relative',
    },
    contentPageLeft: {
        '& .img-plan': {
            width: 128,
            objectFit: 'contain'
        },
        '& .title': {
            textAlign: 'center',
            padding: '48px 0',
            '@media(max-width: 480px)': {
                padding: '16px 0',
            }
        },
        '& .list-plan': {
            '& .item': {
                paddingBottom: 20,
            },
            '& .icon-svg': {
                marginRight: 12,
            },
            '& .text': {
                
            }
        }
    },
    pageRight: {
        backgroundColor: COLORS.white,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    notificationSignIn: {
        position: 'absolute',
        top: 20,
        right: 20,
        zIndex: 20,
    },
    content: {
        width: 300,
        '@media(max-width: 480px)': {
            width: '100%',
            padding: 30,
        },
        '& .divider': {
            width: '100%',
            height: 2,
            backgroundColor: COLORS.grey3
        },
        '& .title': {
            padding: '32px 0',
            '@media(max-width: 480px)': {
                fontSize: 34,
                padding: '16px 0'
            },
        },
        '& .title-signIn-social': {
        },
        '& .box-social': {
            padding: '32px 0',
        },
        '& .button-social': {
            '& .icon': {
                width: 24,
                height: 24,
                marginRight: 4,
            },
            '& .text': {

            }
        },
        '& .box-email': {
            '& .button-custom':{
                width: '100%',
            },
            '& .input': {
                marginBottom: 12
            },
            paddingTop: 32,
        },
        '& .pv-32': {
            paddingTop: 32,
            paddingBottom: 32,
            '@media(max-width: 480px)': {
                padding: '20px 0'
            },
        },
        '& .pt-32': {
            paddingTop: 32,
            '@media(max-width: 480px)': {
                paddingTop: 32
            },
        }
    },
    styleLogo: {
        width: 48,
        height: 48,
        position: 'absolute',
        top: 20,
        left: 20,
    },
});

export default styles;