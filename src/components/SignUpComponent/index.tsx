import { COLORS } from '@constants/colors';
import { iconFacebookPng, iconGooglePng, iconLockLight, iconMailLight, imgSignUpPlan, logo } from '@constants/imageAssets';
import ButtonCustom from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import Input from '@helpers/Input';
import Text from '@helpers/Text';
import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { signInWithFacebook, signInWithGoogle } from '@services/firebase';
import _ from 'lodash';
import React from 'react';
import { useForm } from 'react-hook-form';
import * as Yup from 'yup';
import styles from './styles';

interface SignUpProps {
    onSubmit: (data: any)=> void,
    initialFormSignIn: {
        email: string,
        password: string,
    },
    listPlan: Array<{
        title: string,
        icon: string,
    }>
}

const schema = Yup
    .object()
    .shape({
        email: Yup.string().email('Must be a valid email').min(4).max(255).required('Email is required'),
        password: Yup.string().required('No password provided.') 
            .matches(
                /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
                'Password must contain at least 8 characters, one uppercase, one number and one special case character'
            ),
    })
    .required();

function SignUpComponent(props: SignUpProps  & WithStyles<typeof styles>) {
    const {classes, onSubmit, initialFormSignIn, listPlan} = props;
    const { register,
        setValue,
        watch,
        handleSubmit, 
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        mode: 'onChange',
        defaultValues: initialFormSignIn,
        resolver: yupResolver(schema),
    });

    return (
        <Grid container
            className={classes.root}
            display={'flex'}>
            <Grid item
                sm={12}
                lg={4}
                display={{xs: 'none', md: 'flex'}}
                alignItems='center'
                justifyContent='center'
                className={classes.pageLeft}>
                <Box className={classes.contentPageLeft}>
                    <Box display='flex'
                        component='div'
                        justifyContent={'center'}>
                        <img src={imgSignUpPlan}
                            className='img-plan'
                            alt="" />
                    </Box>
                    <Text color={COLORS.grey6}
                        className='title'
                        h4>Plan includes</Text>
                    <Box className="list-plan">
                        {!_.isEmpty(listPlan) && listPlan.map((item, index)=> (
                            <Box display='flex'
                                key={index}
                                alignItems={'center'}
                                className="item">
                                <IconSVG icon={item.icon}
                                    fill={COLORS.green} />
                                <Text base2
                                    color={COLORS.grey4}>{item.title}</Text>
                            </Box>
                        ))}
                        
                    </Box>
                </Box>
                <img src={logo}
                    className={classes.styleLogo}
                    alt="" />
            </Grid>
            <Grid item
                sm={12}
                lg={8}
                className={classes.pageRight}>
                <Box className={classes.content}>
                    
                    <Text h2
                        color={COLORS.grey7}
                        className='title'>Sign Up</Text>
                    <Text bodySB2
                        color={COLORS.grey7}
                        className='title-signIn-social'>Sign up with Open accounts</Text>
                    <Grid container
                        spacing={2}
                        className='box-social'>
                        <Grid item
                            lg={6}
                            sm={12}>
                            <ButtonTransparent isElement
                                onClick={signInWithGoogle}
                                className='button-social'>
                                <Box display={'flex'}>
                                    <img src={iconGooglePng}
                                        alt=""
                                        className='icon' />
                                    <Text color={COLORS.grey7}
                                        button1>Google</Text>
                                </Box>
                            </ButtonTransparent >
                        </Grid>
                        <Grid item
                            lg={6}
                            sm={12}>
                            <ButtonTransparent isElement
                                onClick={signInWithFacebook}
                                className='button-social'>
                                <Box display={'flex'}>
                                    <img src={iconFacebookPng}
                                        alt=""
                                        className='icon' />
                                    <Text color={COLORS.grey7}
                                        button1>Facebook</Text>
                                </Box>
                            </ButtonTransparent >
                        </Grid>
                    </Grid>
                    <Box className='divider'/>
                    <Text bodySB2
                        color={COLORS.grey7}
                        className='title-signIn-social pt-32'>Or continue with email address</Text>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Box className='box-email'>
                            <Input type='email'
                                icon={iconMailLight}
                                placeholder='Your email'
                                messangeError={errors?.email?.message}
                                {...register('email', { required: true })}
                            />
                            <Input type="password"
                                icon={iconLockLight}
                                placeholder='Password'
                                autoComplete='on'
                                messangeError={errors?.password?.message}
                                {...register('password', { required: true })}
                            />
                            <ButtonCustom type={'submit'}
                                // disabled={!isValid}
                                disabled={!isDirty || !isValid}
                                color='primary'>Continue</ButtonCustom>
                        </Box>
                    </form>
                    <Text bodySB2
                        color={COLORS.grey4}
                        className='pv-32'>This site is protected by reCAPTCHA and the Google Privacy Policy.</Text>
                   
                </Box>
                <Box className={classes.notificationSignIn}>
                    <Text bodySB2
                        color={COLORS.grey4}
                        className='pv-32'>Already a member?  <a style={{color: COLORS.grey7}}
                            href='/sign-in'>Sign in</a></Text>
                </Box>
            </Grid>
        </Grid>
    );
}

export default withStyles(styles)(SignUpComponent);