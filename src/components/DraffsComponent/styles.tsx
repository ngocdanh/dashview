import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {
    
    },
    ProductPage:{
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: '24px 24px 2px',
        marginBottom: 8,
        width: '100%',
        transition: 'all 0.5s ease-in-out 0s',
        '&.margin-more':{
            marginBottom:'80px',
        },
        '& .products-tabs': {
            '& .button-tab': {
                height: 40,
                borderRadius: '4px',
                padding: '8px !important',
                backgroundColor: 'transparent',
                color: COLORS.grey4,
                transition: 'all .3s ease',
                '& .text': {
                    color: `${COLORS.grey4} !important`,
                },
                '&.active': {
                    color: COLORS.grey7,
                    backgroundColor: COLORS.white,
                    boxShadow:`0px 4px 8px -4px ${COLORS.shades40}`,
                    '& .icon-svg': {
                        color: `${COLORS.grey7} !important`,
                    }
                }
            },

        },
        '& .table-products':{
            marginTop: 32,

            '& .products-title:hover': {
                color:`${COLORS.blue} !important`,
            },
            '& .products-image':{
                marginRight: 20,
            },
            '& .box-icon':{
                opacity: 0,
                visibility: 'hidden',
                gap: 'calc(34px - 16px)',
                transition: 'all .3s ease',
                '& .icon':{
                    transition: 'all .3s ease',
                    '&:hover':{
                        backgroundColor: COLORS.white,
                        fill: COLORS.blue,
                    },
                    '&:hover .icon-svg': {
                        color: COLORS.blue,
                    },
                }
            },
            '& tbody > tr:hover .box-icon':{
                opacity: 1,
                visibility: 'visible',
            }
        },
        '& .tab-gridviewproduct':{
           
            borderRadius:'16px',
            overflow: 'hidden',
            padding: '32px 0',
            '& .product-item':{
                padding: '16px',
                '& .product-image':{
                    position: 'relative',
                    width: '100%',
                    overflow: 'hidden',
                    borderRadius: '12px',
                    '& img':{
                        width: '100%',
                        height: '100%',
                        objectFit: 'contain',

                    },
                    '&::before': {
                        content: '""',
                        top: 0,
                        left: 0,
                        width: '100%',
                        height: '100%',
                        position: 'absolute',
                        borderRadius:'16px',
                        backgroundColor: 'transparent',
                        transition: 'all .3s ease',
                        zIndex: -1,
                    },
                    '&:hover::before': {
                        zIndex:1,
                        backgroundColor: 'rgba(70, 70, 70, 0.38)',
                    },
                },
                '& .product-content':{
                    marginTop:8,
                    alignItems: 'flex-start'
                },
            }
        }

    },
    loadMore: {
        display: 'table',
        margin: ' 0 auto 24px',
    },
    MessageComponent:{
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: '24px 24px 20px',
        margin: '32px 0 -40px',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        transition: 'all 0.5s ease-in-out 0s',
        position: 'fixed',
        bottom: '-6%',
        maxWidth: 1200,
        opacity: 0,
        visibility: 'hidden',
        boxShadow: `0px 0px 5px 1px ${COLORS.shades95}`,
        '& .content':{
            display: 'flex',
            alignItems: 'center',
        },
        '& .social-button':{
            marginRight:'16px',
            '&:hover':{
                backgroundColor: COLORS.red,
                border:`2px solid ${COLORS.red}`,
            },
            '&:hover .button-text':{
                color: `${COLORS.white} !important`,  
            },
            '&:hover .icon-svg':{
                color: `${COLORS.white} !important`,

            }
        },
        '&.active':{
            bottom: '4%',
            opacity: 1,
            visibility: 'visible',
        },

      
    },
    contentModal: {
        borderTop: `1px solid ${COLORS.grey3}`,
        '& .modal-party': {
            width: 128,
            height: 128,
            borderRadius: '50%',
            backgroundColor: COLORS.greenLight,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
      
        },
        '& .button-modal':{
            marginTop:8,
            justifyContent: 'flex-start',
            transition: 'border .3s ease 0s',
            '&:hover': {
                borderColor:COLORS.blue,
            },
            '& .icon':{
                marginRight:'12px'
            },
            '&.active-button':{
                borderColor:COLORS.blue,
                '& .icon-svg': {
                    color: COLORS.grey7,
                }
            }
        },
        '& .box-content-text': {
            textAlign: 'center',
            marginTop: 20,
    
        },
        '& .box-calendar':{
            marginTop: 12,
        },
        '& .box-button-custom': {
            marginTop: 20,
        }
    }
});

export default styles;