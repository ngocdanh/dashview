import { COLORS } from '@constants/colors';
import { iconActions } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, Icon } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import styles from '../styles';
import { imagePreview,imageOverviewUser1 } from '@constants/imageAssets';
import Number from '@helpers/Number';
import { useFormContext } from 'react-hook-form';
interface INameAndDescriptionProps{
    title:string,

}


const Preview = (props:INameAndDescriptionProps & WithStyles<typeof styles>) => {
    const { title , classes } = props;
    const { watch } = useFormContext();
    const productTitle = watch('productTitle');
    const price = watch('price.amount');
    const coverImage = watch('coverImage');
    const [preview, setPreview] = React.useState<any>();

    const coverImageRef = React.useRef<any>();

    React.useEffect(() => {
        if (!coverImage) {
            setPreview(undefined);
            return;
        }
        const objectUrl = URL.createObjectURL(new Blob([coverImage[0]]));
        setPreview(objectUrl);

        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl);
    }, [coverImage]);


    return (
        <Box className={classes.overReview}
            style = {{padding: '24px'}}>
            <Box className={classes.header}>
                <Box className={classes.subTitle}
                >
                    <Box className='overReviewBlockColor blueLight'></Box>
                    <Box style={{width:'100%',display:'flex',justifyContent:'space-between',alignItems:'center'}}>
                        <Text titleSB1
                            color={COLORS.grey7}>{title}</Text>
                    </Box>
                    <IconSVG icon={iconActions}
                        width={'40px'}
                        height={'40px'}/>

                </Box>
            </Box>
            <Box>
                <img onError={({ currentTarget }) => {
                    currentTarget.onerror = null; // prevents looping
                    currentTarget.src=imagePreview;
                }}
                src={preview ? preview : imagePreview}
                alt="Preview image"
                ref = {coverImageRef} />
                <Box>
                    <Box style={{display:'flex',gap:'16px',justifyContent:'space-between'}}>
                        <Text titleSB1
                            color={COLORS.grey7}
                            style = {{maxWidth:'50%',overflowWrap: 'break-word'}}>{!productTitle ?
                                'Your product name' : productTitle}</Text>
                        <Number background={COLORS.greenLight}
                            style={{alignSelf:'flex-start'}}>
                            <Text base1
                                color={COLORS.grey7}
                                style={{padding:'4px 8px',overflowWrap: 'break-word'}}
                            >${!price ? '98' : price}</Text>
                        </Number>
                    </Box>

                    <Box style={{display:'flex',alignItems:'center',gap:'12px',marginTop:'8px'}}>
                        <img src={imageOverviewUser1}
                            alt=""
                            width={'32px'}
                            height={'32px'}/>
                        <Box style={{display:'flex',gap:'4px'}}>
                            <Text bodyM1
                                color={COLORS.grey4}
                            >by</Text>
                            <Text bodySB1
                                color={COLORS.grey7}
                                style={{marginLeft:'4px'}}>Hortense</Text>
                        </Box>
                    </Box>

                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(Preview);