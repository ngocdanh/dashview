import { COLORS } from '@constants/colors';
import CheckboxCustom from '@helpers/CheckboxCustom';
import ReactTag from '@helpers/ReactTag';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { select } from 'redux-saga/effects';
import { v4 as uuidv4 } from 'uuid';
import SectionTitle from '../SectionTitle';
import styles from '../styles';


interface INameAndDescriptionProps{
    title:string
}

const options = [
    {
        value: 'Select category',
        label: 'Select category',
    },
    {
        value: 'Category 1',
        label: 'Category 1',
    },
    {
        value: 'Category 2',
        label: 'Category 2',
    }
];

const checkboxs = [
    {
        id: uuidv4(),
        name:'Sketch'
    },
    {
        id: uuidv4(),
        name:'WordPress'
    },
    {
        id: uuidv4(),
        name:'Procreate'
    },
    {
        id: uuidv4(),
        name:'Figma'
    },
    {
        id: uuidv4(),
        name:'HTML'
    },
    {
        id: uuidv4(),
        name:'Illustrator'
    },
    {
        id: uuidv4(),
        name:'Adobe XD'
    },
    {
        id: uuidv4(),
        name:'Keynote'
    },
    {
        id: uuidv4(),
        name:'Framer'
    },
    {
        id: uuidv4(),
        name:'Photoshop'
    },
    {
        id: uuidv4(),
        name:'Maya'
    },
    {
        id: uuidv4(),
        name:'In Design'
    },
    {
        id: uuidv4(),
        name:'Cinema 4D'
    },
    {
        id: uuidv4(),
        name:'Blender'
    },
    {
        id: uuidv4(),
        name:'After Effect'
    },
];

const CategoryAndAttribute = (props:INameAndDescriptionProps & WithStyles<typeof styles>) => {
    const { title , classes } = props;

    const methods = useFormContext();
    const {setValue} = methods;
    const [tags, setTags] = React.useState([]);

    React.useEffect(()=>{
        setValue('tags',tags);
    },[tags]);

    return (
        <Box className={classes.overReview}
            style = {{padding: '24px'}}>
            <Box className={classes.header}>
                <Box className={classes.subTitle}
                >

                    <Box className='overReviewBlockColor violetLight'></Box>
                    <Box style={{width:'100%',display:'flex',justifyContent:'space-between'}}>
                        <Text titleSB1
                            color={COLORS.grey7}>{title}</Text>
                    </Box>

                </Box>
            </Box>
            <Box className={classes.nameAndDecription}
            >
                <SectionTitle title='Category'/>

                <Box>
                    <SelectCustom defaultValues='Select category'
                        {...methods.register('category')}
                        data={options}/>
                </Box>

                <SectionTitle title='Compatibility'/>
                <Box style={{display:'flex',flexWrap:'wrap',margin: '-4px -16px 32px 0',justifyContent:'space-around'}}>
                    {checkboxs.map((checkbox,index) =>
                        <Box key={checkbox.id}
                            style={{display:'flex',alignItems:'center',gap:'12px',
                                flex:' 0 0 calc(33.33% - 16px)',width:' calc(33.33% - 16px)' ,margin: '16px 16px 0 0'}}>
                            <CheckboxCustom value={checkbox.name}
                                {...methods.register(`compatibility.${index}`)}/>
                            <Text base2
                                color={COLORS.grey7}>{checkbox.name}</Text>
                        </Box>
                    )}
                </Box>
                <Box style={{display:'flex',justifyContent:'space-between',alignItems:'center'}}>
                    <SectionTitle title='Tag'/>
                    <Text caption1
                        style={{marginTottom: '14px',
                            marginTop: '34px'}}>{tags.length}/12 tags</Text>
                </Box>
                <Box >
                    <ReactTag tags = {tags}
                        setTags = {setTags}/>

                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(CategoryAndAttribute);