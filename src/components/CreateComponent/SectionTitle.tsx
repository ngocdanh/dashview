import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import React from 'react';
interface ISectionTitleProps{
    title:string
    fontSize?: string
}
const SectionTitle = (props: ISectionTitleProps) => {
    const {title, fontSize = 'big'} = props;
    return (
        <div>
            {fontSize === 'small' ?
                <Text caption2
                    color={COLORS.grey4}
                    style = {{marginBottom: '12px', marginTop:'16px'}} >{title}</Text> :
                <Text base2
                    color={COLORS.grey5}
                    style = {{marginBottom: '14px', marginTop:'34px'}} >{title}</Text>
            }

        </div>
    );
};

export default SectionTitle;