import { COLORS } from '@constants/colors';
import Input from '@helpers/Input';
import Text from '@helpers/Text';
import TextEditor from '@helpers/TextEditor';
import { Box, TextField } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import SectionTitle from '../SectionTitle';
import styles from '../styles';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import iconBackWard from '@assets/icons/arrow_backward/filled.svg';
import { useNavigate } from 'react-router-dom';
import { Controller, useFormContext } from 'react-hook-form';
interface INameAndDescriptionProps{
    title:string
}

const NameAndDescription = (props:INameAndDescriptionProps & WithStyles<typeof styles>) => {
    const { title , classes } = props;
    const [isHide, setIsHide] = React.useState<boolean>(false);
    const methods = useFormContext();
    const {register,control} = methods;
    const navigate = useNavigate();


    return (
        <Box className={classes.overReview}
            style = {{padding: '24px'}}>
            <Box className={classes.header}>
                <Box className={classes.subTitle}
                >

                    <Box className='overReviewBlockColor greenNgoc'></Box>
                    <Box style={{width:'100%',display:'flex',justifyContent:'space-between'}}>
                        <Text titleSB1
                            color={COLORS.grey7}>{title}</Text>

                        <ButtonTransparent style={{padding: '8px 16px !important'}}
                            onClick = {() => {
                                navigate(-1);
                            }}
                        >
                            <span style={{display:'flex',gap:'11px'}}>
                                <IconSVG icon={iconBackWard}/>
                                Back
                            </span>

                        </ButtonTransparent>
                    </Box>

                </Box>
            </Box>
            <Box className={classes.nameAndDecription}
            >
                <Box>
                    <SectionTitle title='Product title'/>
                    <Input disableIconSearch={true}
                        {...register('productTitle')}
                        style = {{width: '100%'}}/>
                </Box>
                <Box>
                    <SectionTitle title='Description'/>
                    <Controller
                        name="description"
                        control={control}
                        defaultValue=""
                        render={({ field }) => (
                            <TextEditor onChange={field.onChange}
                                value={field.value}
                                isHide={isHide} />
                        )}
                    />
                </Box>
                <Box>
                    <SectionTitle title='Key features'/>
                </Box>
                <Box className='KeyFeatures'
                    style={{display:'flex',gap:'12px',flexWrap:'wrap',justifyContent:'space-between'}}>
                    {[0,1,2,3].map((ele,index) => (
                        <Input placeholder='Value'
                            key={index}
                            disableIconSearch={true}
                            {...register(`keyFeature.${index}.feature`,{required:true})}
                            className="textValue"/>
                    ))}
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(NameAndDescription);