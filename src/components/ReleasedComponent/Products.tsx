import { COLORS } from '@constants/colors';
import { iconClock, iconGridLight, iconListFilled, iconStarFilled, iconStarLight } from '@constants/imageAssets';
import Button from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import Input from '@helpers/Input';
import LabelColor from '@helpers/LabelColor';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Grid, Hidden } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import { truncateString } from '@utils/index';
import clsx from 'clsx';
import _ from 'lodash';
import React, { useMemo } from 'react';
import { Column } from 'react-table';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';


interface ProductsProps {
    title: string,
    tabActiveProduct: number,
    dataTableProducts: any,
    handleChangeTabActiveProduct: (index: number) =>void,
    handleSearchProduct: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleChangePaginationProducts: ()=> void,
    handleSelectRows: (rows:any) => void,
    searchProduct: string,
    selectedRow: any,
    classes:any,
    pagination: number,
    isLoadingMore:boolean,
    handleOpenModalProductDetail?: () => void,
}

function ProductsComponent(props: ProductsProps & WithStyles<typeof styles>) {
    const {
        title, 
        classes, 
        tabActiveProduct, 
        handleChangeTabActiveProduct, 
        searchProduct, 
        pagination,  
        handleChangePaginationProducts, 
        isLoadingMore, 
        dataTableProducts, 
        handleSearchProduct,
        handleSelectRows,
        handleOpenModalProductDetail,
        selectedRow, 
        ...otherProps
    } = props;
    const tabs = [
        {

            icon: iconListFilled,
            indexTab: 0,
        },
        {
            icon: iconGridLight,
            indexTab: 1,
        },
    ];
    const disabled = pagination*6 > dataTableProducts?.length;
    const columnsProducts: Column[] = useMemo(()=> [
        {
            Header: 'Product',
            maxWidth: 300,
            minWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box display={'flex'}
                        onClick={handleOpenModalProductDetail}
                        alignItems={'center'}
                        sx={{cursor:'pointer'}}>
                        <Box className="products-image">
                            <img src={original.products.previewsm}
                                alt={original.products.title}
                                width={80}
                                height={80} />
                        </Box>
                        <Box className="products-content info"
                            sx={{maxWidth:180}}>
                            <Text color={COLORS.grey7}
                                className="products-title"
                                baseB1>
                                {truncateString(original.products.title,32)}
                            </Text>
                            <Text color={COLORS.grey4}
                                caption1>
                                {truncateString(original.products.link,25)}
                            </Text>
                        </Box>
                    </Box>
                );
            },
        },
        {
            Header: 'Price',
            accessor: 'price',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={`$${original.price}`}
                    bg={original.price > 0 ? COLORS.greenNgoc : COLORS.grey3} />);
            }
        },
        {
            Header: 'Status',
            accessor: 'status',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={original.status}
                    color={original.status === 'Active' ? COLORS.green : COLORS.red}
                    bg={original.status === 'Active'? COLORS.greenLight: COLORS.redLight}
                />);
            }
        },
        {
            Header: 'Rating',
            accessor: 'rating',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box display={'flex'}
                        alignItems={'center'}>
                        <IconSVG icon={ original.rating >0 ? iconStarFilled: iconStarLight}
                            fill={original.rating >0 ?COLORS.yellowLight:COLORS.grey4}/>
                        <Text base2
                            color={COLORS.grey7}>{original.rating >0 ?  original.rating: 'No ratings'}</Text>
                        {
                            original.rating >0 ?
                                (<Text base2
                                    color={COLORS.grey4}>({  original.totalRating})</Text>):('')}
                    </Box>
                );
            }
        },
        {
            Header: 'Sales',
            accessor: 'sales',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={original.sales.quantity}
                    mode={original.sales.mode}
                    numeral={original.sales.numeral}
                    bg={COLORS.grey3} />);
            }
        },
        {
            Header: 'Views',
            accessor: 'views',
            Cell: (values:any )=> {
                const original = values.cell.row.original;
                return (<Box display={'flex'}>
                    <LabelColor
                        bg={COLORS.grey3} 
                        title={original.views.quantily}
                        widthAfter={original.views.width}
                    />
                </Box>);
            }
        },
    ], []);
    const renderProductsGrid = (datas:any) =>{

        return(
            datas.slice(0, 6*pagination).map((data:any )=>(

                <Grid item
                    xs={12}
                    lg={4}
                    key={uuidv4()}>
                    <Box className="product-item"
                        onClick={handleOpenModalProductDetail}>
                        <Box className='product-image'>
                            <img src={data?.products?.previewxl}
                                alt="" />
                        </Box>
                        <Box className="product-content"
                            display='flex'
                            justifyContent={'space-between'}
                        >
                            <Box className="product-content-left"
                                sx={{alignItems:'center'}}>
                                
                                <Text baseSB1
                                    color={COLORS.grey7}
                                    className="product-title"
                                   
                                >
                                    {data?.products?.title}
                                </Text>
                                <Box sx={{ marginLeft:'-2px',display:'flex',alignItems:'center',paddingTop:'8px'}}>
                                    <IconSVG icon={iconClock}
                                        fill={COLORS.grey4}/>
                                    <Text base2
                                        sx={{marginLeft:'8px'}}
                                        color={COLORS.grey4}>{data.time}</Text>
                                </Box>
                            </Box>
                            <Box className="product-content-right">
                                <Box className="product-price">
                                    <LabelColor bg={COLORS.greenNgoc} 
                                        color={COLORS.grey5}
                                        title={data.price}/>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Grid>
            ))
        );
    };
    const renderTabProducts = () => {
        return (
            <TableCustom className='table-products'
                data={dataTableProducts}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                handleSelectRow={handleSelectRows}
                columns={columnsProducts}/>
        );
    };
    const renderTabGrid = () => {
        return (
            <Box>
                <Grid container
                    className="tab-gridviewproduct"
                    spacing={2}>
                    {renderProductsGrid(dataTableProducts)}


                </Grid>
                {!disabled && (
                    <Box className={classes.loadMore} >
                        <ButtonTransparent isLoading={isLoadingMore}
                            disabled={disabled}
                            onClick={handleChangePaginationProducts}>Load More</ButtonTransparent>
                    </Box>
                )}
            </Box>
        );
    };
    return (
        <Box className={classes.ProductsComponent + (_.size(selectedRow) > 0 ? ' margin-more' :'')}
            sx={{}}>
            <Box display={'flex'}
                justifyContent={'space-between'}
                className='box-title-element'>
                <Box className="content-left"
                    display={'flex'}
                    alignItems={'center'}>

                    <TitleElement bg={COLORS.violetLight}>
                        {title}
                    </TitleElement>
                    <Hidden only='xs'>
                        <Input placeholder='Search product'
                            style={{marginLeft:24}}
                            search={searchProduct}
                            onChange={handleSearchProduct}
                        />

                    </Hidden>
                </Box>
                <Box className="content-right products-tabs"
                    display={'flex'}>

                    {tabs.map((tab: {
                        icon: string,
                        indexTab: number
                    })=> (
                        <Button key={uuidv4()}
                            onClick={()=> {
                                handleChangeTabActiveProduct(tab.indexTab);
                            }}
                            className={clsx('button-tab', {
                                ['active']: tabActiveProduct === tab.indexTab,
                            })}
                            color={'transparent'}>
                            <IconSVG icon={tab.icon}
                                fill={COLORS.grey4}/>
                        </Button>
                    ))}
                </Box>
            </Box>
            {tabActiveProduct === 0 && (
                renderTabProducts()
            )}
            {tabActiveProduct === 1 && (
                renderTabGrid()
            )}
        </Box>
    );
}

export default withStyles(styles)(ProductsComponent);