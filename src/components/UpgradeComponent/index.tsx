import Page from '@helpers/Page';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import ChoosePlan from './ChoosePlan';
import Question from './Question';
import styles from './styles';

interface UpgradeProps {
    handleChoosePlanLite: () => void;
    handleChoosePlanPro: () => void;
    titlePage: string;
    dataChoose: Array<{
        name: string,
        description: string,
        saleYear: string,
        saleYearTitle: string,
        colorTitle: string,
        listOptionPlan: Array<string>,
        isChoose: boolean,
    }>,
    dataQuestions: Array<{
        name: string,
        items: Array<{
            title: string,
            content: string,
        }>
    }>,
    handleChangeTabActive: (idx: number)=> void,
    tabActive: number,
    collapseQuestion: Array<boolean>,
    handleChangeCollapseQuestion: (idx: number)=> void,
}


function UpgradeComponent(props: UpgradeProps & WithStyles<typeof styles>) {
    const { classes,
        titlePage,
        ...otherProps} = props;


    return (
        <Page
            titlePage={titlePage}
            {...otherProps}>
            <ChoosePlan {...otherProps}/>
            <Question {...otherProps}/>
        </Page>
    );
}

export default withStyles(styles)(UpgradeComponent);