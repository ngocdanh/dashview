import { COLORS } from '@constants/colors';
import { iconCheckLight } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Divider, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import _ from 'lodash';
import React from 'react';
import styles from './styles';
import ButtonCustom from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';

interface IItemPlan {
    data: {
        name: string,
        colorTitle: string,
        description: string,
        saleYear: string,
        saleYearTitle: string,
        listOptionPlan: Array<string>,
        isChoose: boolean,
    }
}

interface IChoosePlan {
    dataChoose: Array<{
        name: string,
        colorTitle: string,
        description: string,
        saleYear: string,
        saleYearTitle: string,
        listOptionPlan: Array<string>,
        isChoose: boolean,
    }>,
    handleChoosePlanLite: () => void;
    handleChoosePlanPro: () => void;
}

function ChoosePlan(props:IChoosePlan &  WithStyles<typeof styles>) {
    const {classes,
        dataChoose, 
        handleChoosePlanLite,
        handleChoosePlanPro,
    } = props;

    const RenderPlan = (props: IItemPlan) => {
        const {data} = props;
        return (<Grid item 
            lg={6}
            xs={12}
            className='box-plan'>
            <Box className='content-plan'>
                <TitleElement
                    color={COLORS.grey7}
                    bg={data.colorTitle}>{data.name}</TitleElement>
                <Text className='py-16'
                    color={COLORS.grey4}
                    bodySB1>{data.description}</Text>
                <Divider/>
                <Box display='flex'
                    alignItems='center'
                    paddingY={3}>
                    <Text className='sale-plan'
                        color={COLORS.grey7}>{data.saleYear}</Text>
                    <Text color={COLORS.grey4}
                        caption1>{data.saleYearTitle}</Text>
                </Box>
                <Divider/>
                <Box paddingY={3}>
                    {!_.isEmpty(data.listOptionPlan) && data.listOptionPlan.map((i, idx)=> (
                        <Box marginBottom={3}
                            key={idx}
                            display='flex'
                            alignItems={'center'}>
                            <IconSVG className='mr-16'
                                icon={iconCheckLight}
                                width={38}
                                height={38}
                                fill={COLORS.green}/>
                            <Text baseSB1
                                color={COLORS.grey5}>{i}</Text>
                        </Box>
                    ))}
                </Box>
                {data.isChoose ? (
                    <ButtonTransparent onClick={handleChoosePlanLite}
                        className='button-choose'>Your plan</ButtonTransparent>
                ): (
                    <ButtonCustom  className='button-choose' 
                        onClick={handleChoosePlanPro}
                        color='primary'>Upgrade now</ButtonCustom>
                )}
            </Box>
        </Grid>);
    };

    return (
        <Box className={classes.choosePlan}>
            <Grid className='content'
                container
                spacing={20}>
                {
                    !_.isEmpty(dataChoose) && dataChoose.map((item, index)=> (
                        <RenderPlan key={index}
                            data={item}/>
                    )) 
                }
            </Grid>
        </Box>

    );
}

export default withStyles(styles)(ChoosePlan);