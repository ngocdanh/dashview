import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from '@constants/colors';
import { maxHeight } from '@mui/system';
import { Theme } from '@mui/material';

const styles = (theme: Theme) => createStyles({
    root: {
    },
    choosePlan: {
        backgroundColor: COLORS.white,
        margin: '24px 0',
        padding: 48,
        borderRadius: '12px',
        position: 'relative',
        '&::before': {
            content: '""',
            position: 'absolute',
            top: 40,
            bottom: 40,
            left: '50%',
            width: 2,
            backgroundColor: COLORS.grey3,
            [theme.breakpoints.down('lg')]: {
                content: 'unset',
                display: 'none'
            }
        },
        '& .box-plan': {
            '& .content-plan': {
                paddingBottom: 70,
                position: 'relative',
                height: '100%',
            },
            '& .button-choose': {
                position: 'absolute',
                bottom: 0,
                left: 0,
                right: 0,
            },
            '& .sale-plan': {
                color: COLORS.grey7,
                lineHeight: '64px',
                fontSize: 64,
                fontWeight: 600,
                marginRight: 16,
                [theme.breakpoints.down('lg')]: {
                    fontSize: 40,
                    lineHeight: '40px'
                }
            },
            '& .mr-16': {
                marginRight: 16,
            }
        }
    },
    Question: {
        '& .title': {
            paddingTop: 40,
        },
        '& .content': {
            backgroundColor: COLORS.white,
            borderRadius: '12px',
            padding: 40,
            marginTop: 24,
            marginBottom: 24,
        }
    },
    listMenu: {
        '& .item-menu': {
            height: 40,
            display: 'flex',
            alignItems: 'center',
            width: '100%',
            cursor: 'pointer',
            borderRadius: '8px',
            '&:hover': {
                backgroundColor: COLORS.grey1
            },
            '& .text': {
                color: `${COLORS.grey4} !important`
            },
            '&.active': {
                backgroundColor: COLORS.grey3,
                '& .text': {
                    color: `${COLORS.grey7} !important`,
                }
            }
        }
    },
    rightContentQuestion: {
        '& .box-title': {
            padding: '5px 12px',
            borderRadius: '8px',
            
            transition: 'all .3s ease',
            '&:hover .icon-svg': {
                backgroundColor: COLORS.grey1
            },
            '& .icon-svg': {
                cursor: 'pointer',
            },
            '& .title': {
                cursor: 'default',
                paddingTop: 0,
            }
        },
        '& .text': {
            whiteSpace: 'pre-line'
        },
        '& .collapse': {
          
            transition: 'all 0.7s ease',
            '&.MuiCollapse-hidden': {
                padding: 0,
                height: 'unset !important',
                maxHeight: 0,
            },
            '&.MuiCollapse-entered': {
                padding: 12,
                paddingTop: 0,
                height: 'unset !important',
                maxHeight: 300,
            }
        }
    }
});

export default styles;