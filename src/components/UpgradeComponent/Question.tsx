import { COLORS } from '@constants/colors';
import { iconAddCircledLight, iconCloseCircledLight, iconCloseLight } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, Collapse, Divider, Fade, Grid, IconButton, List, ListItem, Popper, SvgIcon } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import _ from 'lodash';
import React from 'react';
import styles from './styles';
import { isMobile } from '../../utils/index';

interface IQuestion {
    dataQuestions: Array<{
        name: string,
        items: Array<{
            title: string,
            content: string,
        }>
    }>,
    handleChangeTabActive: (idx: number)=> void,
    tabActive: number,
    collapseQuestion: Array<boolean>,
    handleChangeCollapseQuestion: (idx: number)=> void,
}

function Question(props:IQuestion & WithStyles<typeof styles>) {
    const {classes, 
        dataQuestions,
        handleChangeTabActive,
        tabActive,
        collapseQuestion,
        handleChangeCollapseQuestion,
    } = props;

    const RenderItem = ({idxItem, data}: {idxItem: number, data: {title: string, content: string}}) => {
        return <Box className='item'
        >
            <Box className={clsx('box-title', {
                ['active']: collapseQuestion[idxItem]
            })}
            display={'flex'}
            justifyContent='space-between'
            alignItems='center'
            >
                <Text baseSB1
                    className='title'
                    color={COLORS.grey7}>{data.title}</Text>
                <IconButton onClick={() => handleChangeCollapseQuestion(idxItem)}>
                    {
                        collapseQuestion[idxItem] ? 
                            <IconSVG icon={iconCloseCircledLight}
                                fill={COLORS.grey7}/>:
                            <IconSVG icon={iconAddCircledLight}
                                fill={COLORS.grey4}/>
                    }
                </IconButton>
                
            </Box>
            <Collapse className={'collapse'}
                in={collapseQuestion[idxItem]}>
                {data.content}
            </Collapse>
            <Divider/>
        </Box>;
    };

    return (
        <Box className={classes.Question}> 
            <Text color={COLORS.grey6}
                className='title'
                h3>Frequently asked question</Text>
            <Box className="content">
                <Grid container
                    spacing={isMobile ? 2: 13}>
                    <Grid item
                        lg={4}
                        xs={12}>
                        <List className={classes.listMenu}>
                            {!_.isEmpty(dataQuestions) && dataQuestions.map((data: {name: string}, index: number )=> (
                                <ListItem key={index}
                                    onClick={() => handleChangeTabActive(index)}
                                    className={clsx('item-menu', {
                                        ['active']: tabActive === index
                                    })}>
                                    <Text baseB1>{data.name}</Text>
                                </ListItem>
                            ))}
                        </List>
                    </Grid>
                    <Grid item
                        className={classes.rightContentQuestion}
                        lg={8}
                        xs={12}>
                        {!_.isEmpty(dataQuestions) &&
                         dataQuestions[tabActive]?.items.map((
                             data: any, idxItem)=> (
                             <RenderItem data={data}
                                 key={idxItem}
                                 idxItem={idxItem} />
                         ))}
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(Question);