import React from 'react';
import {WithStyles, withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Grid } from '@mui/material';
import RefundRequest from './RefundRequest';
import { ProductRefund } from '@containers/Admin/RefundsContainer/refundModel';
import ProductDetailComponent from '@components/ProductDetailComponent';

interface RefundsProps {
    title: string,
    dataTablePost :ProductRefund[],
    titlePage?: string,
    handleChangeTab?: (tabIndex: number) => void,
    currentTab: number,
    openModalProductDetail: boolean,
    handleOpenModalProductDetail: () => void,
    handleCloseModalProductDetail: () => void,
}

function RefundsComponent(props: RefundsProps & WithStyles<typeof styles>) {
    const {title, classes ,...otherProps} = props;
    return (
        <Page
            {...otherProps}
            title={title}>
            <Grid container>
                <RefundRequest
                    {...otherProps}
                    title='Refund request'/>
                <ProductDetailComponent {...otherProps}/>
            </Grid>
        </Page>
    );
}

export default withStyles(styles)(RefundsComponent);