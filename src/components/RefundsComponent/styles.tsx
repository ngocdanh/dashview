import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from '@constants/colors';
import { maxHeight } from '@mui/system';
const styles = (theme: any) => createStyles({
    root: {

    },
    overReview: {
        backgroundColor:COLORS.grey1,
        width: '100%',
        marginTop: 8,
        height: maxHeight,
        padding: 24,
        borderRadius:8,
        '& .overReviewBlockColor': {
            width: 16,
            height: 32,
            borderRadius:4,
            marginRight:16,

            '&.insights':{
                backgroundColor:COLORS.orangeLight,
            },
            '&.recentPost':{
                backgroundColor:COLORS.blueLight,
            },
            '&.refund':{
                backgroundColor:COLORS.violetLight,
            }
        },
    },
    header: {
        display:'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        gap:'16px'
    },
    subTitle: {
        padding:'12px',
        display: 'flex',
        flex: 1,
    },
    postTable:{
        '& tr td':{
            verticalAlign:'top'
        }
    },
    product:{
        display:'flex',
        alignItems:'center',
        gap:'20px',
        width:'268px',
        height:'80px',
        '& .product-column-content':{
            display:'flex',
            flexDirection:'column',
            alignItems:'flex-start',
            gap:'4px',
            width:'168px',
            height:'68px'
        }
    },
    status:{
        width: '136px',
        height: '28px',
        position:'relative',
        '& .status-number':{
            display:'flex',
            flexDirection:'column',
            justifyContent:'center',
            alignItems:'center',
            padding:'2px 8px',
            gap:'10px',
            position:'absolute',
            width:'101px',
            height:'28px',
            left:'0px',
            top:'calc(50% - 28px/2)',
            background: '#B5E4CA',
            borderRadius: '6px',
            '&.active':{
                width:'91px',
                background: '#CABDFF',
            }
        },
        '& p':{
            width:'max-content'
        }
    },
    customer:{
        display:'flex',
        alignItems:'center',
        gap:'12px',
        width:'180px',
        height:'32px',
        '& .customer-image ':{
            '& img':{
                width:'32px',
                height:'32px'
            }
        }
    },
    buttonTab: {
        background: `${COLORS.grey1}`,
        width: '100%',
        minWidth: 'max-content !important',
        '& .text': {
            color: `${COLORS.grey4} !important`,
        },
        '&.active': {
            backgroundColor: `${COLORS.grey3}  !important`,
            height: 50,
            '& .text': {
                color: `${COLORS.grey7} !important`,
            },
        }
    }
});

export default styles;