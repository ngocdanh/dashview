/* eslint-disable react/prop-types */
import { COLORS } from '@constants/colors';
import { withStyles, WithStyles } from '@mui/styles';
import { ProductRefund } from '@containers/Admin/RefundsContainer/refundModel';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { truncateString } from '@utils/index';
import React, { useMemo } from 'react';
import { CellProps, Column } from 'react-table';
import styles from './styles';
import Button from '@helpers/Button';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import { isMobile } from '../../utils/index';


interface Refund{
    title:string,
    dataTablePost:ProductRefund[],
    handleChangeTab: (tabIndex: number) => void,
    handleOpenModalProductDetail?: () => void,
}

const tabs = [
    {
        title: 'Open requests',
        tab: 0,
    },
    {
        title: 'Closed requests',
        tab: 1,
    }
];

function RefundRequest(props:Refund & WithStyles<typeof styles>) {
    const { title, dataTablePost,handleChangeTab,currentTab, handleOpenModalProductDetail, classes} = props;

    const coloumns : Column[] = useMemo(()=>[
        {
            Header:'Product',
            accessor:'post',
            Cell:(values: CellProps< NonNullable<null> > )=>{
                const original:ProductRefund = values.cell.row.original;
                return (

                    <Box className={classes.product}
                        onClick={handleOpenModalProductDetail}
                        sx={{cursor:'pointer'}}>
                        <Box className='product-column-image'>
                            <img src={original.product?.image}
                                alt={original.product?.title} />
                        </Box>
                        <Box className='product-column-content'>
                            <Text color={COLORS.grey7}
                                baseB1>
                                {truncateString(original.product?.title)}</Text>
                            <Text color={COLORS.grey4}
                                caption1>{original.product?.description}</Text>
                        </Box>
                    </Box>
                );
            }
        },
        {
            Header:'Status',
            accessor:'status',
            Cell:(values:CellProps<NonNullable<null>>)=>{
                const original:ProductRefund = values.cell.row.original;
                const statusClasses:string = original.status?.status ? '' :'active';
                return (
                    // eslint-disable-next-line react/prop-types
                    <Box className={classes.status}>
                        <Box className={'status-number ' + statusClasses} >
                            <Text color={COLORS.grey7}
                                bodySB2>{original.status?.status ? 'New request' : 'In progress'}</Text>
                        </Box>
                    </Box>
                );
            }
        },
        {
            Header:'Date',
            accessor:'date',
            Cell:(values:CellProps<NonNullable<null>>) => {
                const original:ProductRefund = values.cell.row.original;

                return (
                    <Box>
                        <Text color={COLORS.grey7}
                            bodySB2>{original.date?.date}</Text>
                    </Box>
                );
            }
        },
        {
            Header:'Customer',
            accessor:'customer',
            Cell:(values:CellProps<NonNullable<null>>) =>{
                const original:ProductRefund = values.cell.row.original;
                return (
                    // eslint-disable-next-line react/prop-types
                    <Box className={classes.customer}>
                        <Box className='customer-image'>
                            <img src={original.customer?.user_image}
                                alt="" />
                        </Box>
                        <Box className='customer-name'>
                            <Text color={COLORS.grey7}
                                bodySB2>{original.customer?.user_name}</Text>
                        </Box>
                    </Box>
                );
            }
        }
    ],[]);

    return (
        <Box className={classes.overReview}>
            <Box className={classes.header}
                display='flex'
                flexWrap='wrap'
                component={'div'}>
                <TitleElement color={COLORS.grey7}
                    bg={COLORS.violetLight}>{title}</TitleElement>
                <Box display={isMobile ? 'flex': 'flex'}
                    width={isMobile ? '100%': 'unset'}>
                    {
                        tabs.map((tab, index) => (
                            <Button key={index}
                                color = {COLORS.grey7}
                                className = {clsx(classes.buttonTab,{
                                    ['active']: tab.tab === currentTab
                                })}
                                onClick={handleChangeTab.bind(null, tab.tab)}>{tab.title}</Button>
                        ))
                    }
                </Box>
            </Box>
            <Box className={classes.postTable}>
                <TableCustom data={dataTablePost}
                    options={{
                        hoverRow: true,
                        changePage: true,
                    }}
                    columns={coloumns}/>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(RefundRequest);