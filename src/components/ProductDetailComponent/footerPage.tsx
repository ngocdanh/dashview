import iconHeartFilled from '@assets/icons/heart/filled.svg';
import { COLORS } from '@constants/colors';
import { iconArrowForward, iconDownloadFilled, iconStarFilled, imageOverviewUser1 } from '@constants/imageAssets';
import ButtonCustom from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Divider, Grid, } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import _ from 'lodash';
import React from 'react';
import styles from './styles';
import { v4 as uuidv4 } from 'uuid';

interface IPostItem {
    title: string,
    price: string,
    rating: string,
    totalRating: string,
    preview: string,
    url: string,
}

interface IFooterPage {
    titlePage?: string,
    dataPostOther: Array<IPostItem>
}


function FooterPageComponent(props: IFooterPage & WithStyles<typeof styles>) {
    const {
        classes,
        dataPostOther,
        ...otherProps} = props;

    const RenderItem = (propsItem: {product: IPostItem}) => {
        const {product} = propsItem;
        return ( <Grid
            item
            xs={12}
            lg={4}>
            <Box className="product-item">
                <Box className='product-image'>
                    <img src={product.preview}
                        alt="" />
                </Box>
                <Box className="product-content"
                    display='flex'
                    justifyContent={'space-between'}
                >
                    <Box className="product-content-left">
                        <Text baseSB1
                            color={COLORS.grey7}
                            className="product-title">
                            {product.title}
                        </Text>
                        <Box display={'flex'}
                            alignItems={'center'}>
                            <IconSVG icon={iconStarFilled}
                                fill={COLORS.yellowLight}/>
                            <Text base2
                                color={COLORS.grey7}>{product.rating}</Text>
                            <Text base2
                                color={COLORS.grey4}>({product.totalRating})</Text>
                        </Box>
                    </Box>
                    <Box className="product-content-right">
                        <Box className="product-price">
                            <Text baseB1
                                color={COLORS.grey7}>{product.price}</Text>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Grid>);
    };

    return (
    // eslint-disable-next-line react/prop-types
        <Box className={classes.footerModal}
            paddingTop={8}>
            <Box display='flex'
                alignItems={'center'}
                justifyContent='space-between'
                marginBottom={4}
            >
                <TitleElement color={COLORS.grey7}
                    bg={COLORS.greenNgoc}>More like this</TitleElement>
                <ButtonTransparent isElement>
                    <Box display='flex'
                        alignItems={'center'}>
                        <Text button2
                            color={COLORS.grey7}>
                  View All
                        </Text>
                        <IconSVG icon={iconArrowForward}
                            fill={COLORS.grey7}/>
                    </Box>
                    
                </ButtonTransparent>
            </Box>
            <Grid container
                spacing={2}>
                {!_.isEmpty(dataPostOther) && dataPostOther.map((product, index)=> (
                    <RenderItem key={index}
                        product={product} />
                ))}
            </Grid>
        </Box>
    );

}

export default withStyles(styles)(FooterPageComponent);