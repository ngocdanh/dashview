import {
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9
} from '@constants/imageAssets';
import faker from '@faker-js/faker';
import ModalCustom from '@helpers/ModalCustom';
import Page from '@helpers/Page';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { createFakerList } from '@utils/index';
import _ from 'lodash';
import React, { useMemo } from 'react';
import ContentPage from './contentPage';
import FooterPage from './footerPage';
import HeaderPage from './headerPage';
import styles from './styles';

interface IProductDetail {
    titlePage?: string,
    openModalProductDetail: boolean,
    handleOpenModalProductDetail: () => void,
    handleCloseModalProductDetail: () => void,
}

interface IPostItem {
    title: string,
    price: string,
    rating: string,
    totalRating: string,
    preview: string,
    url: string,
}

const listImage = [
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9,
];

const singleDataShop = ()=> {
    return {
        title: faker.lorem.sentence(4),
        price: `$${_.random(20, 100)}`,
        rating: `${_.random(3, 5, true).toFixed(1)}`,
        totalRating: `${_.random(20, 100)}`,
        preview: faker.helpers.arrayElement(listImage),
        url: '',
    };
};

function ProductDetailComponent(props: IProductDetail & WithStyles<typeof styles>) {
    const {
        classes,
        openModalProductDetail,
        handleOpenModalProductDetail, 
        handleCloseModalProductDetail, 
        ...otherProps} = props;
    const dataPostOther: Array<IPostItem> = useMemo(()=> createFakerList(singleDataShop, 6), []) as Array<IPostItem>;

    return (
        <ModalCustom
            open={openModalProductDetail}
            onClose={handleCloseModalProductDetail}
            fullPage
            className={classes.modalStyle}
        >
            <Box className={classes.root}>
                <HeaderPage {...props}/>
                <ContentPage {...props}/>
                <FooterPage dataPostOther={dataPostOther}
                    {...props}/>
            </Box>
        </ModalCustom>
    );
}

export default withStyles(styles)(ProductDetailComponent);