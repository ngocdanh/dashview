import iconHeartFilled from '@assets/icons/heart/filled.svg';
import { COLORS } from '@constants/colors';
import { iconDownloadFilled, iconStarFilled, imageOverviewUser1 } from '@constants/imageAssets';
import ButtonCustom from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, Divider } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';

interface IHeaderPage {
    titlePage?: string,
    openModalProductDetail: boolean,
    handleOpenModalProductDetail: () => void,
    handleCloseModalProductDetail: () => void,
}


function HeaderPageComponent(props: IHeaderPage & WithStyles<typeof styles>) {
    const {
        classes,
        openModalProductDetail,
        handleOpenModalProductDetail, 
        handleCloseModalProductDetail, 
        ...otherProps} = props;

    return (
    // eslint-disable-next-line react/prop-types
        <Box className={classes.headerModal}>
            <Box display='flex'
                alignItems='center'
                justifyContent='space-between'>
                <Box component='div'>
                    <Text baseB1
                        color={COLORS.grey4}>Product</Text>
                </Box>
                <Box display='flex'
                    alignItems='center'>
                    <ButtonTransparent  className='box-like'
                        isElement>
                        <Box
                            display='flex'
                            alignItems={'center'}>
                            <IconSVG icon={iconHeartFilled}
                                marginRight={8}
                                fill={COLORS.red}/>
                            <Text button1
                                color={COLORS.grey7}>32</Text>
                        </Box>

                    </ButtonTransparent>
                    <ButtonCustom isElement>
                        <Box className='box-download'
                            display='flex'
                            alignItems='center'>
                            <Text button1
                                color={COLORS.white}>$97</Text>
                            <Divider className='divider'
                                orientation="vertical"
                                light/>
                            <Text button1
                                color={COLORS.white}>Download now</Text>
                            <IconSVG marginLeft={8}
                                icon={iconDownloadFilled}
                                fill={COLORS.white}/>
                        </Box>
                    </ButtonCustom>
                </Box>
            </Box>
            <Text h4
                className='title'
                color={COLORS.grey7}>
                  Fleet - Travel shopping UI design kit
            </Text>
            <Text className='description'>Elagant product mockup for your next project</Text>
            <Box display='flex'
                alignItems='center'
                className="box-use-info">
                <img src={imageOverviewUser1}
                    width={32}
                    height={32}
                    alt="chelsie haley" />
                <Text base2
                    color={COLORS.grey7}>by <b>Ngoc Danh</b></Text>
                <Box display='flex'
                    alignItems='center'>
                    <IconSVG icon={iconStarFilled}
                        fill={COLORS.yellowLight}/>
                    <Text base2
                        color={COLORS.grey7}>4.9 (87)</Text>
                </Box>
            </Box>
        </Box>
    );

}

export default withStyles(styles)(HeaderPageComponent);