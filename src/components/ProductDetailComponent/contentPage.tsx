import iconHeartFilled from '@assets/icons/heart/filled.svg';
import { COLORS } from '@constants/colors';
import { iconCheckFilled, iconDownloadFilled, imgPreviewProduct } from '@constants/imageAssets';
import ButtonCustom from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Divider, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';

interface IContentPage {
    titlePage?: string,
    openModalProductDetail: boolean,
    handleOpenModalProductDetail: () => void,
    handleCloseModalProductDetail: () => void,
}


function ContentPageComponent(props: IContentPage & WithStyles<typeof styles>) {
    const {
        classes,
        ...otherProps} = props;

    const dataFeatures = [
        '128 prebuilt screens',
        'SaaS landing page ready',
        'Global styleguide',
        'Dark + light more ready',
    ];

    const dataOveview = [
        'Meet Node - a crypto NFT marketplace iOS UI design kit for Figma, Sketch, and Adobe XD. The kit includes 126 stylish mobile screens in light and dark mode, a bunch of crypto 3D illustrations, 1 SaaS landing page with full premade breakpoints, and hundreds of components to help you ship your next crypto, NFT product faster.',
        'Types of screens included: onboarding, connect wallet, home feed, profile, upload, menu, search, product detail, notification...',
        'If you have any questions or requests, please feel free to leave them all in the comments section.'
    ];

    return (
    // eslint-disable-next-line react/prop-types
        <Box className={classes.contentModal}>
            <Box className='box-img-preview'>
                <img src={imgPreviewProduct}
                    alt="Preview product"
                    className="img-preview" />
            </Box>
            <Grid container
                paddingY={5}
                spacing={16}>
                <Grid item
                    lg={8}
                    xs={12}>
                    <TitleElement bg={COLORS.orangeLight}
                        color={COLORS.grey7}>Overview</TitleElement>
                    <Box className="content">
                        {dataOveview.map((item, index)=> (
                            <Text bodyM1
                                key={index}
                                color={COLORS.grey5}>
                                {item}
                            </Text>
                        ))}
                       
                       
                    </Box>
                </Grid>
                <Grid item
                    lg={4}
                    xs={12}>
                    <TitleElement bg={COLORS.violetLight}
                        color={COLORS.grey7}>Feature</TitleElement>
                    <Box className="content">
                        <Box className="content-features">
                            {dataFeatures.map((item, index)=> (
                                <Box key={index}
                                    display='flex'
                                    alignItems={'center'}
                                    paddingBottom={2}>
                                    <IconSVG icon={iconCheckFilled}
                                        width={28}
                                        height={28}
                                        marginRight={12}/>
                                    <Text baseSB1
                                        color={COLORS.grey7}>{item}</Text>
                                </Box>
                            ))}
                        </Box>
                    </Box>
                </Grid>
            </Grid>
            <Divider/>
        </Box>
    );

}

export default withStyles(styles)(ContentPageComponent);