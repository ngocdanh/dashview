import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from '@constants/colors';
import { maxHeight } from '@mui/system';

const styles = (theme: any) => createStyles({
    root: {
        backgroundColor: COLORS.grey1,
        maxWidth: 1020,
        display: 'table',
        width: '100%',
        margin: '0 auto',
        borderRadius: 8,
        padding: 24,
    },
    headerModal: {
        
        '& .title': {
            paddingTop: 40,
        },
        '& .description': {
            paddingTop: 12,
            paddingBottom: 12,
        },
        '& .box-like': {
            marginRight: 16,
            '&:hover': {
                backgroundColor: COLORS.red,
                border: `2px solid ${COLORS.red}`,
                '& .icon-svg': {
                    color: COLORS.white,
                },
                '& .text': {
                    color: `${COLORS.white} !important`,
                }
            }
        },
        '& .box-download': {
            '& .divider': {
                backgroundColor: '#186FE3',
                borderWidth: 1,
                height: 15,
                marginRight: 10,
                marginLeft: 10,
            }
        },
        '& .box-use-info': {
            padding: '20px 0',
            '& img': {
                marginRight: 8,
            },
            '& .text': {
                marginRight: 8,
            }
        }
    },
    modalStyle: {
        '&~div~div.modal-custom': {
            overflow: 'auto',
        }
    },
    contentModal: {
        '& .img-preview': {
            width: '100%',
            objectFit: 'contain',
        },
        '& .content': {
            padding: '20px 0',
            '& .text': {
                marginBottom: 10,
            }
        }
    },
    footerModal: {
        '& .box-title': {
            '& .text': {
                marginRight: 11,
            }
        },
        '& .product': {
            '&-list': {
                marginTop: 32,
            },
            '&-item': {
                marginBottom: 32,
            },
            '&-content': {
                marginTop: 16,
                '&-left': {

                },
                '&-right': {
                    '& .product-price': {
                        borderRadius: '6px',
                        height: 32,
                        padding: '4px 8px',
                        backgroundColor: COLORS.greenLight,
                        textAlign: 'center',
                        '& .text': {
                            marginRight: 0,
                        }
                    }
                },
                '& .icon-svg': {
                    marginRight: 8,
                },
                '& .text': {
                    marginRight: 8,
                }
            },
            '&-image': {
                width: '100%',
                overflow: 'hidden',
                borderRadius: '16px',
                position: 'relative',
                '&::before': {
                    content: '""',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    position: 'absolute',
                    backgroundColor: 'transparent',
                    transition: 'all .3s ease',
                    zIndex: -1,
                },
                '& img': {
                    width: '100%',
                    height: '100%',
                    objectFit: 'contain',
                },
                '&:hover::before': {
                    zIndex: 1,
                    backgroundColor: '#46464661',
                },
            },
            '&-title': {
                marginBottom: 8,
                color: COLORS.grey7
            },

        },
    }
});

export default styles;