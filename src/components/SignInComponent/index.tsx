import { COLORS } from '@constants/colors';
import { iconFacebookPng, iconGooglePng, iconLockLight, iconMailLight, iconPersonFilled, logo } from '@constants/imageAssets';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import Input from '@helpers/Input';
import Text from '@helpers/Text';
import { Box, Divider, Grid, Stack } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';
import ButtonCustom from '@helpers/Button';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { signInWithFacebook, signInWithGoogle } from '@services/firebase';

interface SignInProps {
    onSubmit: (data: any)=> void,
    initialFormSignIn: {
        email: string,
        password: string,
        name: string,
    }
}

const schema = Yup
    .object()
    .shape({
        email: Yup.string().email('Must be a valid email').min(4).max(255).required('Email is required'),
        name: Yup.string().min(1).max(255).required('Name is required'),
        password: Yup.string().required('No password provided.') 
            .matches(
                /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
                'Password must contain at least 8 characters, one uppercase, one number and one special case character'
            ),
    })
    .required();

function SignInComponent(props: SignInProps  & WithStyles<typeof styles>) {
    const {classes, onSubmit, initialFormSignIn} = props;
    const { register,
        setValue,
        watch,
        handleSubmit, 
        formState: { isSubmitting, isDirty, isValid, errors },
    } = useForm({
        mode: 'onChange',
        defaultValues: initialFormSignIn,
        resolver: yupResolver(schema),
    });

    return (
        <Box className={classes.root}>
            <Box className={classes.content}>
                <img src={logo}
                    className={classes.styleLogo}
                    alt="" />
                <Text h2
                    color={COLORS.grey7}
                    className='title'>Sign In</Text>
                <Text bodySB2
                    color={COLORS.grey7}
                    className='title-signIn-social'>Sign in with Open accounts</Text>
                <Grid container
                    spacing={2}
                    className='box-social'>
                    <Grid item
                        lg={6}
                        sm={12}>
                        <ButtonTransparent isElement
                            onClick={signInWithGoogle}
                            className='button-social'>
                            <Box display={'flex'}>
                                <img src={iconGooglePng}
                                    alt=""
                                    className='icon' />
                                <Text color={COLORS.grey7}
                                    button1>Google</Text>
                            </Box>
                        </ButtonTransparent >
                    </Grid>
                    <Grid item
                        lg={6}
                        sm={12}>
                        <ButtonTransparent isElement
                            onClick={signInWithFacebook}
                            className='button-social'>
                            <Box display={'flex'}>
                                <img src={iconFacebookPng}
                                    alt=""
                                    className='icon' />
                                <Text color={COLORS.grey7}
                                    button1>Facebook</Text>
                            </Box>
                        </ButtonTransparent >
                    </Grid>
                </Grid>
                <Box className='divider'/>
                <Text bodySB2
                    color={COLORS.grey7}
                    className='title-signIn-social pt-32'>Or continue with email address</Text>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Box className='box-email'>
                        <Input type='string'
                            icon={iconPersonFilled}
                            placeholder='Name'
                            messangeError={errors?.name?.message}
                            {...register('name', { required: true })}
                        />
                        <Input type='email'
                            icon={iconMailLight}
                            placeholder='Your email'
                            messangeError={errors?.email?.message}
                            {...register('email', { required: true })}
                        />
                        <Input type="password"
                            icon={iconLockLight}
                            placeholder='Password'
                            autoComplete='on'
                            messangeError={errors?.password?.message}
                            {...register('password', { required: true })}
                        />
                        <ButtonCustom type={'submit'}
                            disabled={!isDirty || !isValid}
                            color='primary'>Sign In</ButtonCustom>
                    </Box>
                </form>
                <Text bodySB2
                    color={COLORS.grey4}
                    className='pv-32'>This site is protected by reCAPTCHA and the Google Privacy Policy.</Text>
                <Text bodySB2
                    color={COLORS.grey4}
                    className='pv-32'>Don’t have an account?  <a style={{color: COLORS.grey7}}
                        href='/sign-up'>Sign up</a></Text>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(SignInComponent);