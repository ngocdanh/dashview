import { COLORS } from '@constants/colors';
import { CurrentBalance } from '@containers/Admin/PayoutsContainer/PayoutModel';
import Button from '@helpers/Button';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, Grid } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';

interface CurrentBalanceProps{
    title:string,
    dataTableCurrentBalance:CurrentBalance[],
    titlePage?: string,
    openModalPayouts: boolean,
    handleOpenModalPayouts: () => void,
    handleCloseModalPayouts: () => void,
}

interface ListCurrentBalancesProps{
    data:CurrentBalance,
    index:number
}

const CurrentBalanceComponent = (props:CurrentBalanceProps  & WithStyles<typeof styles>) => {
    const { title,
        dataTableCurrentBalance 
        ,classes,
        openModalPayouts,
        handleOpenModalPayouts, 
        handleCloseModalPayouts, 
    } = props;

    const ListCurrentBalances = (props:ListCurrentBalancesProps)=>{
        const { data, index} = props;
        const { icon , title , price , bg} = data;
        return (
            // eslint-disable-next-line react/prop-types
            <Box className={`${classes.currentBalance} ${index === 0 ?  'first' : ''}`}>
                <Box className='icon'
                    bgcolor={bg}>
                    <IconSVG icon={icon}
                        fill={COLORS.black}/>
                </Box>
                <Box>
                    <Text caption1
                        color={COLORS.grey4}>{title}</Text>
                    <Text h2
                        color={COLORS.grey7}>${price}</Text>
                </Box>
            </Box>
        );
    };
    return (
        <>
            <Box
                className={classes.overReview}>
                <Box className={classes.header}
                    component={'div'}>
                    <Box className={classes.subTitle}>
                        <Box className='overReviewBlockColor recentPost'></Box>
                        <Text titleSB1>{title}</Text>
                    </Box>
                </Box>
                <Grid container
                    display='flex'
                    justifyContent={'center'}
                    className={classes.wrapper}>
                    {
                        dataTableCurrentBalance.map((data:CurrentBalance,index:number) => (
                            <Grid item
                                lg={4}
                                xs={12}
                                key={index} >
                                <ListCurrentBalances data={data}
                                    index={index}/>
                            </Grid>

                        ))
                    }
                    <Button color='primary'
                        onClick={handleOpenModalPayouts} >Withdraw balance</Button>
                </Grid>
            </Box>
        </>
    );
};

export default withStyles(styles)(CurrentBalanceComponent);