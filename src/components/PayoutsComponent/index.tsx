import React from 'react';
import {WithStyles, withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import CurrentBalanceComponent from './CurrentBalance';
import { Box, Grid } from '@mui/material';
import { CurrentBalance , PayoutHistory } from '@containers/Admin/PayoutsContainer/PayoutModel';
import PayoutHistoryComponent from './PayoutHistory';
import ModalCustom from '@helpers/ModalCustom';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import Button from '@helpers/Button';

interface PayoutProps {
    title: string,
    titlePage?: string,
    dataTableCurrentBalance?:CurrentBalance[],
    dataTableCurrentPayoutHistory:PayoutHistory[],
    openModalPayouts: boolean,
    handleOpenModalPayouts: () => void,
    handleCloseModalPayouts: () => void,
}


function PayoutsComponent(props: PayoutProps & WithStyles<typeof styles>) {
    const {title, 
        classes,
        openModalPayouts,
        handleOpenModalPayouts, 
        handleCloseModalPayouts, 
        ...otherProps} = props;

    function ContentModal () {
        return (
            // eslint-disable-next-line react/prop-types
            <Box className={classes.contentModal}>
                <Box textAlign={'center'}
                    display='flex'
                    justifyContent={'center'}
                >
                    <Box className="modal-party">
                        <Text h2
                            center>🎉</Text>
                    </Box>
                </Box>
                <Box className="box-content-text">
                    <Text titleSB1
                        center
                        color={COLORS.grey5}>Success!</Text>
                    <Text h2
                        center 
                        color={COLORS.grey5}>$128,000</Text>
                    <Text baseSB1
                        center
                        color={COLORS.grey5}>Has been sent to your Paypal: ngocdanh0508@gmail.com</Text>
                    <Box className='box-button-custom'
                        textAlign={'center'}>
                        <Button color={'primary'}
                            onClick={handleCloseModalPayouts}>Done!</Button>
                    </Box>
                </Box>
            </Box>
        );
    }

    return (
        <Page title={title}
            {...otherProps}>
            <CurrentBalanceComponent
                openModalPayouts = {openModalPayouts}
                handleOpenModalPayouts = {handleOpenModalPayouts}
                handleCloseModalPayouts = {handleCloseModalPayouts}
                {...otherProps}
                title='Current balance'/>
            <PayoutHistoryComponent
                {...otherProps}
                title='Payout history'
            />
            <ModalCustom
                open={openModalPayouts}
                onClose={handleCloseModalPayouts}
                width={560}
                height={476}
            >
                {ContentModal()}
            </ModalCustom>
        </Page>
    );
}

export default withStyles(styles)(PayoutsComponent);