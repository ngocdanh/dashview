import { COLORS } from '@constants/colors';
import { PayoutHistory } from '@containers/Admin/PayoutsContainer/PayoutModel';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import React, { useMemo } from 'react';
import { CellProps, Column } from 'react-table';
import styles from './styles';

interface PayoutHistoryProps{
    title:string,
    dataTableCurrentPayoutHistory:PayoutHistory[]
}

const PayoutHistoryComponent = (props:PayoutHistoryProps & WithStyles<typeof styles>) => {
    const {title,dataTableCurrentPayoutHistory ,classes} = props;
    const columns : Column[] = useMemo(()=>[
        {
            Header:'Month',
            minWidth: 120,
            Cell:(values:CellProps<NonNullable<null>>) =>
            {
                const original:PayoutHistory = values.cell.row.original;
                return (
                    <Text base2
                        color='#1A1D1F'>{original.month}</Text>
                );
            }
        },
        {
            Header:'Status',
            Cell:(values:CellProps<NonNullable<null>>) =>
            {
                const original:PayoutHistory = values.cell.row.original;
                return (
                    // eslint-disable-next-line react/prop-types
                    <Text base2
                        color='#1A1D1F'
                        className={`${classes.status}`}>{`${original.status ? 'Paid' : 'Unpaid' }`}</Text>
                );
            }
        },
        {
            Header:'Method',
            Cell:(values:CellProps<NonNullable<null>>) =>
            {
                const original:PayoutHistory = values.cell.row.original;
                return (
                    // eslint-disable-next-line react/prop-types
                    <Text base2
                        color='#1A1D1F'
                        className={`${classes.status}`}
                        style={{background:`${original.method ? COLORS.blueLight : COLORS.violetLight}`}}>{`${original.method ? 'Paypal' : 'SWIFT' }`}</Text>
                );
            }
        },
        {
            Header:'Earnings',
            Cell:(values:CellProps<NonNullable<null>>) =>
            {
                const original:PayoutHistory = values.cell.row.original;
                return (
                    <Text base2
                        color='#1A1D1F'>{original.earning}</Text>
                );
            }
        },
        {
            Header:'Amount withdrawn',
            Cell:(values:CellProps<NonNullable<null>>) =>
            {
                const original:PayoutHistory = values.cell.row.original;
                return (
                    <Text base2
                        color='#1A1D1F'>{original.amount}</Text>
                );
            }
        }
    ],[]);
    return (
        <Box className={classes.overReview}>
            <Box className={classes.header}
                component={'div'}>
                <Box className={classes.subTitle}>
                    <Box className='overReviewBlockColor refund'></Box>
                    <Text titleSB1>{title}</Text>
                </Box>
            </Box>

            <Box className={classes.postTable}>
                <Box className={classes.table}>
                    <TableCustom data={dataTableCurrentPayoutHistory}
                        options={{
                            hoverRow: true,
                            changePage: true,
                        }}
                        columns={columns}/>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(PayoutHistoryComponent);