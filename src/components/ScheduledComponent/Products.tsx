import { COLORS } from '@constants/colors';
import { iconCalendar, iconEdit, iconTrash } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import Input from '@helpers/Input';
import LabelColor from '@helpers/LabelColor';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Hidden, IconButton } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import _ from 'lodash';
import moment from 'moment';
import React, { useMemo } from 'react';
import { Column } from 'react-table';
import { truncateString } from './../../utils/index';
import styles from './styles';

interface IProductList {
    title: string,
    tabActiveProduct: number,
    dataTableProducts: any,
    handleSearchProduct: (e: React.MouseEvent<HTMLInputElement>) => void,
    handleDeleteProducts: (selectedRow:any) => void,
    handleSelectRows: (rows:any) => void,
    handleSelectedItem: (item:any) => void,
    selectedRow: any,
    handleOpenModalRechedule: () => void,
    handleCloseModalRechedule: () => void,
    searchProduct: string,
    classes:any,
    openModalRechedule: boolean,
    handleOpenModalProductDetail?: ()=> void,

}

function ProductList(props: IProductList & WithStyles<typeof styles>) {
    const { classes, title,
        tabActiveProduct, 
        dataTableProducts,
        handleSearchProduct, 
        handleOpenModalRechedule, 
        handleCloseModalRechedule,
        handleDeleteProducts,
        handleSelectRows,
        handleSelectedItem,
        selectedRow, 
        openModalRechedule,
        handleOpenModalProductDetail,
        searchProduct,
        ...otherProps
    } = props;

    const columnsProducts: Column[] = useMemo(()=> [
        {
            Header: 'Product',
            maxWidth: 300,
            minWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box display={'flex'}
                        style={{cursor: 'pointer'}}
                        onClick={handleOpenModalProductDetail}
                        alignItems={'center'}
                        sx={{cursor:'pointer'}}>
                        <Box className="products-image">
                            <img src={original.products.previewsm}
                                alt={original.products.title}
                                width={80}
                                height={80} />
                        </Box>
                        <Box className="products-content info"
                            sx={{maxWidth:180}}>
                            <Text color={COLORS.grey7}
                                className="products-title"
                                baseB1>
                                {truncateString(original.products.title,32)}
                            </Text>
                            <Hidden only={'xs'}>
                                <Text color={COLORS.grey4}
                                    caption1>
                                    {truncateString(original.products.link,25)}
                                </Text>
                            </Hidden>
                        </Box>
                    </Box>
                );
            },
        },
        {
            Header: 'Price',
            accessor: 'price',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={`$${original.price}`}
                    bg={original.price > 0 ? COLORS.greenNgoc : COLORS.grey3} />);
            }
        },
        {
            Header: 'Scheduled for',
            minWidth: 300,
            width: 'max-content',
            accessor: 'scheduledFor',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box component={'div'}
                        className="scheduled-for"
                        sx={{display: 'flex', alignItems:'center',justifyContent: 'space-between'}} >

                        <Text color={COLORS.grey4}
                            caption1>
                            {moment(original.scheduledFor).format('MMM D, YYYY')} at {moment(original.scheduledFor).format('h:mm A')}
                        </Text>
                        <Box component={'div'}
                            className="box-icon"
                            sx={{display:'flex',alignItems:'center'}}>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                                onClick ={() =>{
                                    handleSelectedItem(original);
                                    handleOpenModalRechedule();
                                }}
                            >
                                <IconSVG small
                                    icon={iconCalendar}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                            >
                                <IconSVG small
                                    icon={iconEdit}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                            <IconButton className='icon'
                                sx={{backgroundColor:COLORS.white}}
                                onClick ={() =>{
                                    handleDeleteProducts([original]);
                                }}
                            >
                                <IconSVG small
                                    icon={iconTrash}
                                    fill={COLORS.grey4}
                                />
                            </IconButton>
                        </Box>
                    </Box>
                );
            }
        },
     
     
    ], []);

    const renderTabProducts = () => {
        return (
            <TableCustom className='table-products'
                data={dataTableProducts}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                columns={columnsProducts}
                handleSelectRow={handleSelectRows}
            />
        );
    };

    return (
        <Box
            component={'div'}
            className={classes.ProductPage + (_.size(selectedRow) > 0 ? ' margin-more' :'')}
        >
            <Box display={'flex'}
                justifyContent={'space-between'}
                className='box-title-element'>
                <Box className="content-left"
                    display={'flex'}
                    flexWrap='wrap'
                    alignItems={'center'}>

                    <TitleElement bg={COLORS.violetLight}>
                        {title}
                    </TitleElement>
                    <Input placeholder='Search product'
                        style={{marginLeft:24}}
                        search={searchProduct}
                        onChange={handleSearchProduct}
                    />
                </Box>
            </Box>
            {
                renderTabProducts()
            }
        </Box>
    );
}

export default withStyles(styles)(ProductList);