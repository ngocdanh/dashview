import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import Products from './Products';
import Message from './Message';
import ModalCustom from '@helpers/ModalCustom';
import { COLORS } from '@constants/colors';
import { Box } from '@mui/material';
import Text from '@helpers/Text';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import { iconCalendar, iconClock } from '@constants/imageAssets';
import moment from 'moment';
import DateCustom from '@helpers/DateCustom';
import TimeCustom from '@helpers/TimeCustom';
import Button from '@helpers/Button';
import ProductDetailComponent from '@components/ProductDetailComponent';

interface ScheduledProps {
    classes:any,
    title: string,
    dataTableProducts: any,
    handleSearchProduct: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleSelectRows: (rows:any) => void,
    handleSelectedItem: (item:any) => void,
    handleDeleteProducts: (selectedRow:any) => void,
    handlePublishProducts:(selectedRow:any) => void,
    handleSetDate:(selectedRow:any) => void,
    handleOpenModalRechedule: () => void,
    handleCloseModalRechedule: () => void,
    handleOpenSelectDate: () => void,
    handleCloseSelectDate: () => void,
    handleOpenSelectTime: () => void,
    handleCloseSelectTime: () => void,
    handlReschedule: () => void,
    SelectDate: boolean,
    openModalRechedule: boolean,
    openModalSelectDate: boolean,
    openModalSelectTime: boolean,
    searchProduct: string,
    selectedRow:any,
    selectedItem: any,
    dateSelected: any,
    titlePage?: string,
    openModalProductDetail: boolean,
    handleOpenModalProductDetail: () => void,
    handleCloseModalProductDetail: () => void,

}

function ScheduledComponent(props: ScheduledProps) {
    const {
        title, 
        titlePage,
        classes,        
        dateSelected ,
        handleCloseModalRechedule,
        handleOpenSelectDate,
        handleCloseSelectDate,
        handleSetDate, 
        handlReschedule,
        handleOpenSelectTime,
        handleCloseSelectTime,
        openModalSelectTime,
        openModalSelectDate,
        openModalRechedule,
        ...otherProps
    } = props;
    const ContentModal =  () => {
        return (
        // eslint-disable-next-line react/prop-types
            <Box className={classes.contentModal}>
                <Box textAlign={'center'}
                    display='flex'
                    justifyContent={'center'}
                >
                </Box>
                <Box className="box-content-text">
                    <Text bodyM1
                        color={COLORS.grey4}>
                      Choose a day and time in the future you want your product to be published.
                    </Text>
                    <Box className="box-calendar">
                        <ButtonTransparent isElement
                            onClick={() =>{
                                handleOpenSelectDate();
                                handleCloseSelectTime();
                            }}
                            className={'button-modal' + (openModalSelectDate? ' active-button' :'')}
                        >
                            <IconSVG
                                className={'icon'} 
                                width={36}
                                height={24}
                                icon={iconCalendar}
                                fill={COLORS.grey4}/>
                            <Box>
                                <Text captionM2
                                    color={COLORS.grey4}>Date</Text>
                                <Text baseSB1
                                    color={COLORS.grey7}>{moment(dateSelected).format('DD MMMM, YYYY')}</Text>
                            </Box>
                        </ButtonTransparent>
                        <ButtonTransparent isElement
                            onClick={() =>{
                                handleOpenSelectTime();
                                handleCloseSelectDate();
                            }}
                            className={'button-modal' + (openModalSelectTime? ' active-button' :'')}
                        >
                            <IconSVG
                                className={'icon'} 
                                width={36}
                                height={24}
                                icon={iconClock}
                                fill={COLORS.grey4}/>
                            <Box>
                                <Text captionM2
                                    color={COLORS.grey4}>Time</Text>
                                <Text baseSB1
                                    color={COLORS.grey7}>{moment(dateSelected).format('hh:mm A')}</Text>
                            </Box>
                        </ButtonTransparent>
                        <DateCustom  data={dateSelected}
                            handleSetDate={handleSetDate}
                            handleCloseModal={handleCloseSelectDate}
                            openModal={openModalSelectDate} />
                        <TimeCustom data={dateSelected}
                            handleSetDate={handleSetDate}
                            handleCloseModal={handleCloseSelectTime}
                            openModal={openModalSelectTime} />          
                    </Box>
                    <Box className='box-button-custom'
                        textAlign={'right'}>
                        <Button color={'primary'}
                            onClick={() =>{
                                handleCloseSelectTime();
                                handleCloseSelectDate();
                                handleCloseModalRechedule();
                                handlReschedule();
                            }}>Reschedule</Button>
                    </Box>
                </Box>
            </Box>
        );
    };
    return (
        <Page title={title} 
            titlePage={titlePage}
            {...otherProps}>
            <Products title={'Products'}
                {...otherProps} />
            <Message {...otherProps} />
            <ModalCustom
                open={openModalRechedule}
                onClose={() =>{
                    handleCloseModalRechedule();
                    handleCloseSelectDate();
                    handleCloseSelectTime();
                }}
                title={'Reschedule product'}
                bgTitle={COLORS.orangeLight}
                width={390}
                height={400}
            >
                {ContentModal()}
            </ModalCustom>
            <ProductDetailComponent {...otherProps}/>
        </Page>
    );
}

export default withStyles(styles)(ScheduledComponent);