import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Grid, Box } from '@mui/material';
import Overview from './Overview';
import ProductSales from './ProductSales';
import TopCountries from './TopCountries';
import EarningReport from './EarningReport';



interface EarningProps {
    title: string,
    phone?: number, // optional
    titlePage?: string,
    classes:any,
    dataTableContries:any,
    dataTableEarningResults:any,
}

function EarningComponent(props: EarningProps) {
    const {title, titlePage, classes, ...otherProps} = props;
    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}
            className={classes.EarningPage}
        >
            <Overview {...props} />
            <Grid container
                display={'flex'}
                alignItems={'center'}
                spacing={1}>
                <Grid item
                    lg={9}
                    xs={12}>
                    <ProductSales title="Product sales"
                        {...otherProps}/>
                </Grid>
                <Grid item
                    lg={3}
                    xs={12}
                >
                    <TopCountries title="Top countries"
                        {...otherProps}/>
                </Grid>
            </Grid>
            <EarningReport title="earning Report"
                {...otherProps}/>
        </Page>
    );
}

export default withStyles(styles)(EarningComponent);