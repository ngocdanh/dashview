// Earning report
import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Grid, Box } from '@mui/material';
import TableCustom from '@helpers/TableCustom';
import { useMemo } from 'react';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import LabelColor from '@helpers/LabelColor';
import { isMobile } from '../../utils/index';

interface EarningReportsProps {
    title: string,
    titlePage?: string,
    classes:any,
    dataTableEarningResults:any
}

function EarningReportsComponent(props: EarningReportsProps) {
    const {title, classes, dataTableEarningResults, ...otherProps} = props;
    const columnsResults: Column[] = useMemo(()=> [
        {
            Header: 'Date',
            maxWidth: 300,
            minWidth: 120,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;  
                return (
                    <Text base2
                        color={COLORS.grey4} >
                        {original.date}
                    </Text>
                 
                );
            },
        },
        {
            Header: 'Status',
            accessor: 'status',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;  
                return (
                    <LabelColor title={original.status}
                        bg={original.status === 'Paid'? COLORS.greenLight: COLORS.yellowLight}
                    />
                );
            },
        },
        {
            Header: isMobile ? 'Sales count': 'Product sales count',
            accessor: 'salecount',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;  
                return (
                    <Text base2
                        color={COLORS.grey7}>
                        {original.salecount}
                    </Text>
                 
                );
            },
        },
        {
            Header: 'Earnings',
            accessor: 'earnings',
            maxWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;  
                return (
                    <Text base2
                        color={COLORS.grey7}>
                        {original.earnings}
                    </Text>
                 
                );
            },
        },
    ], []);

    const renderTabEarningResults= () => {
        return (
            <TableCustom className='table-results'
                data={dataTableEarningResults}
                options={{
                    hoverRow: false,
                    changePage: false,
                }}
                columns={columnsResults}/>
        );
    };

    return (
        <Box component={'div'}
            className={classes.earningReports}
        >
            {renderTabEarningResults()}
        </Box>
    );
}

export default withStyles(styles)(EarningReportsComponent);