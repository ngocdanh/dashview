import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
    
    },
    productDashboardPage: {
        width: '100%',
    },
    overviewComponent: {
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: 24,
        marginBottom: 32,
        width: '100%',
        '& .box-title-element': {
            padding: '5px 0',
            alignItems: 'center',
            [theme.breakpoints.down('sm')]: {
                display: 'block',
            }
        },
        '& .list-chart': {
            marginTop: 32,
        },
        '& .item-chart': {
           
            '& .icon': {
                width: 48,
                height: 48,
                borderRadius: '48px',
                backgroundColor: COLORS.grey7,
                
            },
            '& .chart-info': {
                marginTop: 16,
                '& .icon-mode': {
                    width: 16,
                    height: 16,
                },
                '& .image-chart': {
                    objectFit: 'contain',
                    height: 50,
                },
                '& .icon-svg svg': {
                    width: 16,
                    height: 16,
                }
            }
        }
    },
    ProductActivityComponent: {
        height: 300,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: 24,
        marginBottom: 32,
        width: '100%',
        '& .box-title-element': {
            padding: '5px 0',
            alignItems: 'center',
            [theme.breakpoints.down('sm')]: {
                display: 'block',
            }
        },
        '& .table-product-activity': {
            marginTop: 32,
            '& .cell-week': {
                '@media (max-width: 1600px)': {
                    fontSize: 13,
                }
            },
        }
    },
    ProductsComponent: {
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: 24,
        marginBottom: 32,
        width: '100%',
        '& .box-title-element': {
            padding: '5px 0',
            alignItems: 'center',
            [theme.breakpoints.down('sm')]: {
                display: 'block',
            }
        },
        '& .table-products': {
            marginTop: 32,
            [theme.breakpoints.down('sm')]: {
                marginTop: 5,
            },
            '& tr td:nth-child(1)': {
                verticalAlign: 'top',
            },
            '& .product-box': {
            },
            '& .no-wrap': {
                flexWrap: 'nowrap'
            },
            '& .product-image': {
                // marginRight: 20,
            },
            '& .product-content.traffic': {
                minWidth: 200,
                paddingLeft: 10,
            },
            '& .box-mode': {
                backgroundColor: 'transparent',
            },

        },
        '& .product-tabs': {
            [theme.breakpoints.down('sm')]: {
                justifyContent: 'center',
                display: 'flex',
                flexWrap: 'wrap',
            },
            '& .button-tab': {
                height: 40,
                borderRadius: '8px',
                padding: '8px 16px',
                backgroundColor: 'transparent',
                color: COLORS.grey4,
                transition: 'all .3s ease',
                '& .text': {
                    color: `${COLORS.grey4} !important`,
                },
                '&.active': {
                    color: COLORS.grey7,
                    backgroundColor: COLORS.grey3,
                    '& .text': {
                        color: `${COLORS.grey7} !important`,
                    }
                }
            }
        },
        '& .content-left': {
            '& .title-element': {
                [theme.breakpoints.down('sm')]: {
                    width: '100%'
                }
            }
        },
        '& .box-left':{
            position:'relative',
            '& .search-box':{
                opacity: 0,
                visibility: 'hidden',
                transition: 'opacity .3s ease',
                '&.show':{
                    opacity: 1,
                    visibility: 'visible',
                    [theme.breakpoints.down('sm')]: {
                        margin: '10px 0 !important',
                    }
                }
            },
            '& .messgae-box':{
                position:'absolute',
                display:'flex',
                justifyContent:'space-between',
                alignItems:'center',
                top:0,
                left:'24px',
                opacity: 0,
                visibility: 'hidden',
                transition: 'opacity .3s ease',
                '&.show':{
                    opacity: 1,
                    visibility: 'visible',
                    
                }
            }
        }
    },
    productViews: {
        height: 300,
        backgroundColor: COLORS.white,
        borderRadius: 8,
        padding: 24,
        marginBottom: 32,
        width: '100%',
        // boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        '& .render-chart': {
            marginTop: 32,
            '& img': {
                width: '100%',
                objectFit: 'cover',
            }
        }
    },
    boxHeader: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    contentModal: {
        borderTop: `1px solid ${COLORS.grey3}`,
        '& .modal-party': {
            width: 128,
            height: 128,
            borderRadius: '50%',
            backgroundColor: COLORS.greenLight,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
      
        },
        '& .button-modal':{
            marginTop:8,
            justifyContent: 'flex-start',
            transition: 'border .3s ease 0s',
            '&:hover': {
                borderColor:COLORS.blue,
            },
            '& .icon':{
                marginRight:'12px'
            },
            '&.active-button':{
                borderColor:COLORS.blue,
                '& .icon-svg': {
                    color: COLORS.grey7,
                }
            }
        },
        '& .box-content-text': {
            textAlign: 'center',
            marginTop: 20,
    
        },
        '& .box-calendar':{
            marginTop: 12,
        },
        '& .box-button-custom': {
            marginTop: 20,
        }
    }
});

export default styles;