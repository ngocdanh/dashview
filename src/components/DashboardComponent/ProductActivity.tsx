import { COLORS } from '@constants/colors';
import LabelColor from '@helpers/LabelColor';
import SelectCustom from '@helpers/SelectCustom';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as _ from 'lodash';
import React, { useMemo } from 'react';
import { Column } from 'react-table';
import styles from './styles';

interface IProductActivityComponent {
    title: string,
    dataTableProductActivity:any,
    fillterProductActivitySelect:string,
    handleSelectProductActivity: (e: React.ChangeEvent<HTMLInputElement>) => void,
   
}


function ProductActivityComponent(props: IProductActivityComponent & WithStyles<typeof styles>) {
    const {title,
        classes,
        dataTableProductActivity,
        handleSelectProductActivity,
        fillterProductActivitySelect, 
        ...otherProps
    } = props;

    const dataSelect = [
        {
            value: 'Last 2 weeks',
            label: 'Last 2 weeks',
        },
        {
            value: 'Last 7 days',
            label: 'Last 7 days',
        },

    ];

    const columns: Column[] = useMemo(()=> [
        {
            Header: 'Week',
            width: 'max-content',
            maxWidth: 100,
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                
                return  (<Text base2
                    className={clsx('cell-week', 'w-max-content')}
                    color={COLORS.grey4}>
                    {`${original.start_week} - ${original.end_week}`}
                </Text>);
            },
        },
        {
            Header: 'Products',
            accessor: 'products',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    _.isObject(original.products)? (
                        <LabelColor title={original.products.quantity}
                            mode={original.products.mode}
                            numeral={original.products.numeral}
                            bg={original.colors[3]} />
                    ): (<LabelColor title={original.products}
                        bg={original.colors[3]} />)
                );
            }
        },
        {
            Header: 'Views',
            accessor: 'views',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    _.isObject(original.views)? (
                        <LabelColor title={original.views.quantity}
                            mode={original.views.mode}
                            numeral={original.views.numeral}
                            bg={original.colors[1]} />
                    ): (<LabelColor title={original.views}
                        bg={original.colors[1]} />)
                );
            }
        },
        {
            Header: 'Likes',
            accessor: 'likes',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    _.isObject(original.likes)? (
                        <LabelColor title={original.likes.quantity}
                            mode={original.likes.mode}
                            numeral={original.likes.numeral}
                            bg={original.colors[2]} />
                    ): (<LabelColor title={original.likes}
                        bg={original.colors[2]} />)
                );
            }
        },
        {
            Header: 'Comments',
            accessor: 'comments',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    _.isObject(original.comments)? (
                        <LabelColor title={original.comments.quantity}
                            mode={original.comments.mode}
                            numeral={original.comments.numeral}
                            bg={original.colors[0]} />
                    ): (<LabelColor title={original.comments}
                        bg={original.colors[0]} />)
                );
            }
        },
    ], []);

    return (
        <Box className={classes.ProductActivityComponent}>
            <Box display={'flex'}
                justifyContent={'space-between'}
                className='box-title-element'>
                <TitleElement color={COLORS.grey7}
                    bg={COLORS.violetLight}>
                    {title}
                </TitleElement>
                <SelectCustom defaultValues='Last 2 weeks'
                    data={dataSelect}
                    select={fillterProductActivitySelect}
                    handleSelect={handleSelectProductActivity}
                />
            </Box>
            <TableCustom className={'table-product-activity'}
                data={dataTableProductActivity}
                columns={columns}/>
        </Box>
    );
}

export default withStyles(styles)(ProductActivityComponent);