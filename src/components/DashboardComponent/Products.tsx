import { COLORS } from '@constants/colors';
import Button from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import Input from '@helpers/Input';
import LabelColor from '@helpers/LabelColor';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import TooltipCustom from '@helpers/TooltipCustom';
import { Box, Grid } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as _ from 'lodash';
import React, { useMemo } from 'react';
import { Column } from 'react-table';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';
import { isMobile } from '../../utils/index';

interface IProductsComponent {
    title: string,
    tabActiveProduct: number,
    dataTableMarket: any,
    handleChangeTabActiveProduct: (index: number)=> void,
    handleDeleteProducts: (selectedRow:any) => void,
    handleSelectRows: (rows: any)=> void,
    handleSearchProduct: (e: React.MouseEvent<HTMLInputElement>)=> void,
    handleOpenModalStatus:() => void,
    searchProduct: string,
    selectedRow:any,
    handleOpenModalProductDetail: () => void,
}



const ProductsComponent = React.memo((props: IProductsComponent & WithStyles<typeof styles>) => {
    const {
        title,
        tabActiveProduct, 
        classes,
        dataTableMarket,
        handleChangeTabActiveProduct,
        handleSearchProduct,
        handleSelectRows,
        searchProduct,
        selectedRow,
        handleDeleteProducts,
        handleOpenModalStatus,
        handleOpenModalProductDetail,
        ...otherProps} = props;


    const columnsMarket: Column[] = useMemo(()=> [
        {
            Header: 'Product',
            maxWidth: 300,
            minWidth: 300,
            width:  'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Grid container
                        spacing={2}
                        className='no-wrap'
                        style={{cursor: 'pointer'}}
                        onClick={handleOpenModalProductDetail}>
                        <Grid item
                            lg={4}
                            sm={4}
                            className="product-image product-box">
                            <img src={original.products.preview}
                                alt={original.products.title}
                                width={80}
                                height={80} />
                        </Grid>
                        <Grid item
                            lg={8}
                            sm={8}
                            className="product-content product-box">
                            <Text color={COLORS.grey7}
                                baseB1>
                                {original.products.title}
                            </Text>
                            <Text color={COLORS.grey4}
                                caption1>
                                {original.products.description}
                            </Text>
                        </Grid>
                    </Grid>
                );
            },
        },
        {
            Header: 'Status',
            accessor: 'status',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={original.status}
                    bg={original.status === 'Active'? COLORS.greenLight: COLORS.redLight}
                />);
            }
        },
        {
            Header: 'Price',
            accessor: 'price',
            Cell: (item)=> (
                <Text baseSB1
                    color={COLORS.grey7}>
                    {item.value}
                </Text>
            )
        },
        {
            Header: 'Sales',
            accessor: 'sales',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={original.sales.quantity}
                    mode={original.sales.mode}
                    numeral={original.sales.numeral}
                    bg={original.color} />);
            }
        },
        {
            Header: 'Views',
            accessor: 'views',
            Cell: (values)=> {
                const original = values.cell.row.original;
                return (<Box display={'flex'}>
                    <LabelColor
                        bg={COLORS.grey3} 
                        title={original.views.quantity}
                        widthAfter={original.views.width}
                    />
                </Box>);
            }
        },
        {
            Header: 'Likes',
            accessor: 'likes',
            Cell: (values)=> {
                const original = values.cell.row.original;
                return (<Box display={'flex'}>
                    <LabelColor
                        bg={COLORS.grey3} 
                        title={original.likes.quantity}
                        widthAfter={original.likes.width}
                    />
                </Box>);
            }
        },
    ], []);

    const columnsTrafficSources: Column[] = useMemo(()=> [
        {
            Header: 'Product',
            maxWidth: 300,
            width: 300,
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box display={'flex'}
                        style={{cursor: 'pointer'}}
                        onClick={handleOpenModalProductDetail}
                        alignItems={'center'}>
                        <Box className="product-image">
                            <img src={original.products.preview}
                                alt={original.products.title}
                                width={80}
                                height={80} />
                        </Box>
                        <Box className="product-content traffic">
                            <Text color={COLORS.grey7}
                                baseB1>
                                {original.products.title}
                            </Text>
                            <Text color={COLORS.grey4}
                                caption1>
                                {original.products.description}
                            </Text>
                        </Box>
                    </Box>
                );
            },
        },
        {
            Header: 'Traffic source',
            RenderHeader: () => {
                const listTitle = [
                    {
                        title: 'Market',
                        color: COLORS.orangeLight,
                    },
                    {
                        title: 'Source media',
                        color: COLORS.violet,
                    },
                    {
                        title: 'Direct',
                        color: COLORS.green,
                    },
                    {
                        title: 'Dashview',
                        color: COLORS.blue,
                    },
                    {
                        title: 'Other',
                        color: COLORS.yellowLight,
                    },
                ];
                return (<Box display={'flex'}
                    justifyContent={'space-between'}>
                    <Text base1
                        color={COLORS.grey4}>
                          Traffic source
                    </Text>
                    <Box display={'flex'}
                        alignItems="center">
                        {listTitle.map((item: {
                            title: string,
                            color: string
                        })=> (<Box display={'flex'}
                            alignItems={'center'}
                            sx={{
                                marginRight: 2,
                            }}
                            key={uuidv4()}>
                            <Box style={{
                                width: 12,
                                height: 12,
                                borderRadius: 2,
                                backgroundColor: item.color,
                                marginRight: 8,
                            }}/>
                            <Text caption1
                                color={COLORS.grey4}>{item.title}</Text>
                        </Box>))}
                    </Box>
                </Box>);
            },
            Cell: (values)=> {
                const trafficSources = values.cell.row.original.traffic_sources;
                const arrData = Object.values(trafficSources);
                
                return (<Box display={'flex'}>
                    {!_.isEmpty(arrData) && arrData.map((item: {
                        title: string,
                        value: number,
                        color: string,
                    })=> {
                        const renderTooltip = () => (
                            <Box className='tooltip-traffic'>
                                <Text caption1>
                                    {item.title}</Text>  
                                <Box display={'flex'}
                                    alignItems={'center'}>
                                    <Box style={{
                                        backgroundColor: item.color,
                                        width: 12,
                                        height: 12,
                                        borderRadius: '4px',
                                        marginRight: 4,
                                    }}/>
                                    <Text caption1>
                                        {item.value}</Text>  
                                </Box>
                            </Box>
                        );
                        return (
                            <TooltipCustom  key={uuidv4()}
                                title={renderTooltip()}>
                                <Box
                                    style={{
                                        backgroundColor: item.color,
                                        width: item.value/800 * 300,
                                        borderRadius: '2px', 
                                        height: 12,
                                    }}
                                    className="traffic-item" />
                            </TooltipCustom>
                        );
                    })}
                </Box>);
            }
        }
    ], []);
    const columnsViewers: Column[] = useMemo(()=> [
        {
            Header: 'Product',
            maxWidth: 300,
            width: 300,
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box display={'flex'}
                        style={{cursor: 'pointer'}}
                        onClick={handleOpenModalProductDetail}
                        alignItems={'center'}>
                        <Box className="product-image">
                            <img src={original.products.preview}
                                alt={original.products.title}
                                width={80}
                                height={80} />
                        </Box>
                        <Box className="product-content traffic">
                            <Text color={COLORS.grey7}
                                baseB1>
                                {original.products.title}
                            </Text>
                            <Text color={COLORS.grey4}
                                caption1>
                                {original.products.description}
                            </Text>
                        </Box>
                    </Box>
                );
            },
        },
        {
            Header: 'Traffic source',
            RenderHeader: () => {
                const listTitle = [
                    {
                        title: 'Folowers',
                        color: COLORS.green,
                    },
                    {
                        title: 'Others',
                        color: COLORS.violet,
                    },
                ];
                return (<Box display={'flex'}
                    justifyContent={'space-between'}>
                    <Text base1
                        color={COLORS.grey4}>
                          Traffic source
                    </Text>
                    <Box display={'flex'}
                        alignItems="center">
                        {listTitle.map((item: {
                            title: string,
                            color: string
                        })=> (<Box display={'flex'}
                            alignItems={'center'}
                            sx={{
                                marginRight: 2,
                            }}
                            key={uuidv4()}>
                            <Box style={{
                                width: 12,
                                height: 12,
                                borderRadius: 2,
                                backgroundColor: item.color,
                                marginRight: 8,
                            }}/>
                            <Text caption1
                                color={COLORS.grey4}>{item.title}</Text>
                        </Box>))}
                    </Box>
                </Box>);
            },
            Cell: (values)=> {
                const viewers = values.cell.row.original.viewers;
                const arrData = Object.values(viewers);
                
                return (<Box display={'flex'}>
                    {!_.isEmpty(arrData) && arrData.map((item: {
                        title: string,
                        value: number,
                        color: string,
                    })=> {
                        const renderTooltip = () => (
                            <Box className='tooltip-traffic'>
                                <Text caption1>
                                    {item.title}</Text>  
                                <Box display={'flex'}
                                    alignItems={'center'}>
                                    <Box style={{
                                        backgroundColor: item.color,
                                        width: 12,
                                        height: 12,
                                        borderRadius: '4px',
                                        marginRight: 4,
                                    }}/>
                                    <Text caption1>
                                        {item.value}</Text>  
                                </Box>
                            </Box>
                        );
                        return (
                            <TooltipCustom  key={uuidv4()}
                                title={renderTooltip()}>
                                <Box
                                    style={{
                                        backgroundColor: item.color,
                                        width: item.value/800 * 300,
                                        borderRadius: '2px', 
                                        height: 12,
                                    }}
                                    className="traffic-item" />
                            </TooltipCustom>
                        );
                    })}
                </Box>);
            }
        }
    ], []);

    const renderTabMarket = () => {
        return (
            <TableCustom className='table-products'
                data={dataTableMarket}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                handleSelectRow={handleSelectRows}
                columns={columnsMarket}/>
        );
    };

    const renderTabTrafficSources = () => {
        return (
            <TableCustom className='table-products'
                data={dataTableMarket}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                handleSelectRow={handleSelectRows}  
                columns={columnsTrafficSources}/>
        );
    };

    const renderTabViewers = () => {
        return (
            <TableCustom className='table-products'
                data={dataTableMarket}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                handleSelectRow={handleSelectRows}
                columns={columnsViewers}/>
        );
    };

    const tabs = [
        {
            title: 'Market',
            indexTab: 0,
        },
        {
            title: 'Traffic Sources',
            indexTab: 1,
        },
        {
            title: 'Viewers',
            indexTab: 2,
        },
    ];

    return (
        <Box className={classes.ProductsComponent}
            component='div'>
            <Box display={'flex'}
                justifyContent={'space-between'}
                className='box-title-element'>
                <Box className="content-left"
                    display={'flex'}
                    flexWrap={'wrap'}
                    alignItems={'center'}>
                    <TitleElement color={COLORS.grey7}
                        bg={COLORS.violetLight}>
                        {title}
                    </TitleElement>
                    <Box className="box-left"
                        display={'flex'}
                        flexWrap='wrap'>
                        <Input placeholder='Search product'
                            style={{marginLeft:24}}
                            search={searchProduct}
                            className={'search-box' + (_.size(selectedRow) == 0?' show':'')}
                            onChange={handleSearchProduct}
                        />
                        <Box className={'messgae-box' + (_.size(selectedRow) == 0?'':' show')}>
                            <ButtonTransparent isElement
                                className='box-item'
                                onClick={() =>{
                                    handleDeleteProducts(selectedRow);
                                }}
                                sx={{marginRight:'8px'}}>
                                <Text button2
                                    color={COLORS.grey7}>
                        Deleted
                                </Text>
                            </ButtonTransparent> 
                            <ButtonTransparent isElement
                                className='box-item'
                                onClick={() =>{
                                    handleOpenModalStatus();
                                }}
                                sx={{marginRight:'8px'}}>
                                <Text button2
                                    color={COLORS.grey7}>
                        Set status
                                </Text>
                            </ButtonTransparent> 
                            <Text button2
                                color={COLORS.grey7}>
                                {_.size(selectedRow)} selected
                            </Text>
                        </Box>
                    </Box>  
                </Box>
                <Box className="content-right product-tabs">
                    {tabs.map((tab: {
                        title: string,
                        indexTab: number
                    })=> (
                        <Button key={uuidv4()}
                            onClick={()=> {
                                handleChangeTabActiveProduct(tab.indexTab);
                            }}
                            className={clsx('button-tab', {
                                ['active']: tabActiveProduct === tab.indexTab,
                            })}
                            color={'transparent'}>
                            {tab.title}
                        </Button>
                    ))}
                </Box>
              
            </Box>
            {tabActiveProduct === 0 && (
                renderTabMarket()
            )}
            {tabActiveProduct === 1 && (
                renderTabTrafficSources()
            )}
            {tabActiveProduct === 2 && (
                renderTabViewers()
            )}
        </Box>
    );
});

export default withStyles(styles)(ProductsComponent);