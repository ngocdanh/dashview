import { COLORS } from '@constants/colors';
import { faker } from '@faker-js/faker';
import SelectCustom from '@helpers/SelectCustom';
import TitleElement from '@helpers/TitleElement';
import { Box, Grid } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import {
    BarElement, CategoryScale, Chart as ChartJS, Legend, LinearScale, Title,
    Tooltip
} from 'chart.js';
import React from 'react';
import { Bar } from 'react-chartjs-2';
import styles from './styles';

interface TrafficChannelProps {
    title: string,
    classes?: any,
    fillterTrafficChannelSelect:string;
    handleSelectTrafficChannel: (value: React.ChangeEvent<HTMLInputElement>)=> void;
}
const dataSelectTime = [
    {
        label: 'Last 7 days',
        value: 'Last 7 days',
    },
    {
        label: 'Last 14 days',
        value: 'Last 14 days',
    },
    {
        label: 'Last 28 days',
        value: 'Last 28 days',
    },
];
function TrafficChannelComponent(props: TrafficChannelProps & WithStyles<typeof styles>) {
    const {
        title, 
        classes,
        fillterTrafficChannelSelect,
        handleSelectTrafficChannel, 
        ...otherProps
    } = props;
    ChartJS.register(
        CategoryScale,
        LinearScale,
        BarElement,
        Title,
        Tooltip,
        Legend
    );
    const labels = ['22', '23', '24', '25', '26', '27', '28'];
    const data = React.useMemo(() =>({
        labels,
        datasets: [
            {
                label: 'Direct',
                data: labels.map(() => faker.datatype.number({ min: 0, max: 15 })),
                backgroundColor: COLORS.blue,
                barPercentage: 0.7,

            },
            {
                label: 'Search',
                data: labels.map(() => faker.datatype.number({ min: 0, max: 15 })),
                backgroundColor: COLORS.orangeLight,
                barPercentage: 0.7,

            },
            {
                label: 'Market',
                data: labels.map(() => faker.datatype.number({ min: 0, max: 15 })),
                backgroundColor: COLORS.blueLight,
                barPercentage: 0.7,

            },
            {
                label: 'Social Media',
                data: labels.map(() => faker.datatype.number({ min: 0, max: 15 })),
                backgroundColor: COLORS.violetLight,
                barPercentage: 0.7,

            },
            {
                label: 'Other',
                data: labels.map(() => faker.datatype.number({ min: 0, max: 15 })),
                backgroundColor: COLORS.yellowLight,
                barPercentage: 0.7,
                borderRadius: '8px',
            },
        ],
    }),[fillterTrafficChannelSelect]);
    
    const options = {
        plugins: {
            legend: {
                containerID: 'legend-container',
                position: 'bottom',
                labels: {
                    pointStyle:'rectRounded',
                    usePointStyle:true,

                },
            },
            htmlLegend: {
              
            },
            title: {
                display: false,
            },
            crosshair: false,
        },
        responsive: true,
        scales: {
            x: {
                grid: {
                    display: false,
                    drawBorder: false,
                    drawOnChartArea: false,
                },
                stacked: true,
            },
            y: {
                stacked: true,
                grid: {
                    drawBorder: false,

                },
                ticks: {
                    stepSize: 10,
                    suggestedMin: 5,
                }
            },
        },

    };
    return (
        <Box className={classes.TrafficChannelComponent}>
            <Grid container
                className={classes.boxHeader}>
                <Grid item >
                    <TitleElement color={COLORS.grey7}
                        bg={COLORS.violetLight}>{title}</TitleElement>
                </Grid>
                <Grid item >
                    <SelectCustom data={dataSelectTime}
                        defaultValues="Last 7 days"
                        handleSelect={handleSelectTrafficChannel} />
                </Grid>
            </Grid>
            <Box className={classes.TrafficChannelChart}>
                <Bar options={options}
                    data={data} />
            </Box>
        </Box>
    );
}

export default withStyles(styles)(TrafficChannelComponent);