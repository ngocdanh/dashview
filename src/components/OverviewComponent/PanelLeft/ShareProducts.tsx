import React from 'react';
import { WithStyles, withStyles } from '@mui/styles';

import styles from './styles';
import { Box, Grid, colors } from '@mui/material';
import TitleElement from '@helpers/TitleElement';
import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { iconClock, iconFacebook, iconInstagram, iconPromtion, iconTwitterLight } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import { faker } from '@faker-js/faker';
import { 
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9
} from '@constants/imageAssets';
import moment from 'moment';
import {useNavigate } from 'react-router-dom';
import LabelColor from '@helpers/LabelColor';

interface IShareProducts {
    title: string;
    handleOpenModalProductDetail?: ()=> void,
}
interface IShareProductsItem {
    title: string;
    price: string;
    preview: any;
    time:string;
}
const listImage = [
    imageShop1,
    imageShop2,
    imageShop3,
    imageShop4,
    imageShop5,
    imageShop6,
    imageShop7,
    imageShop8,
    imageShop9,
];


function ShareProducts(props: IShareProducts & WithStyles<typeof styles>) {
    const Navigate = useNavigate();

    const { classes, title, handleOpenModalProductDetail, ...otherProps } = props;
    const socialNetworks = [
        {
            title: 'facebook',
            icon: iconFacebook,
            url: 'https://facebook.com/'
        },
        {
            title: 'twitter',
            icon: iconTwitterLight,
            url: 'https://twitter.com/'
        },
        {
            title: 'instagram',
            icon: iconInstagram,
            url: 'https://www.instagram.com/'
        },
    ];
    const listProductsDummyData = [
        {
            title: faker.lorem.sentence(4),
            price: `$${_.random(20, 100)}`,
            preview: faker.helpers.arrayElement(listImage),
            time: `${moment(faker.date.past()).format('MMM D, YYYY')} at ${moment(faker.date.past()).format('h:m A')}`
        },
        {
            title: faker.lorem.sentence(4),
            price: `$${_.random(20, 100)}`,
            preview: faker.helpers.arrayElement(listImage),
            time: `${moment(faker.date.past()).format('MMM D, YYYY')} at ${moment(faker.date.past()).format('h:m A')}`
        },
    ];
    const renderlistProducts = (data:IShareProductsItem) =>{
        const {
            title,price,preview,time
        } = data;
        return(

            <Grid item
                lg={6}
                xs={12}
                key={uuidv4()}>
                <Box className="product-item"
                    style={{cursor: 'pointer'}}
                    onClick={handleOpenModalProductDetail}>
                    <Box className='product-image'>
                        <img src={preview}
                            alt="" />
                    </Box>
                    <Box className="product-content"
                        display='flex'
                        justifyContent={'space-between'}
                    >
                        <Box className="product-content-left"
                            sx={{alignItems:'center'}}>
                            <Text baseSB1
                                color={COLORS.grey7}
                                className="product-title">
                                {title}
                            </Text>
                            <Box sx={{ marginLeft:'-2px',display:'flex',alignItems:'center',paddingTop:'8px'}}>
                                <IconSVG icon={iconClock}
                                    fill={COLORS.grey4}/>
                                <Text base2
                                    sx={{marginLeft:'8px'}}
                                    color={COLORS.grey4}>{time}</Text>
                            </Box>
                        </Box>
                        <Box className="product-content-right">
                            <Box className="product-price">
                                <LabelColor bg={COLORS.greenNgoc} 
                                    color={COLORS.grey5}
                                    title={price}/>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Grid>
        );
    };
    return (
        <Box className={classes.shareProductsPage}>
            <Grid container
                className={classes.boxHeader}>
                <Grid item >
                    <TitleElement color={COLORS.grey7}
                        bg={COLORS.blueLight}>{title}</TitleElement>
                </Grid>
                <Grid item >
                    <Box >
                        <ButtonTransparent isElement
                            onClick={()=>{ Navigate('/dashboard/promote');}}>
                            <Text button1
                            
                                sx={{marginRight:'4px'}}
                                color={COLORS.grey7}>
                                    Go to promote
                            </Text>
                            <IconSVG width={20}
                                height={20}
                                icon={iconPromtion}
                                fill={COLORS.grey4}/>
                        </ButtonTransparent>
                    </Box>
                </Grid>
            </Grid>
            <Box className="content">
                <Box className={classes.listShareProducts}>
                    <Grid container
                        spacing={2}
                        className={'product-list'}>
                        {!_.isEmpty(listProductsDummyData) && listProductsDummyData.map((data: IShareProductsItem)=> (
                            renderlistProducts(data)
                        ))}
                    </Grid>
                </Box>
                <Box className={classes.shareModal}>
                    <Text bodyM1
                        sx={{marginBottom:'16px'}}
                        color={COLORS.grey4}>
            50% of new customers explore products because the author sharing 
            the work on the social media network. Gain your earnings right now! 🔥
                    </Text>
                    <Grid container
                        spacing={3}
                        className={'social-list'}
                        justifyContent={'space-between'}>
                        {!_.isEmpty(socialNetworks) && socialNetworks.map((item: {icon: any, url: string, title: string})=> {
                            return (
                                <Grid item
                                    key={uuidv4()}
                                    lg={4}
                                    xs={12}>
                                    <ButtonTransparent isElement
                                        className='social-button'>
                                        <IconSVG width={24}
                                            height={24}
                                            sx={{marginRight:'4px'}}
                                            icon={item.icon}/>
                                        <Text button1
                                            color={COLORS.grey7}>
                                            {item.title}
                                        </Text>
                                    </ButtonTransparent>
                                </Grid>
                            );
                        })}
                    </Grid>
                </Box>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(ShareProducts);
