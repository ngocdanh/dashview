import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';

import { COLORS } from '@constants/colors';
import { iconArrowForward } from '@constants/imageAssets';
import faker from '@faker-js/faker';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import LabelColor from '@helpers/LabelColor';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Grid, IconButton } from '@mui/material';
import { isMobile } from '@utils/index';
import {
    CategoryScale, Chart as ChartJS, ChartOptions, Legend, LinearScale, LineElement, LineProps, PointElement, Title,
    Tooltip
} from 'chart.js';
import Crosshair from 'chartjs-plugin-crosshair';
import _ from 'lodash';
import moment from 'moment';
import { Line, ChartProps } from 'react-chartjs-2';
import { useNavigate } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';
interface ITotals {
    title: string;
    fillterTotalCustomersSelect:string;
    handleSelectTotalCustomers: (value: React.ChangeEvent<HTMLInputElement>)=> void;
    overviewUserDummyData?: {id: number, url: string, name: string}[];
}
interface ITotalsItem {
    customer: string;
    mode:string;
    numeral:string;
    datetime : string;

}
const dataSelectTime = [
    {
        label: 'Last 28 days',
        value: 'Last 28 days',
    },
    {
        label: 'Last 14 days',
        value: 'Last 14 days',
    },
    {
        label: 'Last 7 days',
        value: 'Last 7 days',
    },
];

function TotalCustomers(props: ITotals & WithStyles<typeof styles>) {
    const { 
        classes, 
        title, 
        overviewUserDummyData,
        fillterTotalCustomersSelect,
        handleSelectTotalCustomers, 
        ...otherProps 
    } = props;
    const renderList =(data:ITotalsItem) =>{
        const {    
            customer,
            mode,
            numeral,
            datetime
        } = data;
        return(
           
            <Box className={classes.itemBox}
                key={uuidv4()}
                sx={{
                    borderRadius: '12px',
              
                }}>
                <Text h4
                    color={COLORS.grey6}
                >
                    {customer} customers
                </Text>
                <LabelColor 
                    sx={{marginLeft:'-14px',      
                    }}
                    mode={mode}
                    titleMode={datetime}
                    numeral={numeral}
                />

            </Box>
        );
    };
    const listTotalCustomersDummyData =React.useMemo(() =>(
        [ {
            customer: _.random(1000, 3000, false),
            mode: faker.datatype.boolean() ?'up':'down',
            numeral: `${_.round(_.random(10, 100, true), 1)}%`,
            titleMode: 'this week',
            datetime: `vs. ${moment(faker.date.past()).format( 'MMM D, YYYY')}`
        }]),[fillterTotalCustomersSelect]);
    ChartJS.register(
        CategoryScale,
        LinearScale,
        Title,
        Tooltip,
        Legend,
        PointElement,
        LineElement,
        Crosshair
        
    );
    const labels =faker.date.betweens('2020-01-01T00:00:00.000Z', '2022-01-01T00:00:00.000Z', 6);
    const data = React.useMemo(() =>({
        labels:labels.map((date:any)=> moment(date).format( 'MMM D')),
        datasets:[
            {
                label: 'Customers',
                data: labels.map(() => faker.datatype.number({ min:100,max:1600})),
                lineTension: 0.4,
                borderColor: COLORS.blue,
                borderWidth: 4,
                pointRadius: 0,
                hoverRadius:4,
                pointColor: COLORS.blue,
                backgroundColor: COLORS.blue,
                hoverBackgroundColor: COLORS.blue
            },
        ],
    }),[fillterTotalCustomersSelect]);
    const newCustomersCount =React.useMemo(()=>(_.random(1000, 3000, false)),[fillterTotalCustomersSelect]);
    const options: ChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false,
            },
            tooltip: {
                // mode: 'nearest',
                intersect: false,
                positioners:'nearest',
                boxPadding:4,
                titleSpacing:4,
            },

            crosshair: {
                line: {
                    color: COLORS.grey4,
                    width: 1,
                    dashPattern:[10,10]
                },
                snapping:{
                    enabled:true
                },
                sync: {
                    enabled: true,
                    group: 1,
                    suppressTooltips: false
                },
                zoom: {
                    enabled: true,
                    zoomboxBackgroundColor: 'rgba(66,133,244,0.2)',
                    zoomboxBorderColor: '#48F',
                    zoomButtonText: 'Reset Zoom',
                    zoomButtonClass: 'reset-zoom',
                }
            }
        },
        interaction: { mode: 'index' },
        hover: {
            mode: 'index' as const,
            intersect: false,
        },
        scales: {
            x: {
                grid: {
                    display: false,
                    drawBorder: false,
                    drawOnChartArea: false,
                },
            },
            y: {
                grid: {
                    drawBorder: false,

                },
                ticks: {
                    stepSize: 300,
                    callback: (label:any, index: any, labels:any) => {
                        return label;
                    }
                }
            },
        }
    } as ChartOptions;
    const Navigate = useNavigate();


    return (
        <Box component={'div'}
            className={classes.totalCustomers}>
            <Grid container
                className={classes.boxHeader}>
                <Grid item >
                    <TitleElement color={COLORS.grey7}
                        bg={COLORS.orangeLight}>{title}</TitleElement>
                </Grid>
                <Grid item >
                    <SelectCustom data={dataSelectTime}
                        defaultValues="Last 28 days"
                        handleSelect={handleSelectTotalCustomers} />
                </Grid>
            </Grid>
            <Box className={classes.totalInfo}>
                {!_.isEmpty(listTotalCustomersDummyData) && listTotalCustomersDummyData.map((data: any)=> (
                    renderList(data)
                ))}
            </Box>
            <Box component={'div'}
                className={classes.Chart}>
                <Line
                    data={data}
                    options={options}
                    redraw
                />
            </Box>
            <Box className={classes.contentTabLeft}
                component="div">
                <Box className="box-message">
                    <Text className="text"
                        style={{marginRight: 5}}>
              Welcome <b>{newCustomersCount} customers</b> with a personal message 😎
                    </Text>
                    <ButtonTransparent>{isMobile ? 'Send': 'Send message'}</ButtonTransparent>
                </Box>
                <Grid className="box-users"
                    container>
                    {!_.isEmpty(overviewUserDummyData) &&
                    overviewUserDummyData?.map(
                        (
                            item: {
                                id: number;
                                name: string;
                                url: string;
                            },
                            index: number
                        ) => (
                            <Grid key={uuidv4()}
                                className="user"
                                item
                                lg={3}
                                xs={6}>
                                <Box>
                                    <img alt=""
                                        src={item.url} />
                                    <Text caption1
                                        center
                                        color={COLORS.grey6}>
                                        {item.name}
                                    </Text>
                                </Box>
                            </Grid>
                        )
                    )}
                    <Grid className="view-all"
                        item
                        lg={3}
                        xs={6}>
                        <ButtonTransparent isElement
                            className='button-transparent'
                            onClick={()=>{ Navigate('/dashboard/customer-list');}}>

                            <IconButton className="box-arrow">
                                <img alt=""
                                    src={iconArrowForward} />
                            </IconButton>
                            <Text
                                caption1
                                center
                                className="view-all-text"
                                color={COLORS.grey6}
                            >
                          View all
                            </Text>
                        </ButtonTransparent>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(TotalCustomers);
