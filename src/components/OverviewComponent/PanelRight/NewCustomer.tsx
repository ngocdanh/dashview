import React from 'react';
import {withStyles,WithStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid, IconButton } from '@mui/material';
import clsx from 'clsx';
import TitleElement from '@helpers/TitleElement';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import { Doughnut } from 'react-chartjs-2';
import { Chart as ChartJS,
    ArcElement,
    CategoryScale,
    LinearScale,
    Filler,
    Legend,
    Title,
    Tooltip,
    SubTitle } from 'chart.js';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';

interface NewCustomerProps {
    title: string,
    classes?: any,
}

function NewCustomerComponent(props: NewCustomerProps & WithStyles<typeof styles>) {
    const {title, classes, ...otherProps} = props;
    ChartJS.register(
        ArcElement,
        CategoryScale,
        LinearScale,
        Filler,
        Legend,
        Title,
        Tooltip,
        SubTitle
    );
    const chartData = [20, 80];
    const showData = chartData[0] + '%';
    const options = {
        cutoutPercentage: 20,
        suppressTooltips: true,
        responsive: true,
        scales: {
        },
        plugins: {
            legend: {
                position: 'bottom' as const,
                display: false,
            },
            tooltip: {
                intersect: false,
                boxPadding:4,
                titleSpacing:4,
            },
            crosshair: false
        }
    };
    const labels =['New customer', 'Returning customer', ];
    const data = React.useMemo(()=>({
        labels,
        datasets: [
            {
                labels: labels,
                data: labels.map(() => _.round(_.random(100, 8000, true), 0)),
                backgroundColor: [COLORS.greenNgoc,COLORS.violetLight],
                // hoverBackgroundColor: [COLORS.violetLight,COLORS.greenLight,COLORS.blueLight],
                pointStyle: 'circle',
                cutout:'80%',
            }
        ]
    }),[]);
    const listCustomerRender = (dataset:any) =>{

        return(
            dataset.data.map((item:number,index:number) => (
                <Box className={classes.CustomerItem}
                    key={uuidv4()}
                    component={'div'}
                    sx={{display:'flex', alignItems:'center',justifyContent: 'space-between'}}
                >
                    <Box sx={{
                        borderRadius:'4px',width:16,height:16,background: dataset.backgroundColor[index],marginRight:'8px'
                    }}></Box>
                    <Text captionM2
                        color={COLORS.grey4}
                        sx={{padding:'8px 0 4px'}}>
                        {dataset.labels[index]}
                    </Text>
                    
                </Box>
            ))
        );
    };
    return (
        <Box className={classes.NewCustomerComponent}>
            <TitleElement bg={COLORS.blueLight}>
                {title}
            </TitleElement>
            <Box
                className={classes.DoughnutChart}
            >
                <Doughnut 
                    data={data}
                    options={options}
                />
            </Box>
            <Box className={classes.listCustomer}
                sx={{ display: 'flex',alignItems:'center',justifyContent: 'space-between'}}>
                { data.datasets[0] && listCustomerRender(data.datasets[0]) } 
            </Box>
        </Box>
    );
}

export default withStyles(styles)(NewCustomerComponent);