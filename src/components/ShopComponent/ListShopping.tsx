import React, { ReactNode } from 'react';
import {WithStyles, withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Divider, Grid, Hidden, IconButton } from '@mui/material';
import Button from '@helpers/Button';
import { v4 as uuidv4 } from 'uuid';
import SelectCustom from '@helpers/SelectCustom';
import IconSVG from '@helpers/IconSVG';
import { iconFilterLight, iconStarFilled, iconEdit, iconArrowForward, iconTrash } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import { iconFilterLight, iconStarFilled } from '@constants/imageAssets';
import Button from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import { Box, Divider, Grid, IconButton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import clsx from 'clsx';
import _ from 'lodash';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';
import ModalFilter from '@helpers/ModalFilter';
import { IinitialDefaultValueFilter } from '@containers/Admin/CustomerListContaier/types';

interface ShopProps {
    titlePage?: string,
    dataListShop: any,
    tabActive: number,
    pagination: number,
    handleChangeTabActive: (e: number)=> void,
    handleChangePaginationProducts: ()=> void,
    handleChangeStatusFLowClick:(id:string)=>void,
    handleSelectFillterShop:(e: React.ChangeEvent<HTMLInputElement>) => void,
    handleDeleteProducts: (id:string) => void,
    isLoadingMore:boolean,
    dataListFollower: any,
    dataListFollowing: any,
    handleOpenModalProductDetail?: ()=> void,
    fillterShop:string,
    handleSearchShop: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleOpenModalFilter: () => void;
    handleCloseModalFilter: ()=> void;
    handleSelectRows:(selectedRow:any) => void;
    openModalFilter: boolean,
    refFilter: HTMLButtonElement;
    ContentModal: () => ReactNode;
    initialDefaultValueFilter: IinitialDefaultValueFilter,
    filters?: IinitialDefaultValueFilter
}

interface IDataProducts {
    id:string,
    title: string,
    price: string,
    rating: string,
    totalRating: string,
    preview: any,
    url: '',
}
interface IDataFollowers {
    id:string,
    name: string,
    totalProduct:number,
    isMessage:boolean,
    isFollowing:boolean,
    totalFollower: number,
    preview: string,
    listProduct: string[],
}


function ShopComponent(props: ShopProps  & WithStyles<typeof styles>) {
    const {titlePage,
        classes,
        dataListShop, 
        handleChangeTabActive, 
        tabActive, 
        handleChangePaginationProducts,
        handleDeleteProducts,
        handleChangeStatusFLowClick,
        pagination,
        isLoadingMore,
        dataListFollower,
        dataListFollowing,
        handleOpenModalProductDetail,
        fillterShop,
        handleSearchShop,
        handleOpenModalFilter,
        handleCloseModalFilter,
        handleSelectRows,
        openModalFilter,
        refFilter,
        ContentModal,
        initialDefaultValueFilter,
        filters,
        handleSelectFillterShop,
        ...otherProps} = props;

    const listTitle = [
        {
            title: 'Products',
            indexActive: 0,
        },
        {
            title: 'Followers',
            indexActive: 1,
        },
        {
            title: 'Following',
            indexActive: 2,
        },
    ];

    const dataFilters = [
        {
            value: 'mostRecent',
            label: 'Most recent'
        },
        {
            value: 'mostNew',
            label: 'Most new'
        },
        {
            value: 'mostPopular',
            label: 'Most popular'
        },
    ];

    const renderListProducts = () => {
        return (
            !_.isEmpty(dataListFollower) && dataListShop.slice(0, 9*pagination).map((product: IDataProducts)=> (
                <Grid key={product.id}
                    item
                    xs={12}
                    lg={4}>
                    <Box className="product-item">
                        <Box className='product-image'
                            style={{cursor: 'pointer'}}
                            onClick={handleOpenModalProductDetail}
                            alignItems={'center'}>
                            <img src={product.preview}
                                alt="" />
                            <Box component={'div'}
                                className="box-icon"
                                sx={{display:'flex',alignItems:'center'}}>
                                <IconButton className='icon'
                                    sx={{backgroundColor:COLORS.white}}
                                >
                                    <IconSVG small
                                        icon={iconArrowForward}
                                        fill={COLORS.grey4}
                                    />
                                </IconButton>
                                <IconButton className='icon'
                                    sx={{backgroundColor:COLORS.white}}
                                    onClick ={(event: React.MouseEvent<HTMLElement>) =>{
                                        event.stopPropagation();
                                        handleDeleteProducts(product.id);
                                    }}
                                >
                                    <IconSVG small
                                        icon={iconTrash}
                                        fill={COLORS.grey4}
                                    />
                                </IconButton>
                            </Box>
                        </Box>
                        <Box className="product-content"
                            display='flex'
                            justifyContent={'space-between'}
                        >
                            <Box className="product-content-left">
                                <Text baseSB1
                                    color={COLORS.grey7}
                                    className="product-title">
                                    {product.title}
                                </Text>
                                <Box display={'flex'}
                                    alignItems={'center'}>
                                    <IconSVG icon={iconStarFilled}
                                        fill={COLORS.yellowLight}/>
                                    <Text base2
                                        color={COLORS.grey7}>{product.rating}</Text>
                                    <Text base2
                                        color={COLORS.grey4}>({product.totalRating})</Text>
                                </Box>
                            </Box>
                            <Box className="product-content-right">
                                <Box className="product-price">
                                    <Text baseB1
                                        color={COLORS.grey7}>${product.price}</Text>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Grid>
            ))
        );
    };
    const renderListFollowers = () => {

        return (
            !_.isEmpty(dataListFollower) && dataListFollower.slice(0, 9*pagination).map((product: IDataFollowers)=> (

                <React.Fragment key={product.id}>
                    <Box className="follower-item"
                        display={'flex'}
                        justifyContent='space-between'>
                        <Box className='follower-left'>
                            <Box display='flex'>
                                <Box className="follower-user">
                                    <img src={product.preview}
                                        width={80}
                                        height={80}
                                        alt="" />
                                </Box>
                                <Box className={'follower-content'}>
                                    <Text titleSB1
                                        color={COLORS.grey7}>{product.name}</Text>
                                    <Box className='follower-data'
                                        display='flex'>
                                        <Text caption1
                                            color={COLORS.grey4}>{product.totalProduct} products</Text>
                                        <Text caption1
                                            color={COLORS.grey4}>{product.totalFollower} followers</Text>
                                    </Box>
                                    <Box display='flex'>
                                        <ButtonTransparent onClick={() =>{handleChangeStatusFLowClick(product.id);}}>{product.isFollowing ? 'Unfollow': 'Follow'}</ButtonTransparent>
                                        {product.isMessage && <Button color={'primary'}>Message</Button>}
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                        <Box className='follower-right'
                            display={'flex'}>
                            {!_.isEmpty(product.listProduct) && product.listProduct.map((pic)=> (
                                <Box className='follower-img'
                                    key={uuidv4()}>
                                    <img src={pic}
                                        alt=""
                                        className="cursor-pointer" />
                                </Box>
                            ))}
                        </Box>
                    </Box>
                    <Divider/>
                </React.Fragment>
            ))
        );
    };
    const renderListFollowing = () => {
        return (
            !_.isEmpty(dataListFollowing) && dataListFollowing.slice(0, 9*pagination).map((product: IDataFollowers)=> (

                <React.Fragment key={product.id}>
                    <Box className="follower-item"
                        
                        display={'flex'}
                        justifyContent='space-between'>
                        <Box className='follower-left'>
                            <Box display='flex'>
                                <Box className="follower-user">
                                    <img src={product.preview}
                                        width={80}
                                        height={80}
                                        alt="" />
                                </Box>
                                <Box className={'follower-content'}>
                                    <Text titleSB1
                                        color={COLORS.grey7}>{product.name}</Text>
                                    <Box className='follower-data'
                                        display='flex'>
                                        <Text caption1
                                            color={COLORS.grey4}>{product.totalProduct} products</Text>
                                        <Text caption1
                                            color={COLORS.grey4}>{product.totalFollower} followers</Text>
                                    </Box>
                                    <Box display='flex'>
                                        <ButtonTransparent onClick={() =>{handleChangeStatusFLowClick(product.id);}}>{product.isFollowing ? 'Unfollow': 'Follow'}</ButtonTransparent>
                                        {product.isMessage && <Button color={'primary'}>Message</Button>}
                                    </Box>
                                </Box>
                            </Box>
                        </Box>
                        <Box className='follower-right'
                            display={'flex'}>
                            {!_.isEmpty(product.listProduct) && product.listProduct.map((pic)=> (
                                <Box className='follower-img'
                                    key={uuidv4()}>
                                    <img src={pic}
                                        alt=""
                                        className="cursor-pointer" />
                                </Box>
                            ))}
                        </Box>
                    </Box>
                    <Divider/>
                </React.Fragment>
            ))
        );
    };

    const disabled = pagination*9 > dataListShop?.length;
    return (
        <Box className={classes.listShoppingComponent}>
            <Box display='flex'
                alignItems={'center'}
                flexWrap='wrap'
                justifyContent={'space-between'}> 
                <Box className="list-tab"
                    display='flex'
                    marginBottom={1}>
                    {listTitle.map((item: any)=> (
                        <Button key={uuidv4()}
                            color='transparent'
                            onClick={()=>handleChangeTabActive(item.indexActive)}
                            className={clsx('tab-item', 
                                {['active']: tabActive === item.indexActive})}>
                            {item.title}
                        </Button>
                    ))}
                </Box>
                <Box className="filters">
                    <SelectCustom data={dataFilters}
                        handleSelect={handleSelectFillterShop}
                        defaultValues='mostRecent'/>
                    <Hidden only='xs'>
                        <ModalFilter
                            openModal={openModalFilter}
                            defaultValues={initialDefaultValueFilter}
                            handleCloseModal={handleCloseModalFilter}
                            handleOpenModal={handleOpenModalFilter}
                            title={`Showing ${_.size(dataListShop)} of 100 products`}
                        >
                            {filters && ContentModal()}
                        </ModalFilter>
                    </Hidden>
                </Box>
            </Box>
            <Grid container
                spacing={2}
                className={'product-list'}>
                {tabActive === 0 && renderListProducts()}
                
            </Grid>
            <Box className='follower-list'>
                {tabActive === 1 && renderListFollowers()}
                {tabActive === 2 && renderListFollowing()}
            </Box>
            {!disabled && (
                <Box className={classes.loadMore}>
                    <ButtonTransparent isLoading={isLoadingMore}
                        disabled={disabled}
                        onClick={handleChangePaginationProducts}>Load More</ButtonTransparent>
                </Box>
            )}
            
        </Box>
    );
}

export default withStyles(styles)(ShopComponent);