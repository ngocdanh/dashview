import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    ShoppingPage: {
        zIndex: 20,
        position: 'relative',
        borderRadius: '8px',
        backgroundColor: COLORS.white,
        padding: 24,
        marginTop: -60,
        [theme.breakpoints.down('sm')]: {
            marginTop: -300,
        },
    },
    listShoppingComponent: {
        '& .filters': {
            display: 'flex',
            alignItems: 'center',
            [theme.breakpoints.down('sm')]: {
                width: '100%',
                marginBottom: -10,
                marginTop: 10,
                '& .select-custom': {
                    width: '100%',
                },
            },
            '& button': {
                borderRadius: '8px',
            },
            '& .box-all-filter': {
                width: 40,
                height: 40,
                border: `2px solid ${COLORS.grey3}`,
                borderRadius: '8px',
                marginLeft: 16,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                '&:hover .icon-svg': {
                    color: COLORS.grey7,
                }
            }
        },
        '& .box-icon':{
            position: 'absolute',
            top: '50%',
            left: '50%',
            zIndex: 2,
            opacity: 0,
            visibility: 'hidden',
            transform:'translate(-50%,-50%)',
            gap: 'calc(34px - 16px)',
            transition: 'all .3s ease',
            '& .icon':{
                transition: 'all .3s ease',
                '&:hover':{
                    backgroundColor: COLORS.white,
                    fill: COLORS.blue,
                },
                '&:hover .icon-svg': {
                    color: COLORS.blue,
                },
            }
        },
        '& .product': {
            '&-list': {
                marginTop: 32,
            },
            '&-item': {
                marginBottom: 32,
                [theme.breakpoints.down('sm')]: {
                    marginBottom: 10,
                }
            },

            '&-content': {
                marginTop: 16,
                '&-left': {

                },
                '&-right': {
                    '& .product-price': {
                        borderRadius: '6px',
                        height: 32,
                        padding: '4px 8px',
                        backgroundColor: COLORS.greenLight,
                        textAlign: 'center',
                        '& .text': {
                            marginRight: 0,
                        }
                    }
                },
                '& .icon-svg': {
                    marginRight: 8,
                },
                '& .text': {
                    marginRight: 8,
                }
            },
            '&-image': {
                width: '100%',
                overflow: 'hidden',
                borderRadius: '16px',
                position: 'relative',
                '&::before': {
                    content: '""',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    position: 'absolute',
                    backgroundColor: 'transparent',
                    transition: 'all .3s ease',
                    zIndex: -1,
                },
                '& img': {
                    width: '100%',
                    height: '100%',
                    objectFit: 'contain',
                },
                '&:hover::before': {
                    zIndex: 1,
                    backgroundColor: '#46464661',
                },
                '&:hover .box-icon': {
                    opacity: 1,
                    visibility: 'visible',
                }
            },
            '&-title': {
                marginBottom: 8,
                color: COLORS.grey7
            },

        },
        '& .list-tab': {
            '& .tab-item': {
                '&.active': {
                    backgroundColor: COLORS.grey3
                }
            }
        },

        //followers
        '& .follower-list': {
            marginBottom: 32,
        },
        '& .follower-list .follower': {
            '&-item': {
                width: '100%',
                marginTop: 25,
                marginBottom: 25,
            },
            '&-user': {
                marginRight: 16,
                width: 80,
                height: 80,
                borderRadius: '50%',
                overflow: 'hidden',
                '& img': {
                    width: '100%',
                    height: '100%',
                    objectFit: 'contain',
                }
            },
            '&-content': {
                '& .button-transparent': {
                    marginRight: 16,
                },
            },
            '&-data': {
                marginTop: 4,
                marginBottom: 16,
                '& .text:nth-child(1)': {
                    marginRight: 12,
                    paddingRight: 12,
                    borderRight: `2px solid ${COLORS.grey2}`
                },
            },
            '&-right': {
                display: 'flex',
                '& .follower-img': {
                    height: 116,
                    width: 148,
                    marginRight: 12,
                    borderRadius: 12,
                    overflow: 'hidden',
                    '& img': {
                        width: '100%',
                        height: '100%',
                        objectFit: 'cover',
                        display: 'inline-block',
                        
                    }
                }
            }
        },
    },
    dividerStyle: {
        margin: '48px 0 !important',
        [theme.breakpoints.down('sm')]: {
            margin: '24px 0 !important',
        }
    },
    headerPageComponent: {
        '& .header-left': {
            '&-image': {
                marginRight: 16,
                position: 'relative',
                '& img': {
                    borderRadius:'50%'
                },
                '& .button-add': {
                    position: 'absolute',
                    bottom: 6,
                    right: 0,
                    border: `2px solid ${COLORS.white}`,
                    backgroundColor: COLORS.blue,
                    width: 24,
                    height: 24,
                    borderRadius: '24px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    '& svg': {
                        width: 16,
                        height: 16,
                    }
                }
            },
            '&-content': {  
                '& .title': {
                    marginBottom: 8,
                }
            }
        },
        '& .header-right': {
            [theme.breakpoints.down('sm')]: {
                marginTop: 20,
            },
            '& .list-icon': {
                '& .icon-social': {
                    marginRight: 32,
                    display: 'inline-block',
                    '&:hover .icon-svg': {
                        color: COLORS.grey7,
                    }
                }
            }
        },
       
    },
    contentModalFilter: {
        '& .box-input-search': {
            padding: '10px 0',
        },
        '& .box-sortBy': {
            padding: '10px 0',
        },
        '& .list-checkbox': {
            padding: '10px 0',
            '& .list-item': {
                marginTop: 6,
                marginBottom: 6,
                '& .box-item': {
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    padding: '6px 0',
                    position: 'relative',
                    '& .checkbox-custom': {
                        position: 'absolute',
                        width: '100%',
                        textAlign: 'right',
                    }
                }
            }
        },
        '& .box-price': {
            padding: '10px 0',
            '& .progress-bar': {
                color: COLORS.blue,
                marginTop: 40,
            },
            '& .MuiSlider-track': {
                border: 'unset !important',
            },
            '& .MuiSlider-rail': {
                backgroundColor: `${COLORS.grey3} !important`,
            },
            '& .MuiSlider-valueLabel': {
                background: COLORS.grey6,
                color: COLORS.white,
                borderRadius: '4px',
                padding: '2px 8px',
                height: 28,
                minWidth: 40,
                boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1), inset 0px 0px 1px #000000',
                '& .MuiSlider-valueLabelLabel': {
                    color: COLORS.white,
                    fontWeight: 600,
                    fontSize: 12,
                    lineHeight: 24,
                }
            },
            '& .MuiSlider-thumb': {
                width: '16px !important',
                height: '16px !important',
                '&:after': {
                    width: '6px !important',
                    height: '6px !important',
                    background: COLORS.white,
                }
            }
        }
    },
    loadMore: {
        display: 'table',
        margin: ' 0 auto 32px auto',
    }
});

export default styles;