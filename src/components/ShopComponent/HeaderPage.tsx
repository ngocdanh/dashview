import { COLORS } from '@constants/colors';
import { iconInstagram, iconPinterestLight, iconTwitter, imgUserDefault } from '@constants/imageAssets';
import Button from '@helpers/Button';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';

interface ShopProps {
    titlePage?: string,
    userInfo?: any
}

function HeaderPage(props: ShopProps  & WithStyles<typeof styles>) {
    const {classes, userInfo, ...otherProps} = props;
    return (
        <Box className={classes.headerPageComponent}>
            <Box
                display={'flex'}
                justifyContent={'space-between'}
                flexWrap='wrap'
                alignItems={'flex-start'}
            >
                <Box className="header-left"
                    display={'flex'}
                    alignItems='center'>
                    <Box className={'header-left-image'}>
                        <img src={userInfo?.photoURL || imgUserDefault}
                            width={80}
                            height={80}
                            style={{borderRadius: '50%'}}
                            alt="" />
                        {/* <IconButton className='button-add'>
                            <IconSVG icon={iconAdd}
                                fill={COLORS.white}
                                width={16}
                                height={16} />
                        </IconButton> */}
                    </Box>
                    <Box className='header-left-content'>
                        <Text h4
                            className="title"
                            color={COLORS.grey7}>
                            {userInfo?.displayName}
                        </Text>
                        <Text titleSB1
                            className={'description'}
                            color={COLORS.grey4}>Dream big. Think different. Do great!</Text>
                    </Box>
                </Box>
                <Box className="header-right"
                    display={'flex'}
                    alignItems={'center'}
                >
                    <Box className="list-icon">
                        <a href="https://twitter.com/"
                            className={'icon-social'}
                            target={'_blank'}
                            rel="noreferrer"><IconSVG icon={iconTwitter}
                                fill={COLORS.grey4}/></a>
                        <a href="https://www.instagram.com/"
                            className={'icon-social'}
                            target={'_blank'}
                            rel="noreferrer"><IconSVG icon={iconInstagram}
                                fill={COLORS.grey4}/></a>
                        <a href="https://www.pinterest.com/"
                            className={'icon-social'}
                            target={'_blank'}
                            rel="noreferrer"><IconSVG icon={iconPinterestLight}
                                fill={COLORS.grey4}/></a>
                       
                    </Box>
                    <Button color='primary'>Follow</Button>
                </Box>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(HeaderPage);