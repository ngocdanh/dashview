import React from 'react';
import { withStyles } from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import CustomerList from './CustomerList';
import Message from './Message';
import ModalCustom from '@helpers/ModalCustom';
import { COLORS } from '@constants/colors';
import { Box, Divider, Slider } from '@mui/material';
import Input from '@helpers/Input';
import Text from '@helpers/Text';
import { v4 as uuidv4 } from 'uuid';
import CheckboxCustom from '@helpers/CheckboxCustom';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import Button from '@helpers/Button';
import { Controller, useForm } from 'react-hook-form';
import { IinitialDefaultValueFilter } from '@containers/Admin/CustomerListContaier/types';
import SearchField from '@helpers/FormField/SearchField';
import SelectField from '@helpers/FormField/SelectField';
import CheckboxField from '@helpers/FormField/CheckboxField';
import {useEffect} from 'react';

interface CustomerListProps {
    title: string;
    classes:any;
    titlePage?: string;
    handleChangeTabActiveCustomer: (index: number)=> void;
    tabActiveCustomer: number;
    dataTableNewCustomers: any;
    dataTableActiveCustomers: any;
    handleSearchCustomer: (e: React.ChangeEvent<HTMLInputElement>) => void;
    selectedRow:any;
    handleDeleteCustomers:(selectedRow:any) => void;
    handlePublishCustomers:(selectedRow:any) => void;
    handleSelectRows:(selectedRow:any) => void;
    searchCustomer: string;
    openModalFilter: boolean;
    handleOpenModalFilter: () => void;
    handleCloseModalFilter: () => void;
    refFilter: HTMLButtonElement,
    positionModal: DOMRect,
    handleChangeProgressBarPrice: (event: Event, newValue: number | number[]) => void,
    handleResetProgressBarPrice: () => void,
    progressBarPrice: number[],
    initialDefaultValueFilter: IinitialDefaultValueFilter,
    handleChangeFilters: (data: IinitialDefaultValueFilter) => void,
    filters: IinitialDefaultValueFilter,
}

function CustomerListComponent(props: CustomerListProps) {
    const { title, titlePage,classes, openModalFilter,
        handleOpenModalFilter, handleCloseModalFilter, 
        refFilter, positionModal, 
        handleChangeProgressBarPrice,
        handleResetProgressBarPrice,
        progressBarPrice,
        initialDefaultValueFilter,
        handleChangeFilters, 
        filters,
        ...otherProps } = props;
    const {reset, control, handleSubmit, watch, setValue, getValues,register} = useForm({
        defaultValues: initialDefaultValueFilter,
    });

    const handleCheckAllShowing = () => {
        // console.log('handleCheckAllShowing');
    };

    const onSubmit = (data: IinitialDefaultValueFilter) => {
        handleChangeFilters(data);
    };

    useEffect(()=> {
        reset(filters);
    }, [filters, reset]);

    const dataSelectSort = [
        {
            label: 'Featured',
            value: 'Featured',
        },
        {
            label: 'Last',
            value: 'Last',
        },
        {
            label: 'New',
            value: 'New',
        },
    ];

    const dataListCheckbox = [
        {
            label: 'All Products',
            value: 'All Products',
        },
        {
            label: 'UI kit',
            value: 'UI kit',
        },
        {
            label: 'Illustration',
            value: 'Illustration',
        },
        {
            label: 'WireFrame kit',
            value: 'WireFrame kit',
        },
        {
            label: 'Icons',
            value: 'Icons',
        },
    ];

    const renderContentModalFilter = () => {
        return (
            <form autoComplete='false'
                onSubmit={handleSubmit(onSubmit)}> 
                <Box className={classes.contentModalFilter}>
                
                    <Box className="box-input-search">
                        <SearchField 
                            placeholder='Search for products'
                            control={control}
                            name='search'
                        />
                    </Box>
                
                    <Box className="box-sortBy">
                        <Text base2
                            color={COLORS.grey4}>
                    Sort by
                        </Text>
                        <SelectField 
                            style={{
                                marginTop: 12,
                            }}
                            data={dataSelectSort}
                            control={control}
                            name='sort_by'
                        />
                    </Box>
                
                    <Box className="list-checkbox">
                        <Text base2
                            color={COLORS.grey4}>Showing</Text>
                        <Box
                            className='list-item'>
                            {
                                dataListCheckbox.map((checkbox: {
                                    label: string,
                                    value: string,
                                }, index: number) => (
                                    <Box key={uuidv4()}
                                        className='box-item'>
                                        <Text baseB1
                                            color={COLORS.grey7}>{checkbox.label}</Text>
                                        <CheckboxField control={control}
                                            index={index}
                                            handleCheckAllShowing={handleCheckAllShowing}
                                            name={`showing[${index}]`}
                                            className={'cursor-pointer'}/>
                                    </Box>
                                ))
                            }
                        </Box>
                    
                        <Box className="box-price">
                            <Text base2
                                color={COLORS.grey4}>
                        Price
                            </Text>
                            <Box className='price-select'>
                                <Controller

                                    control={control}
                                    name="price"
                                    defaultValue={progressBarPrice}

                                    render={({ field: { value, ...field } }) => (
                                        <Slider
                                            classes={{
                                                root: 'progress-bar',
                                            }}
                                            getAriaLabel={() => 'Minimum distance shift'}
                                            getAriaValueText={(val) => `$${val}`}
                                            {...field}
                                            value={value}
                                            // onChangeCommitted={(event: Event, newValue: number | number[]) => {
                                            //     handleChangeProgressBarPrice(event, newValue);
                                            // }}
                                            max={progressBarPrice[1]}
                                            valueLabelFormat={(val)=>`$${val}`}
                                            valueLabelDisplay="on"
                                            disableSwap
        
                                        />
                                    )}
                                />

                            </Box>
                            <Box className="group-button"
                                display={'flex'}
                                justifyContent={'flex-end'}>
                                <Box display={'flex'}>
                                    <ButtonTransparent style={{marginRight: 12}}
                                        onClick={() => {
                                            reset(initialDefaultValueFilter);
                                            handleCloseModalFilter();
                                            handleSubmit(onSubmit);
                                        }}>Reset</ButtonTransparent>
                                    <Button color='primary'
                                        onClick={() =>handleCloseModalFilter()}
                                        type='submit'>Apply</Button>
                                </Box>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </form>
        );
    };
    
    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}>
            <CustomerList title={'Customer'}
                handleOpenModalFilter={handleOpenModalFilter}
                handleCloseModalFilter={handleCloseModalFilter}
                openModalFilter={openModalFilter}
                refFilter={refFilter}
                ContentModal={renderContentModalFilter}
                initialDefaultValueFilter={initialDefaultValueFilter}
                filters={watch()}
                {...otherProps} />
            <Message title={'Message'}
                {...otherProps} />
        </Page>
    );
}
export default withStyles(styles)(CustomerListComponent);
