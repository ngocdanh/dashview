import { COLORS } from '@constants/colors';
import { iconFilterLight } from '@constants/imageAssets';
import { IinitialDefaultValueFilter } from '@containers/Admin/CustomerListContaier/types';
import Button from '@helpers/Button';
import IconSVG from '@helpers/IconSVG';
import Input from '@helpers/Input';
import LabelColor from '@helpers/LabelColor';
import ModalFilter from '@helpers/ModalFilter';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Hidden, IconButton } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import _ from 'lodash';
import React, { ReactNode, useMemo } from 'react';
import { Column } from 'react-table';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';

interface ICustomerList {
    title: string,
    handleChangeTabActiveCustomer: (index: number)=> void;
    tabActiveCustomer: number;
    dataTableNewCustomers: any;
    dataTableActiveCustomers: any;
    handleSearchCustomer: (e: React.MouseEvent<HTMLInputElement>) => void;
    searchCustomer: string;
    handleOpenModalFilter: () => void;
    handleCloseModalFilter: ()=> void;
    selectedRow:any;
    handleSelectRows:(selectedRow:any) => void;
    openModalFilter: boolean,
    refFilter: HTMLButtonElement;
    ContentModal: () => ReactNode;
    initialDefaultValueFilter: IinitialDefaultValueFilter,
    filters?: IinitialDefaultValueFilter
}

function CustomerList(props: ICustomerList & WithStyles<typeof styles>) {
    const { classes, title,
        handleSearchCustomer, 
        searchCustomer, 
        tabActiveCustomer,
        handleChangeTabActiveCustomer, 
        dataTableNewCustomers, 
        dataTableActiveCustomers, 
        handleOpenModalFilter,
        handleCloseModalFilter,
        openModalFilter,
        refFilter,
        selectedRow,
        handleSelectRows,
        ContentModal,
        initialDefaultValueFilter,
        filters,
        ...otherProps
    } = props;
    const tabs = [
        {
            title: 'Active',
            indexTab: 0,
        },
        {
            title: 'New',
            indexTab: 1,
        },

    ];
    const columnsCustomers: Column[] = useMemo(()=> [
        {
            Header: 'Name',
            maxWidth: 250,
            minWidth: 200,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box display={'flex'}
                        alignItems={'center'}>
                        <Box className="Customer-image">
                            <img src={original.name.avatar}
                                alt={original.name.fullName}
                                width={48}
                                height={48} />
                        </Box>
                        <Box className="Customer-content info">
                            <Text color={COLORS.grey7}
                                baseB1>
                                {original.name.fullName}
                            </Text>
                            <Text color={COLORS.grey4}
                                caption1>
                                {original.name.userName}
                            </Text>
                        </Box>
                    </Box>
                );
            },
        },
        {
            Header: 'Email',
            accessor: 'email',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<Text color={COLORS.grey4}
                    caption1>
                    {original.email}
                </Text>);
            }
        },
        {
            Header: 'Purchase',
            accessor: 'purchase',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<LabelColor title={original.purchase}
                    bg={COLORS.greenNgoc} />);
            }
        },
        {
            Header: 'Lifetime',
            accessor: 'lifetime',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (<Box display={'flex'}>
                    <LabelColor title={`$${original.lifetime.quantity}`}
                        numeral={original.lifetime.numeral}
                        mode={original.lifetime.mode}
                    />
                </Box>);
            }
        },
        {
            Header: 'Comments',
            accessor: 'comments',
            Cell: (item)=> (
                <Text baseSB1
                    color={COLORS.grey7}>
                    {item.value}
                </Text>
            )
        },
        {
            Header: 'Likes',
            accessor: 'likes',
            Cell: (item)=> (
                <Text baseSB1
                    color={COLORS.grey7}>
                    {item.value}
                </Text>
            )
        },
       
    ], []);

    const renderTabActiveCustomers = () => {
        return (
            <TableCustom className='table-customer'
                data={dataTableActiveCustomers}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                columns={columnsCustomers}
                handleSelectRow={handleSelectRows}
            />
        );
    };
    const renderTabNewCustomers = () => {
        return (
            <TableCustom className='table-customer'
                data={dataTableNewCustomers}
                checkbox
                options={{
                    hoverRow: true,
                    changePage: true,
                }}
                columns={columnsCustomers}
                handleSelectRow={handleSelectRows}
            />
        );
    };
    return (
        <Box
            component={'div'}
            className={classes.CustomerPage + (_.size(selectedRow) > 0 ? ' margin-more' :'')}
        >
            <Box display={'flex'}
                justifyContent={'space-between'}
                className='box-title-element'>
                <Box className="content-left"
                    display={'flex'}
                    flexWrap='wrap'
                    alignItems={'center'}>

                    <TitleElement bg={COLORS.violetLight}>
                        {title}
                    </TitleElement>
                    <Hidden only='xs'>
                        <Input placeholder='Search by name or email'
                            style={{marginLeft:24}}
                            search={searchCustomer}
                            onChange={handleSearchCustomer}
                        />
                    </Hidden>
                </Box>
                <Box className="content-right customers-tabs"
                    display={'flex'}>

                    {tabs.map((tab: {
                        title: string,
                        indexTab: number
                    })=> (
                        <Button key={uuidv4()}
                            onClick={()=> {
                                handleChangeTabActiveCustomer(tab.indexTab);
                            }}
                            className={clsx('button-tab', {
                                ['active']: tabActiveCustomer === tab.indexTab,
                            })}
                            color={'transparent'}>
                            {tab.title}
                        </Button>
                    ))}
                    <Hidden only='xs'>
                        <ModalFilter
                            openModal={openModalFilter}
                            defaultValues={initialDefaultValueFilter}
                            handleCloseModal={handleCloseModalFilter}
                            handleOpenModal={handleOpenModalFilter}
                            title={`Showing ${tabActiveCustomer==0 ? _.size(dataTableActiveCustomers): _.size(dataTableNewCustomers)} of 100 products`}
                        >
                            {filters && ContentModal()}
                        </ModalFilter>
                    </Hidden>
                </Box>
            </Box>
            {tabActiveCustomer === 0 && (
                renderTabActiveCustomers()
            )}
            {tabActiveCustomer === 1 && (
                renderTabNewCustomers()
            )}
        </Box>
    );
}

export default withStyles(styles)(CustomerList);