import { COLORS } from '@constants/colors';
import { iconHeartLight, iconRepeatFilled, iconSmileLight, iconTrash ,iconHeartFilled } from '@constants/imageAssets';
import { IProductComment } from '@containers/Admin/CommentsContainer/types';
import IconSVG from '@helpers/IconSVG';
import Input from '@helpers/Input';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import { Box, IconButton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React, { useMemo, useState } from 'react';
import { CellProps, Column } from 'react-table';
import styles from './styles';
import _ from 'lodash';
import FaceBookReactions from './FacebookReactions';
import LoveReaction from './LoveReaction';
import clsx from 'clsx';
interface IProductCommentProps{
    title:string,
    dataTablePost: IProductComment[],
    keyword: string,
    selections: IProductComment[],
    setKeyword: (keyword: string) => void,
    handlerSearchComment: (e : React.FormEvent<HTMLFormElement>) => void,
    handlerDeleteComment: (comments : IProductComment[]) => void,
    handlerRowSelection: (rows: any) => void,
    onChangeHandleSearchComment: (e : React.MouseEvent<HTMLInputElement>) => void,
}

const ProductComment = (props:IProductCommentProps & WithStyles<typeof styles>) => {
    const {title, dataTablePost, classes, keyword,selections ,setKeyword ,handlerSearchComment, handlerDeleteComment, handlerRowSelection, onChangeHandleSearchComment} = props;
    const coloumns : Column[] = useMemo(()=>[
        {
            Header:'Comments',
            minWidth: 300,
            maxWidth: 300,
            Cell: (values: CellProps<NonNullable<null>>) =>{
                const original:IProductComment = values.cell.row.original;
                return (
                    // eslint-disable-next-line react/prop-types
                    <Box className={classes.comments}>
                        <Box className='content'>
                            <img src={original.comment.image}
                                alt="" />
                            <Box>
                                <Text baseB1
                                    color={COLORS.grey7}>{original.comment.name}</Text>
                                <Text base1
                                    color={COLORS.grey5}>{original.comment.comment}</Text>
                                <Box component={'div'}
                                    className="box-icon"
                                    sx={{display:'flex',alignItems:'center',marginTop:'16px'}}>

                                    <LoveReaction/>

                                    <IconButton className='icon'
                                        sx={{backgroundColor:COLORS.white}}
                                        onClick = {() => handlerDeleteComment([original])}
                                    >
                                        <IconSVG small
                                            icon={iconTrash}
                                            fill={COLORS.grey4}
                                        />
                                    </IconButton>
                                    <FaceBookReactions>
                                        <IconButton className='icon'
                                            sx={{backgroundColor:COLORS.white}}
                                        >
                                            <IconSVG small
                                                icon={iconSmileLight}
                                                fill={COLORS.grey4}
                                            />
                                        </IconButton>
                                    </FaceBookReactions>
                                </Box>
                            </Box>
                        </Box>
                        <Box>
                            <Text caption1
                                color={COLORS.shades75}>{original.comment.time}</Text>
                        </Box>
                    </Box>
                );
            }
        },
        {
            Header:'Products',
            Cell: (values: CellProps<NonNullable<null>>) =>{
                const original:IProductComment = values.cell.row.original;
                return (
                    // eslint-disable-next-line react/prop-types
                    <Box className={classes.products}>
                        <img src={original.product.image}
                            alt="" />
                        <Box>
                            <Text baseB1
                                color={COLORS.grey7}>{original.product.title}</Text>
                            <Text caption1
                                color={COLORS.shades75}>{original.product.type}</Text>
                        </Box>
                    </Box>
                );
            }
        }
    ],[dataTablePost]);

    return (
        <Box className={`${classes.overReview} ${clsx({['active']: selections.length > 0})}`}>
            <Box className={classes.header}
                component={'div'}>
                <Box className={classes.subTitle}>
                    <Box className='overReviewBlockColor refund'></Box>
                    <Text titleSB1
                        color={COLORS.grey7}>{title}</Text>
                    <form action=""
                        onSubmit={handlerSearchComment}>
                        <Input placeholder='Search product'
                            style={{marginLeft:24}}
                            search = {keyword}
                            onChange = {onChangeHandleSearchComment}
                        />
                    </form>

                </Box>

            </Box>
            <Box className={classes.postTable}>
                <TableCustom
                    data={dataTablePost}
                    options={{
                        hoverRow: true,
                        changePage: true,
                    }}
                    handleSelectRow = {handlerRowSelection}
                    checkbox
                    columns={coloumns}/>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ProductComment);