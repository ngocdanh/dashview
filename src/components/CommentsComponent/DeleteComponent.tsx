import { COLORS } from '@constants/colors';
import { iconCheckAll, iconTrash } from '@constants/imageAssets';
import { IProductComment } from '@containers/Admin/CommentsContainer/types';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, IconButton } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import React from 'react';
import styles from './styles';


interface MessageProps {
    title?: string,
    selections: IProductComment[],
    handlerDeleteComment: (comments?: IProductComment[]) => void
}

function DeleteComponent(props: MessageProps & WithStyles<typeof styles>) {
    const {title, classes, selections,handlerDeleteComment, ...otherProps} = props;
    return (
        <Box className={`${classes.MessageComponent} ${clsx({['active']: selections.length > 0})}`}>

            <Box component={'div'}
                className='content'>
                <IconButton className='icon-basket'>
                    <IconSVG icon={iconCheckAll}/>
                </IconButton>
                <Text caption1
                    color={COLORS.grey4}>
                    <b>{selections.length} comments </b>selected
                </Text>
            </Box>
            <ButtonTransparent isElement
                className='social-button'
                onClick = {handlerDeleteComment}>
                <Text button1
                    className='button-text'
                    color={COLORS.red}
                    sx={{marginRight:'4px'}}>
                    Deleted
                </Text>
                <IconSVG width={24}
                    height={24}
                    icon={iconTrash}
                    fill={COLORS.red}/>
            </ButtonTransparent>
        </Box>
    );
}

export default React.memo(withStyles(styles)(DeleteComponent));