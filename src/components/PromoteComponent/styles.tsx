import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { maxHeight } from '@mui/system';
import { max } from 'lodash';
import { COLORS } from './../../constants/colors';

const styles = (theme: Theme) => createStyles({
    root: {
    
    },
    overReview: {
        backgroundColor:COLORS.grey1,
        width: '100%',
        marginTop: 8,
        height: maxHeight,
        padding: 24,
        borderRadius:8,
        '& .overReviewBlockColor': {
            width: 16,
            height: 32,
            borderRadius:4,
            marginRight:16,

            '&.insights':{
                backgroundColor:COLORS.orangeLight,
            },
            '&.recentPost':{
                backgroundColor:COLORS.blueLight,
            },
        },
    },
    header: {
        display:'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',
        '& .selectDay':{
            marginRight: 16,
        }
    },
    subTitle: {
        display: 'flex',
        flex: 1,
    },
    content: {
        paddingTop:32,
        '& .item:nth-child(10n+1)':{
            '& .itemImg': {
                backgroundColor:COLORS.greenLight,
            }
        },
        '& .item:nth-child(10n+4)':{
            '& .itemImg': {
                backgroundColor:COLORS.violetLight,
            }
        },
        '& .item:nth-child(10n+7)':{
            '& .itemImg': {
                backgroundColor:COLORS.orangeLight,
            }
        },
        '& .item:nth-child(10n)':{
            '& .itemImg': {
                backgroundColor:COLORS.blueLight,
            }
        },
        '& .item':{
            
            '& .itemImg': {
                width:48,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height:48,
                backgroundColor:COLORS.greenLight,
                borderRadius:48,
    
                '& img':{
                    width:24,
                }
            },
            '& .itemContent': {
                paddingTop:24,
                '& .contentText':{
                    display: 'flex',
                    color:COLORS.grey4,
                    alignItems: 'center',
                    '& img':{
                        width:18,
                    }
                },
                '& .itemProgress': {
                    padding: 0,
                    margin: 0,
                    '& *':{
                        padding:0,
                    },

                },
            },
        },
    },

    quantity:{
        padding:'12px 0 !important',  
        '&> *':{
            padding:'0 1px!important',
        },
    },
    postTable:{
        paddingTop:'32px',
        '& tbody .MuiTableRow-root':{
          
        },
        '& tr td:nth-child(1)': {
            verticalAlign: 'top',
        },
        '& .post-content':{
            paddingLeft:'20px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent:'space-between',
        },
        '& .post-content .post-title':{
            maxWidth: 160,
            lineHeight:1.6,
            marginBottom:12,
            height:'calc(15px * 1.6 * 2)',
        },
        '& .table-post':{
            '& .post-image':{
                position: 'relative',
                '&  img':{
                    borderRadius:'4px',
                    width: '100%',
                    height: '100%',
                    objectFit: 'cover',
                },
                '& .post-thumbnail':{
                    position: 'absolute',
                    top: '4px',
                    left: '4px',
                    padding: '4px',
                    backgroundColor: COLORS.white,
                    borderRadius: 4,    
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }
            },
            '& .post-links':{
                display: 'flex',
                alignItems: 'center',
                '& *~*':{
                    marginLeft:'12px'
                },
                '& .post-link':{
                    transition: 'all 0.5s ease-in-out 0s',
                    '& .icon':{
                        background: COLORS.grey3,
                    },

                    '&:hover .icon':{
                        background: COLORS.blueHover,
                    },
                    '&:hover .icon-svg':{
                        color: `${COLORS.white} !important`,
                    }
                },

            }
        },
        '& .distribution-number' :{
            paddingBottom:'24px'
        }
    },
    itemStyle: {
        paddingLeft: 32,
        paddingBottom: 20,
        [theme.breakpoints.down('sm')]: {
            paddingLeft: 0,
            display: 'flex',
            flexDirection: 'row-reverse',
            width: '100%',
            justifyContent: 'space-between',
            alignItems: 'center',
        }
    }
});

export default styles;
