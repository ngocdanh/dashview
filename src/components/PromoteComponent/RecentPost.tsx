import { COLORS } from '@constants/colors';
import { iconFacebook, iconTwitterLight } from '@constants/imageAssets';
import Button from '@helpers/Button';
import IconSVG from '@helpers/IconSVG';
import LabelColor from '@helpers/LabelColor';
import SelectCustom from '@helpers/SelectCustom';
import TableCustom from '@helpers/TableCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Grid, IconButton } from '@mui/material';
import Link from '@mui/material/Link';
import { withStyles } from '@mui/styles';
import { isMobile, truncateString } from '@utils/index';
import React, { useMemo } from 'react';
import { Column } from 'react-table';
import styles from './styles';
interface IRecentPost {
    classes:any,
    title: string,
    dataTablePost: any,
    handleOpenModalProductDetail: () => void,
}

function RecentPost(props: IRecentPost) {
    const { title, 
        classes,        
        dataTablePost,
        handleOpenModalProductDetail,
        ...otherProps } = props;
    const dataSelectRecentPost = [
        {
            label: 'Last 7 days',
            value: 'Last 7 days',
        },
        {
            label: 'Last 14 days',
            value: 'Last 14 days',
        },
        {
            label: 'Last 21 days',
            value: 'Last 21 days',
        },
    ];
    
    const columnsPost: Column[] = useMemo(()=> [
        {
            Header: 'Post',
            maxWidth: 300,
            minWidth: 300,
            width: 'max-content',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Grid container
                        spacing={isMobile ? 0: 2}
                        display={'flex'}
                        onClick={handleOpenModalProductDetail}
                        sx={{cursor:'pointer'}}
                        alignItems={'center'}>
                        <Grid item
                            xs={5}
                            className="post-image">
                            <img src={original.post.preview}
                                alt={original.post.title}
                            />
                            <Box className="post-thumbnail">
                                <img src={original.post.thumbnail}
                                    alt={original.post.title}
                                    width={16}
                                    height={16}
                                />
                            </Box>
                        </Grid>
                        <Grid item
                            xs={7}
                            className="post-content">
                            <Text className="post-title"
                                color={COLORS.grey7}
                                baseB1>
                                  
                                {!isMobile ? truncateString(original.post.title,32): 
                                    truncateString(original.post.title,20)}
                            </Text>
                            <Box className="post-links">
                                {original.post.linkFacebook && <Link target={'_blank'}
                                    className="post-link"
                                    href={original.post.linkFacebook}>
               
                                    <IconButton className='icon'>
                                        <IconSVG small
                                            className='icon-svg'
                                            icon={iconFacebook}
                                            fill={COLORS.black}/>
                                    </IconButton>
                                </Link>
                                }
                            
                                {original.post.linkTwitter && <Link target={'_blank'}
                                    className="post-link"
                                    href={original.post.linkTwitter}>
                                    <IconButton className='icon'>
                                        <IconSVG className='icon-svg'
                                            small
                                            icon={iconTwitterLight}
                                            fill={COLORS.black}/>
                                    </IconButton>
                                </Link>
                                }
                            </Box>
                        </Grid>
                    </Grid>
                );
            },
        },
        {
            Header: 'Distribution',
            accessor: 'distribution',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box component={'div'}>
                        <Box display={'flex'}>
                            <LabelColor className="distribution-number"
                                title={original.distribution.mode === 'up'? `+ ${original.distribution.numeral}` :`- ${original.distribution.numeral}`}
                                bg={original.distribution.mode === 'up'? COLORS.greenLight: COLORS.redLight}
                            />
                        </Box>
                        <LabelColor
                            className={classes.quantity}
                            bg={COLORS.white} />
                    </Box>
                );
            }
        },
        {
            Header: 'Link clicks',
            accessor: 'linkClicks',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box component={'div'}>
                        <Box display={'flex'}>
                            <LabelColor
                                bg={COLORS.grey3} 
                                title={original.linkClicks.quantity}
                                widthAfter={original.linkClicks.width}
                            />
                        </Box>
                        <LabelColor
                            className={classes.quantity}
                            mode={original.linkClicks.mode}
                            numeral={original.linkClicks.numeral}
                            bg={original.color} />
                    </Box>
                );
            }
        },
        {
            Header: 'Views',
            accessor: 'views',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box component={'div'}>
                        <Box display={'flex'}>
                            <LabelColor
                                bg={COLORS.grey3} 
                                title={original.views.quantity}
                                widthAfter={original.views.width}
                            />
                        </Box>
                        <LabelColor
                            className={classes.quantity}
                            mode={original.views.mode}
                            numeral={original.views.numeral}
                            bg={original.color} />
                    </Box>
                );
            }
        },
        {
            Header: 'Engagement',
            accessor: 'engagement',
            Cell: (values: any)=> {
                const original = values.cell.row.original;
                return (
                    <Box component={'div'}>
                        <Box display={'flex'}>
                            <LabelColor
                                bg={COLORS.grey3} 
                                title={original.engagement.quantity}
                                widthAfter={original.engagement.width}
                            />
                        </Box>
                        <LabelColor
                            className={classes.quantity}
                            mode={original.engagement.mode}
                            numeral={original.engagement.numeral}
                            bg={original.color} />
                    </Box>
                );
            }
        },
    ], []);

    const renderTabPost = () => {
        return (
            <TableCustom className='table-post'
                data={dataTablePost}
              
                options={{
                    hoverRow: false,
                    changePage: true,
                }}
                columns={columnsPost}/>
        );
    };




    return (
        <Box component={'div'}
            className={classes.overReview}>
            <Box className={classes.header}>
                <TitleElement bg={COLORS.blueLight}>{title}</TitleElement>
                <Box className='selectDay'>
                    <SelectCustom data={dataSelectRecentPost}
                        defaultValues="Last 7 days" />
                </Box>
                {/* <Button 
                    color={'primary'}
                >New post</Button> */}
            </Box>
            <Box className={classes.postTable}>
          
                {
                    renderTabPost()
                }
            </Box>

        </Box>
    );
}


export default withStyles(styles)(RecentPost);