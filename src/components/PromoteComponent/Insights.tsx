import { COLORS } from '@constants/colors';
import { iconArrowUpDown, 
    iconMessages, 
    iconProfile,
    iconMouse,
    iconInfoFilled } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import LabelColor from '@helpers/LabelColor';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import TooltipCustom from '@helpers/TooltipCustom';
import { Box, Divider, Grid, Hidden } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';
import { v4 as uuidv4 } from 'uuid';
import TitleElement from '@helpers/TitleElement';
import { isMobile } from '../../utils/index';

interface Insights {
    title: string;
    phone?: number; // optional
}

function Insights(props: Insights & WithStyles<typeof styles>) {
    const dataSelectOverView = [
        {
            label: 'Last 7 days',
            value: 'Last 7 days',
        },
        {
            label: 'Last 14 days',
            value: 'Last 14 days',
        },
        {
            label: 'Last 21 days',
            value: 'Last 21 days',
        },
    ];
    const listStaticOverView = [
        {
            icon: iconProfile,
            title: 'People reached',
            numberStatic: '256K',
            mode: 'up',
            numeral: '41.0%',
            titleMode: 'this week',
        },
        {
            icon: iconArrowUpDown,
            title: 'Engagement',
            numberStatic: '1.2x',
            numeral: '41.0%',
            mode: 'up',
            titleMode: 'this week',
        },
        {
            icon: iconMessages,
            title: 'Comments',
            numberStatic: '128',
            mode: 'down',
            numeral: '41.0%',
            titleMode: 'this week',
        },
        {
            icon: iconMouse,
            title: 'Link clicks',
            numberStatic: '80',
            mode: 'up',
            numeral: '41.0%',
            titleMode: 'this week',
        },
    ];
    const { title, classes, ...otherProps } = props;
    return (
        <Box component={'div'}
            className={classes.overReview}>
            <Box className={classes.header}
                component={'div'}>
                <TitleElement bg={COLORS.orangeLight}>{title}</TitleElement>
                <Box >
                    <SelectCustom data={dataSelectOverView}
                        defaultValues="Last 7 days" />
                </Box>
            </Box>
            <Grid className={classes.content}
                container
                spacing={2}>
                {listStaticOverView.map((item, index) => (
                    <React.Fragment key={uuidv4()}>
                        <Grid
                            key={index}
                            item
                            display={'flex'}
                            alignItems='center'
                            justifyContent={'space-between'}
                            component={'div'}
                            className="item"
                            flexDirection={!isMobile ? 'row': 'column'}
                            xs={12}
                            md={3}
                        >
                            <Box
                                className={classes.itemStyle}>
                                <Box component={'div'}
                                    className="itemImg">
                                    <IconSVG icon={item.icon}
                                        fill={COLORS.black}/>
                                </Box>
                                <Box component={'div'}
                                    className="itemContent">
                                    <Text caption1
                                        component={'div'}
                                        className="contentText">
                                        {item.title} &nbsp;
                                        <TooltipCustom title={'small description'}>
                                            <img alt=""
                                                src={iconInfoFilled} />
                                        </TooltipCustom>
                                    </Text>
                                    <Text h2
                                        color={COLORS.black}>{item.numberStatic}</Text>
                                    <LabelColor
                                        className="itemProgress" 
                                        mode={item.mode}
                                        titleMode={item.titleMode}
                                        numeral={item.numeral}
                                    />
                                </Box>
                            </Box>
                            {((index+1)%4!==0) && (
                                <Divider key={index}
                                    orientation={isMobile ? 'horizontal': 'vertical'}
                                    flexItem/>
                            )
                            }
                        </Grid>
                      
                    </React.Fragment>
                ))}
            </Grid>
        </Box>
    );
}


export default withStyles(styles)(Insights);