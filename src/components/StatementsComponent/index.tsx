import { IStatementBanner, ITransactions } from '@containers/Admin/StatementsContainer/types';
import Page from '@helpers/Page';
import { Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import StatmentBanner from './StatmentBanner';
import styles from './styles';
import Transaction from './Transaction';

interface StatementProps {
    title: string,
    phone?: number, // optional
    titlePage?: string,
    dataTableTransactions:ITransactions[],
    statementBannerList:IStatementBanner[],
    handleChangeSelect: (option: number) => void
}


function StatementsComponent(props: StatementProps & WithStyles<typeof styles>) {
    const {title, classes , ...otherProps} = props;
    return (
        <Page title={title}
            {...otherProps}>
            <Grid container>
                <StatmentBanner {...otherProps} />
            </Grid>
            <Grid container>
                <Transaction title='Transactions'
                    {...otherProps}
                />
            </Grid>
        </Page>
    );
}

export default withStyles(styles)(StatementsComponent);