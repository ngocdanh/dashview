import React from 'react';
import { IStatementBanner } from '@containers/Admin/StatementsContainer/types';
import { withStyles, WithStyles } from '@mui/styles';
import styles from './styles';
import { Box } from '@mui/material';
import Text from '@helpers/Text';
import IconSVG from '@helpers/IconSVG';
import { COLORS } from '@constants/colors';
import Button from '@helpers/Button';
import clsx from 'clsx';

interface IStatementBannerProps{
    statementBannerList: IStatementBanner[]
}

interface IStatementBannerProps{
    data:IStatementBanner,
    index:number
}

const StatmentBanner = (props:IStatementBannerProps & WithStyles<typeof styles>) => {
    const {statementBannerList , classes} = props;

    const StatementBannerItem = (props:IStatementBannerProps) =>{
        const {data, index} = props;
        const { icon, title, price, bg} = data;
        return (
            // eslint-disable-next-line react/prop-types
            <Box className={clsx(classes.currentBalance,{
                ['first']:index === 0,
                ['last']:index === 2,
            })}>
                <Box className='icon'
                    bgcolor={bg}>
                    <IconSVG icon={icon}
                        fill={COLORS.black}/>
                </Box>
                <Box>
                    <Text caption1
                        color={COLORS.grey4}>{title}</Text>
                    <Text h2
                        color={COLORS.grey7}>${price}</Text>
                </Box>
            </Box>
        );
    };
    return (
        <>
            <Box className={classes.overReview}>
                <Box className={classes.wrapper}>
                    {
                        statementBannerList.map((data:IStatementBanner,index:number) => (
                            <Box key={index} >
                                <StatementBannerItem data={data}
                                    index={index} />
                            </Box>

                        ))
                    }
                </Box>
            </Box>
        </>
    );
};

export default withStyles(styles)(StatmentBanner);
