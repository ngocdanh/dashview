import React from 'react';
import {withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box, Grid } from '@mui/material';
import Overiew from './PanelLeft/Overiew';
import clsx from 'clsx';
import Popular from './PanelRight/Popular';
import Comment from './PanelRight/Comment';
import ProductViews from './PanelLeft/ProductViews';
import ProTips from './PanelLeft/ProTips';
import GetMore from './PanelLeft/GetMore';
import RefundRequests from './PanelRight/RefundRequests';
import ModalVideoCustom from '@helpers/ModalVideoCustom';

interface HomeProps {
    classes?: any,
    title: string,
    titlePage?: string,
    dataOverView:any,
    fillterOverviewSelect:string;
    fillterProductViewsSelect:string;
    handleChangeSelectTabOverview: (value: number)=> void,
    handleCloseModalVideo:() => void;
    handleSelectOverview: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleSelectProductViews: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleOpenModalVideo:() =>void;
    selectTabOverView?: number,
    phone?: number, // optional
    openModalVideo:boolean;
    overviewUserDummyData?: {id: number, url: string, name: string}[]
}

function HomeComponent(props: HomeProps) {
    const {title, classes, titlePage,openModalVideo,handleCloseModalVideo, ...otherProps} = props;
    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}>
            <ModalVideoCustom open={openModalVideo}
                videoID={'C-CB3-Pj2oQ'}
                onClose={() =>{
                    handleCloseModalVideo();
                }}
            />

            <Grid container
                spacing={2}
                className={clsx(classes.homePage)}>
                <Grid item
                    className={classes.panelLeft}
                    xs={12}
                    lg={8}>
                      
                    <Overiew title='Overview'
                        {...otherProps}/>
                    <ProductViews title='Product Views'
                        {...otherProps}/>
                    <ProTips title='Pro tips'
                        {...otherProps}/>
                    <GetMore title={'Get more customers!'}
                        {...otherProps}/>
                </Grid>
                <Grid item
                    className={classes.panelRight}
                    xs={12}
                    lg={4}>
                    <Popular title='Popular products'/>
                    <Comment title={'Comments'}/>
                    <RefundRequests title='Refund requests'/>
                </Grid>
            </Grid>
        </Page>
    );
}

export default withStyles(styles)(HomeComponent);