import { COLORS } from '@constants/colors';
import {
    iconArrowForward,
    iconInfo
} from '@constants/imageAssets';
import { faker } from '@faker-js/faker';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import TooltipCustom from '@helpers/TooltipCustom';
import { Box, Grid, IconButton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import {
    CategoryScale, Chart as ChartJS, Legend, LinearScale, LineElement, PointElement, Title,
    Tooltip
} from 'chart.js';
import clsx from 'clsx';
import * as _ from 'lodash';
import React from 'react';
import { Line } from 'react-chartjs-2';
import { useNavigate } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import { formatMoney, isMobile } from '../../../utils/index';
import styles from './styles';

interface IOverview {
    title: string;
    dataOverView:any,
    fillterOverviewSelect:string;
    selectTabOverView?: number;
    handleSelectOverview: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleChangeSelectTabOverview?: (value: number) => void ;
    overviewUserDummyData?: any;
}

function OverviewElement(props: IOverview & WithStyles<typeof styles>) {
    const {
        title,
        classes,
        dataOverView,
        handleChangeSelectTabOverview,
        handleSelectOverview,
        fillterOverviewSelect,
        selectTabOverView,
        overviewUserDummyData,
        ...otherProps
    } = props;
    const Navigate = useNavigate();
    const dataSelectOverView = [
        {
            label: 'All time',
            value: 'All time',
        },
        {
            label: 'In a year',
            value: 'In a year',
        },
        {
            label: 'Per month',
            value: 'Per month',
        },
    ];

    ChartJS.register(
        CategoryScale,
        LinearScale,
        PointElement,
        LineElement,
        Title,
        Tooltip,
        Legend,
    );
    

    const labels = ['Apr', 'May', 'Jun', 'July', 'Aug', 'Sep'];
    const data = React.useMemo(()=> ({
        labels,
        datasets: [
            {
                label: 'Earning',
                data: labels.map(() => faker.datatype.number({ min: 0, max: 2000 })),
                lineTension: 0.4,
                borderColor: COLORS.blue,
                borderWidth: 4,
                pointRadius: 0,
            },
        ],
    }), [fillterOverviewSelect]);

    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false,
            },
            crosshair: false,
        },
        scales: {
            y: {
                grid: {
                    display: true,
                },
                ticks: {
                    stepSize: 500,
                    callback: (label:any, index: any, labels:any) => {
                        return formatMoney.format(label);
                    }
                }
            },
            x: {
                grid: {
                    display: false,
                }
            }
        }
    };

    return (
        <Box className={classes.overviewPage}
            component="div">
            <Grid className={classes.boxHeader}
                container>
                <Grid item>
                    <TitleElement bg={COLORS.orangeLight}
                        color={COLORS.grey7}>
            Overview
                    </TitleElement>
                </Grid>
                <Grid component="div"
                    item>
                    <SelectCustom data={dataSelectOverView}
                        handleSelect={handleSelectOverview}
                        defaultValues="All time" />
                </Grid>
            </Grid>
            <Grid className={classes.boxBody}
                container>
                {dataOverView.map((data:any,index:number)=>(
                    <Grid
                        key={uuidv4()}
                        item
                        lg={6}
                        xs={12}
                        className={clsx('box-tab', { active: selectTabOverView === (index+1) })}
                        onClick={() => handleChangeSelectTabOverview && handleChangeSelectTabOverview(index+1)}
                    >
                        <Box className={classes.dFlex}>
                            <Box className="box-image"
                                sx={{backgroundColor:data.color}}>
                                <img src={data.icon} />
                            </Box>
                            <Box className="content">
                                <Box className="box-subtitle">
                                    <Text caption1
                                        color={COLORS.grey4}>
                                        {data.title}
                                    </Text>
                                    <TooltipCustom title={'small description'}>
                                        <img alt=""
                                            src={iconInfo} />
                                    </TooltipCustom>
                                </Box>
                                <Text color={COLORS.grey7}
                                    h2>
                                    {data.quantity}
                                </Text>
                            </Box>
                        </Box>
                        <Box className={classes.timeline + (data.mode== 'down'? ' down': ' up')}
                            component="div">
                            <Text caption2>{data.numeral}</Text>
                        </Box>
                    </Grid>
                ))}
            </Grid>
            {selectTabOverView === 1 && (
                <Box className={classes.contentTabLeft}
                    component="div">
                    <Box className="box-message">
                        <Text className="text">
              Welcome <b>857 customers</b> with a personal message 😎
                        </Text>
                        <ButtonTransparent>{isMobile ? 'Send': 'Send message'}</ButtonTransparent>
                    </Box>
                    <Grid className="box-users"
                        container>
                        {!_.isEmpty(overviewUserDummyData) &&
              overviewUserDummyData?.map(
                  (
                      item: {
                          id: number;
                          name: string;
                          url: string;
                      },
                      index: number
                  ) => (
                      <Grid key={uuidv4()}
                          className="user"
                          item
                          lg={3}
                          xs={6}>
                          <Box>
                              <img alt=""
                                  src={item.url} />
                              <Text caption1
                                  center
                                  color={COLORS.grey6}>
                                  {item.name}
                              </Text>
                          </Box>
                      </Grid>
                  )
              )}
                        <Grid className="view-all"
                            item
                            lg={3}
                            xs={6}>
                            <ButtonTransparent className='button-transparent'
                                isElement
                                onClick={()=>{ Navigate('/dashboard/customer-list');}}>

                                <IconButton className="box-arrow">
                                    <img alt=""
                                        src={iconArrowForward} />
                                </IconButton>
                                <Text
                                    caption1
                                    center
                                    className="view-all-text"
                                    color={COLORS.grey6}
                                >
                View all
                                </Text>
                            </ButtonTransparent>
                        </Grid>
                    </Grid>
                </Box>
            )}
            {selectTabOverView === 2 && (
                <Box className={classes.contentTabRight}
                    component="div">
                    <Line options={options}
                        data={data} />
                </Box>
            )}
        </Box>
    );
}

export default withStyles(styles)(OverviewElement);
