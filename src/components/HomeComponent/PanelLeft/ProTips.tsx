import imageOverviewUser1 from '@assets/images/img_overview_user_1.png';
import { COLORS } from '@constants/colors';
import { iconArrowForward, iconDesignLight, iconMultiSelectLight, iconPhoneLight, iconScheduleLight, iconVideoRecorderLight, imageOverviewUser2, imageOverviewUser3 } from '@constants/imageAssets';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, CardMedia, colors, Grid, IconButton } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import _ from 'lodash';
import React, { useState } from 'react';
import styles from './styles';
import { v4 as uuidv4 } from 'uuid';
import IconSVG from '@helpers/IconSVG';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
interface IProoTips {
    title: string,
    handleOpenModalVideo:() =>void;

}

interface IpropsItem {
    icon: any,
    title: string,
    label?: string,
    colorLabel?: string,
    timeRead: string,
    iconAuth: string,
}

function PropTIpsElement(props: IProoTips & WithStyles<typeof styles>) {
    const {
        title,classes,handleOpenModalVideo,...otherProps
    } = props;
    const listTipsDumyData = [
        {
            icon: iconScheduleLight,
            title: 'Early access',
            label: 'New',
            colorLabel: COLORS.violetLight,
            timeRead: '3 mins read',
            iconAuth: imageOverviewUser1,
        },
        {
            icon: iconArrowForward,
            title: 'Asset use guidelines',
            label: 'Small label',
            colorLabel: COLORS.greenLight,
            timeRead: 'Time',
            iconAuth: imageOverviewUser2,
        },
        {
            icon: iconDesignLight,
            title: 'Exclusive downloads',
            label: '',
            colorLabel: '',
            timeRead: '2 mins read',
            iconAuth: imageOverviewUser1,
        },{
            icon: iconVideoRecorderLight,
            title: 'Behind the scenes',
            label: 'Hot',
            colorLabel: COLORS.orangeLight,
            timeRead: '3 mins read',
            iconAuth: imageOverviewUser1,
        },{
            icon: iconPhoneLight,
            title: 'Asset use guidelines',
            label: 'New',
            colorLabel: COLORS.violetLight,
            timeRead: '3 mins read',
            iconAuth: imageOverviewUser1,
        },{
            icon: iconMultiSelectLight,
            title: 'Life & work updates',
            label: '',
            colorLabel: '',
            timeRead: '3 mins read',
            iconAuth: imageOverviewUser3,
        }
    ];

    const renderItem = (propsItem: IpropsItem) => {
        const {icon, title, label, colorLabel, timeRead, iconAuth} = propsItem;

        return (
            <Grid item
                display='flex'
                key={uuidv4()}
                className={classes.boxRenderItem}
                sm={12}
                lg={6}
                alignItems='center'>
                <ButtonTransparent className='button-transparent'
                    isElement
                    onClick={()=> handleOpenModalVideo()}>
                    <IconButton className={'box-icon'}>
                        <IconSVG icon={icon}
                            title={title}
                            width={24}
                            height={24}
                            fill={COLORS.grey7} />
                    </IconButton>
                    <Box>
                        <a href="#"
                            onClick={(e: React.MouseEvent<HTMLAnchorElement>)=> {
                                e.preventDefault();
                            }}>
                            <Text baseSB1
                                className='info-title'
                                color={COLORS.grey7}>
                                {title}
                            </Text>
                        </a>
                        <Box display={'flex'}
                            className="info-tip">
                            {label && <Box className="info-label"
                                style={{backgroundColor: colorLabel}}>
                                {label}
                            </Box>}
                            <Box className="info-time"
                                display={'flex'}>
                                <img src={iconAuth} />
                                <Text caption2
                                    color={COLORS.grey4}>{timeRead}</Text>
                            </Box>
                        </Box>
                    </Box>
                </ButtonTransparent>
            </Grid>
        );
    };

    return (
        <Box
            component={'div'}
            className={classes.propTipsPage}
        >
            <Box
                className='box-title'
            >
                <TitleElement bg={COLORS.greenLight}>{title}</TitleElement>

            </Box>
            <Text
                className='desc-title'
                color={COLORS.grey4}
                bodyM1
            >
            Need some ideas for the next product?
            </Text>
            <Grid component={'div'}
                display={'flex'}
                container
                justifyContent={'space-between'}
                className='list-tips'
            >
                {!_.isEmpty(listTipsDumyData) && listTipsDumyData.map((item: IpropsItem) => (
                    renderItem({...item})
                ))}
            </Grid>
        </Box>
    );
}

export default withStyles(styles)(PropTIpsElement);