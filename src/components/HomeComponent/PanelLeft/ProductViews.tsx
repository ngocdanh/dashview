import { COLORS } from '@constants/colors';
import { faker } from '@faker-js/faker';
import SelectCustom from '@helpers/SelectCustom';
import TitleElement from '@helpers/TitleElement';
import { Box, Grid } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import {
    BarElement, CategoryScale, Chart as ChartJS, Legend, LinearScale, Title,
    Tooltip
} from 'chart.js';
import React from 'react';
import { Bar } from 'react-chartjs-2';
import styles from './styles';

interface IProductViews {
    title: string,
    fillterProductViewsSelect:string;
    handleChangeSelectTabOverview?: (value: number)=> void,
    handleSelectProductViews: (e: React.ChangeEvent<HTMLInputElement>) => void,
    selectTabOverView?: number,
}

const ProductViewElement = (props: IProductViews & WithStyles<typeof styles>) => {
    const { classes, title,fillterProductViewsSelect,handleSelectProductViews, ...otherProps} = props;

    ChartJS.register(
        CategoryScale,
        LinearScale,
        BarElement,
        Title,
        Tooltip,
        Legend
    );

    const labels = ['22', '23', '24', '25', '26', '27', '28'];

    const data = React.useMemo(()=> ({
        labels,
        datasets: [
            {
                label: 'Product view',
                data: labels.map(() => faker.datatype.number({ min: 5, max: 30 })),
                backgroundColor: [COLORS.blue],
                barPercentage: 0.7,
                hoverBackgroundColor: COLORS.blueHover,
                borderRadius: 4,
            },
        ],
    }), [fillterProductViewsSelect]);

    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false
            },
            crosshair: false,
        },
        scales: {
            y: {
                grid: {
                    display: false,
                    drawBorder: false,
                    drawOnChartArea: false,
                    
                },
                ticks: {
                    stepSize: 5,
                    suggestedMin: 5,
                }
            },
        }
    };
    
    const dataSelectTime = [
        {
            label: 'Last 7 days',
            value: 'Last 7 days',
        },
        {
            label: 'This month',
            value: 'This month',
        },
        {
            label: 'All time',
            value: 'All time',
        },
    ];
   
    return (
        <Box
            component={'div'}
            className={classes.productViews}
        >
            <Grid container
                className={classes.boxHeader}>
                <Grid item>
                    <TitleElement color={COLORS.grey7}
                        bg={COLORS.violetLight}>
                        {title}
                    </TitleElement>
                </Grid>
                <Grid item
                    component={'div'}

                >
                    <SelectCustom data={dataSelectTime}
                        handleSelect={handleSelectProductViews}
                        defaultValues='Last 7 days'/>
                </Grid>
            </Grid>
            <Box
                className={'render-chart'}
            >
                <Bar options={options}
                    data={data} />
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ProductViewElement);