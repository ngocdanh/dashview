import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    dFlex: {
        display: 'flex',
    },
    popularPage: {
        backgroundColor: COLORS.grey1,
        padding: '24px 12px',
        borderRadius: 8,
    },
    commentPage: {
        backgroundColor: COLORS.grey1,
        padding: '24px 12px',
        borderRadius: 8,
        marginTop: 8,
    },
    listProducts: {
        marginTop: 32,
        '& .box-title': {
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: 24,
            padding: '0 12px',
        },
        '& .render-list': {
            '& .product': {
                textDecoration: 'unset',
                display: 'flex',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
                padding: 12,
                marginBottom: 12,
                '&-start .title': {
                    maxWidth: 142,
                },
                '& .box-image': {
                    overflow: 'hidden',
                    borderRadius: 8,
                    width: 64,
                    height: 64,
                    marginRight: 12,
                    '& img': {
                        width: 100,
                        objectFit: 'cover',
                        height: 'auto',
                    }
                },
                '& .price': {
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    '& .quanity': {

                    },
                    '& .status': {
                        textAlign: 'center !important',
                        borderRadius: 4,
                        '&.up': {
                            color: COLORS.green,
                            backgroundColor: COLORS.greenLight,
                        },
                        '&.down': {
                            color: COLORS.red,
                            backgroundColor: COLORS.redLight,

                        }
                    }
                }
            }
        }
    },
    listComments: {
        marginTop: 32,
        padding: '0 24px',
        '& .item-comment': {
            '& .icon-user': {
                '& img': {
                    width: 48,
                    height: 48,
                    display: 'inline-block', 
                    marginRight: 12,
                }
            },
            '& .info-user': {
                width: '100%',
                display: 'block',
                alignItems: 'start',
                textDecoration: 'none',
                color: 'unset',
                transition: 'all .3s ease',
                '&:hover': {
                    // color: COLORS.grey4,
                },
                '& .on': {
                    marginRight: 3,
                }
            }
        },
        '& .divider': {
            margin: '24px 0',
        }
    },
    buttonActive: {
        marginTop: 5,
        '& *': {
            color: `${COLORS.green} !important`,
        },
        backgroundColor: COLORS.greenLight,
        padding: '4px 8px',
        borderRadius: 4,
    },
    buttonDeActive: {
        marginTop: 5,
        '& *': {
            color: `${COLORS.red} !important`,
        },
        backgroundColor: COLORS.redLight,
        padding: '4px 8px',
        borderRadius: 4,
    },
    buttonAll: {
        marginTop: 32,
    },
    groupReaction: {
        display: 'flex',
        justifyContent: 'space-between',
        padding: '16px 0 0 0',
        '& img': {
            width: 20,
            height: 20,
        },
        '& .reaction-icon': {
            '&:hover .icon-svg': {
                color: COLORS.grey7,
            },
            '& .icon-svg': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }
        }
    },
    refundRequestPage: {
        backgroundColor: COLORS.grey1,
        padding: '24px 12px',
        borderRadius: 8,
        marginTop: 8,
        '& .content': {
            marginTop: 32,
            '& .icon-basket': {
                backgroundColor: '#FFE7E4',
                transition: 'all .3s ease',
                width: 48,
                height: 48,
                borderRadius: '48px',
                marginRight: 8,
                justifyContent: 'center',
                alignItems: 'center',
                display: 'flex',
                cursor: 'default',
                '& .icon-svg': {
                    color: COLORS.red,
                }
            }
        }
    },
    buttonTransparent: {
        marginTop: 64,
        [theme.breakpoints.down('sm')]: {
            marginTop: 20,
        }
    }
});

export default styles;