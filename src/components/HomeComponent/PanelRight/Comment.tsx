import iconLinkLight from '@assets/icons/link/light.svg';
import imageOverviewUser1 from '@assets/images/img_overview_user_1.png';
import imageOverviewUser2 from '@assets/images/img_overview_user_2.png';
import { COLORS } from '@constants/colors';
import { iconMessage, imageOverviewUser3 } from '@constants/imageAssets';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import { Box, Divider, IconButton } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import _ from 'lodash';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import LoveReaction from './LoveReaction';
import styles from './styles';

interface IComment {
    title: string,
    handleChangeSelectTabOverview?: ()=> void,
    selectTabOverView?: number,
}

function PopularElement(props: IComment & WithStyles<typeof styles>) {
    const { classes, title, ...otherProps} = props;
    const Navigate = useNavigate();

    const listPopularDummyData = [
        {
            id: 1,
            image: imageOverviewUser1,
            username: '@ethel',
            name: 'ethel',
            message: 'Gate work 😊',
            time: '0.5h',
            category: 'Smiles – 3D icons',
        },
        {
            id: 2,
            image: imageOverviewUser2,
            username: '@jaz.designer',
            name: 'Jazmyn',
            message: 'Simple work',
            time: '4h',
            category: 'Smiles – 3D icons',
        },
        {
            id: 3,
            image: imageOverviewUser3,
            username: '@ethel',
            name: 'ethel',
            message: 'How can I buy only the design?',
            time: '8h',
            category: 'Fleet - Travel shopping',
        },
    ];
    return (
        <Box
            component={'div'}
            className={classes.commentPage}
        >
            <TitleElement bg={COLORS.orangeLight}>
                {title}
            </TitleElement>
            <Box
                className={classes.listComments}
            >
                <Box className="render-list">
                    {!_.isEmpty(listPopularDummyData) && listPopularDummyData?.map((comment: {
                        id: number,
                        image: string,
                        username: string,
                        name: string,
                        message: string,
                        time: string,
                        category: string,
                    }, index: number) => (
                        <React.Fragment  key={uuidv4()}>
                            <Box
                                display={'flex'}
                                alignItems={'start'}
                                className='item-comment'>
                                <div className="icon-user">
                                    <img src={comment.image}
                                        alt="" />
                                </div>
                                <a href=''
                                    onClick={(e: React.MouseEvent<HTMLAnchorElement>)=> e.preventDefault()}
                                    className="info-user">
                                    <Box component={'div'}
                                        width="100%">
                                        <Box display='flex'
                                            justifyContent='space-between'>
                                            <Box display={'flex'}
                                                component={'div'}>
                                                <b>{comment.name}</b>
                                                {comment.username}
                                            </Box>
                                            <Text caption1
                                                color={COLORS.shades75}>{comment.time}</Text>
                                        </Box>
                                        <Box display={'flex'}
                                            component={'div'}>
                                            <div className='on'>On</div>
                                            <b>{comment.category}</b>
                                        </Box>
                                        <Box>
                                            {comment.message}
                                        </Box>
                                    </Box>
                                    <Box component={'div'}
                                        className={classes.groupReaction}>
                                        <IconButton className='reaction-icon'>
                                            <IconSVG  icon={iconMessage}
                                                fill={COLORS.grey4}
                                                small
                                                title="" />
                                        </IconButton>
                                        <Box className='reaction-icon'>
                                            <LoveReaction/>
                                        </Box>
                                        <IconButton className='reaction-icon'
                                            onClick={()=>{ Navigate('/dashboard/comments');}}>
                                            <IconSVG icon={iconLinkLight}
                                                fill={COLORS.grey4}
                                                small
                                                title="" />
                                        </IconButton>
                                    </Box>
                                </a>
                            </Box>
                            <Divider className='divider'/>
                        </React.Fragment>
                    ))}
                    <Box className={classes.buttonAll}>
                        <ButtonTransparent   onClick={()=>{ Navigate('/dashboard/comments');}}>
                      View All
                        </ButtonTransparent>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}

export default withStyles(styles)(PopularElement);