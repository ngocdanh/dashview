import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {
        width: '100%',
        minHeight: '100vh',
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& .logo': {
            position: 'absolute',
            top: 20,
            left: 20,
            width: 48,
            cursor: 'pointer'
        }
    },
    content: {
        '& .title': {
            color: COLORS.grey7,
            padding: '30px 0',
            textAlign: 'center',
            fontWeight: 'bold',
        }
    },
    imgNotFound: {
        width: 'calc(30vw + 50px)',
    },
    buttonBack: {
        borderRadius: '30px !important',
        '& .text': {
            textTransform: 'uppercase',
            fontWeight: 900,
            fontSize: 18,
            '@media(max-width: 800px)': {
                fontSize: 16,
            }
        }
    }
});

export default styles;