import React from 'react';
import {WithStyles, withStyles} from '@mui/styles';
import styles from './styles';
import Page from '@helpers/Page';
import { Box } from '@mui/material';
import { img404NotFound, logo } from '@constants/imageAssets';
import Text from '@helpers/Text';
import { COLORS } from '@constants/colors';
import Button from '@helpers/Button';
import { useNavigate } from 'react-router-dom';

function NotFoundComponent(props: WithStyles<typeof styles>) {
    const {classes} = props;
    const navigate = useNavigate();
    return (
        <Box className={classes.root}>
            <Box className={classes.content}>
                <Text h3
                    className='title'
                    color={COLORS.grey4}
                    center>404 ERROR PAGE</Text>
                <Box display='flex'
                    justifyContent={'center'}>
                    <img src={img404NotFound}
                        className={classes.imgNotFound}
                        alt="" />
                </Box>
                <Box paddingY={3}>
                    <Text titleM1
                        color={COLORS.grey4}
                        center>Whoops! It looks like you are lost in space!</Text>
                </Box>
                <Box display='flex'
                    justifyContent={'center'}
                    marginY={5}>
                    <Button className={classes.buttonBack}
                        onClick={()=> navigate(-1)}
                        color='primary'>
                      GO BACK
                    </Button>
                </Box>
            </Box>
            <img src={logo}
                onClick={()=> navigate('/')}
                className='logo'
                alt="" />
        </Box>
    );
}

export default withStyles(styles)(NotFoundComponent);