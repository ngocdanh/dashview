import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    dFlex: {
        display: 'flex',
    },
    root: {

    },
    homePage: {

    },
    panelLeft: {},
    panelRight: {},
    boxTitle: {
        display: 'flex',
        alignItems: 'center',
    },
    overviewPage: {
        padding: 24,
        backgroundColor: COLORS.grey1,
        borderRadius: 8,
    },
    boxHeader: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    boxBody: {
        margin: '32px 0',
        borderRadius: 20,
        backgroundColor: COLORS.grey2,
        padding: 12,
        display: 'flex',
        '& .box-tab': {
            display: 'flex',
            justifyContent: 'space-between',
            borderRadius: 12,
            padding: 20,
            cursor: 'pointer',
            transition: 'all .4s ease',
            backgroundColor: 'transparent',
            '&.active': {
                backgroundColor: COLORS.grey1,
            }
        },
        '& .box-image ': {
            width: 40,
            height: 40,
            borderRadius: 40,
            backgroundColor: COLORS.blueLight,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 16,
            '& img': {
                width: 24,
                height: 24,
            }
        },
        '& .content': {
            '& .box-subtitle': {
                display: 'flex',
                alignItems: 'center',
                '& img': {
                    width: 13,
                    height: 13,
                    display:'inline-block',
                    marginLeft: 5,
                }
            }
        },
        '& .timeline': {
            padding: 5,
            borderRadius: 8,
            backgroundColor: 'transparent',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: 'fit-content',
            height: 'fit-content',
            '&.down': {
                backgroundColor: 'rgba(255, 231, 228, 1)',
                color: COLORS.red,
            },
            '&.up': {
                backgroundColor: 'rgba(252, 252, 252, 1)',
                color: COLORS.green,
            },
        }
    },
    boxSelect: {
        borderRadius: 12,
        border: `2px solid ${COLORS.grey3}`,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        padding: 8,
        outlineOffset: -2,
        borderImage: 'none',
        outline: 'transparent',
        color: COLORS.grey4,
        fontWeight: 600,
        fontSize: 14,
        [theme.breakpoints.down('sm')]: {
            padding: 3,
            height: 35,
        }
    },
    rock: {
        borderRadius: 4,
        width: 16,
        height: 32,
        background: COLORS.orangeLight,
        marginRight: 16,
    }
});

export default styles;