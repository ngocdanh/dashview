import { COLORS } from '@constants/colors';
import {
    iconInfo
} from '@constants/imageAssets';
import { faker } from '@faker-js/faker';
import SelectCustom from '@helpers/SelectCustom';
import Text from '@helpers/Text';
import TitleElement from '@helpers/TitleElement';
import TooltipCustom from '@helpers/TooltipCustom';
import { Box, Grid } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { formatMoney } from '@utils/';
import {
    BarElement,
    CategoryScale, Chart as ChartJS, Legend, LinearScale, LineElement, PointElement, Title,
    Tooltip
} from 'chart.js';
import clsx from 'clsx';
import React from 'react';
import { Bar, Line } from 'react-chartjs-2';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';

interface IOverview {
    title: string;
    dataOverView:any,
    fillterOverviewSelect:string;
    selectTabOverView?: number;
    handleSelectOverview: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleChangeSelectTabOverview?: (value: number) => void ;
    overviewUserDummyData?: any;
}

function SnapShot(props: IOverview & WithStyles<typeof styles>) {
    const {
        title,
        classes,
        dataOverView,
        handleChangeSelectTabOverview,
        handleSelectOverview,
        fillterOverviewSelect,
        selectTabOverView,
        overviewUserDummyData,
        ...otherProps
    } = props;

    const dataSelectOverView = [
        {
            label: 'Last 7 days',
            value: 'Last 7 days',
        },
        {
            label: 'This month',
            value: 'This month',
        },
        {
            label: 'All time',
            value: 'All time',
        },
    ];

    ChartJS.register(
        CategoryScale,
        LinearScale,
        PointElement,
        LineElement,
        Title,
        Tooltip,
        Legend,
        BarElement
    );


    const labelsBar = ['22', '23', '24', '25', '25', '27', '28'];
    const labelsLine = ['Apr', 'May', 'Jun', 'July', 'Aug', 'Sep'];

    const dataLine = React.useMemo(()=> ({
        labels:labelsLine,
        datasets: [
            {
                label: 'Clicks',
                data: labelsLine.map(() => faker.datatype.number({ min: 0, max: 2000 })),
                lineTension: 0.4,
                borderColor: COLORS.blue,
                borderWidth: 4,
                pointRadius: 0,
            },
        ],
    }), [fillterOverviewSelect]);

    const dataBar = React.useMemo(()=> ({
        labels:labelsBar,
        datasets: [
            {
                label: 'Product view',
                data: labelsBar.map(() => faker.datatype.number({ min: 5, max: 50 })),
                backgroundColor: [COLORS.greenNgoc],
                barPercentage: 0.7,
                hoverBackgroundColor: COLORS.blueHover,
                borderRadius: 4,
            },
        ],
    }), [fillterOverviewSelect]);

    const optionsLine = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false,
            },
            crosshair: false,
        },
        scales: {
            y: {
                grid: {
                    display: true,
                },
                ticks: {
                    stepSize: 500,
                    callback: (label:any, index: any, labels:any) => {
                        return formatMoney.format(label);
                    }
                }
            },
            x: {
                grid: {
                    display: false,
                }
            }
        }
    };

    const optionsBar = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                position: 'top' as const,
                display: false
            },
            crosshair: false,
        },
        scales: {
            y: {
                grid: {
                    display: false,
                    drawBorder: false,
                    drawOnChartArea: false,

                },
                ticks: {
                    stepSize: 5,
                    suggestedMin: 5,
                }
            },
        }
    };

    return (
        <Box className={classes.overviewPage}
            component="div">
            <Grid className={classes.boxHeader}
                container>
                <Grid item>
                    <TitleElement bg={COLORS.orangeLight}
                        color={COLORS.grey7}>
            SnapShot
                    </TitleElement>
                </Grid>
                <Grid component="div"
                    item>
                    <SelectCustom data={dataSelectOverView}
                        handleSelect={handleSelectOverview}
                        defaultValues="All time" />
                </Grid>
            </Grid>
            <Grid className={classes.boxBody}
                container>
                {dataOverView.map((data:any,index:number)=>(
                    <Grid
                        key={uuidv4()}
                        item
                        lg={6}
                        xs={12}
                        className={clsx('box-tab', { active: selectTabOverView === (index+1) })}
                        onClick={() => handleChangeSelectTabOverview && handleChangeSelectTabOverview(index+1)}
                    >
                        <Box className={classes.dFlex}>
                            <Box className="box-image"
                                sx={{backgroundColor:data.color}}>
                                <img src={data.icon} />
                            </Box>
                            <Box className="content">
                                <Box className="box-subtitle">
                                    <Text caption1
                                        color={COLORS.grey4}>
                                        {data.title}
                                    </Text>
                                    <TooltipCustom title={'small description'}>
                                        <img alt=""
                                            src={iconInfo} />
                                    </TooltipCustom>
                                </Box>
                                <Text color={COLORS.grey7}
                                    h2>
                                    {data.quantity}
                                </Text>
                            </Box>
                        </Box>
                        <Box className={classes.timeline + (data.mode== 'down'? ' down': ' up')}
                            component="div">
                            <Text caption2>{data.numeral}</Text>
                        </Box>
                    </Grid>
                ))}
            </Grid>
            {selectTabOverView === 1 && (
                <Box className={classes.productViews}>
                    <Box
                        className={'render-chart'}
                    >
                        <Bar options={optionsBar}
                            data={dataBar} />
                    </Box>
                </Box>
            )}
            {selectTabOverView === 2 && (
                <Box className={classes.contentTabRight}
                    component="div">
                    <Line options={optionsLine}
                        data={dataLine} />
                </Box>
            )}
        </Box>
    );
}

export default withStyles(styles)(SnapShot);
