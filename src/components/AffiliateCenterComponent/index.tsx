import Page from '@helpers/Page';
import { Grid } from '@mui/material';
import { withStyles } from '@mui/styles';
import clsx from 'clsx';
import React from 'react';
import styles from './styles';

import SnapShot from './PanelLeft/SnapShot';

interface HomeProps {
    classes?: any,
    title: string,
    titlePage?: string,
    dataOverView:any,
    fillterOverviewSelect:string;
    fillterProductViewsSelect:string;
    handleChangeSelectTabOverview: (value: number)=> void,

    handleSelectOverview: (e: React.ChangeEvent<HTMLInputElement>) => void,
    handleSelectProductViews: (e: React.ChangeEvent<HTMLInputElement>) => void,

    selectTabOverView?: number,
    phone?: number, // optional
}

function AffiliateCenter(props: HomeProps) {
    const {title, classes, titlePage, ...otherProps} = props;
    return (
        <Page title={title}
            titlePage={titlePage}
            {...otherProps}>
            <Grid container
                spacing={2}
                className={clsx(classes.homePage)}>
                <Grid item
                    className={classes.panelLeft}
                    xs={12}
                    lg={8}>

                    <SnapShot title='SnapShot'
                        {...otherProps}/>
                </Grid>
            </Grid>
        </Page>
    );
}

export default withStyles(styles)(AffiliateCenter);