import interBoldFontwoff from './Inter-Bold.woff';
import interBoldFontwoff2 from './Inter-Bold.woff2';
import interExtraBoldFontwoff from './Inter-ExtraBold.woff';
import interExtraBoldFontwoff2 from './Inter-ExtraBold.woff2';
import interSemiBoldFontwoff from './Inter-SemiBold.woff';
import interSemiBoldFontwoff2 from './Inter-SemiBold.woff2';
import interLightFontwoff from './Inter-Light.woff';
import interLightFontwoff2 from './Inter-Light.woff2';
import interRegularFontwoff from './Inter-Regular.woff';
import interRegularFontwoff2 from './Inter-Regular.woff2';
import interMediumFontwoff from './Inter-Medium.woff';
import interMediumFontwoff2 from './Inter-Medium.woff2';

const inter = {
    fontFamily: 'inter',
    fontStyle: 'normal',
    src: `local('myAwesomeFont-Regular'), 
  url(${interRegularFontwoff}) format('woff'),
  url(${interRegularFontwoff2}) format('woff2')`,
};

const interLight = {
    ...inter,
    fontweight: 300,
    src: `local('Inter-Light'), 
  url(${interRegularFontwoff}) format('woff'),
  url(${interRegularFontwoff2}) format('woff2')`,
};

const interRegular = {
    ...inter,
    fontWeight: 400,
    src: `local('Inter-Regular'), 
  url(${interRegularFontwoff}) format('woff'),
  url(${interRegularFontwoff2}) format('woff2')`,
};

const interMedium = {
    ...inter,
    fontweight: 500,
    src: `local('Inter-Medium'), 
  url(${interMediumFontwoff}) format('woff'),
  url(${interMediumFontwoff2}) format('woff2')`,
};

const interSemiBold = {
    ...inter,
    fontweight: 600,
    src: `local('Inter-SemiBold'), 
  url(${interSemiBoldFontwoff}) format('woff'),
  url(${interSemiBoldFontwoff2}) format('woff2')`,
};

const interBold = {
    ...inter,
    fontweight: 700,
    src: `local('Inter-Bold'), 
  url(${interRegularFontwoff}) format('woff'),
  url(${interRegularFontwoff2}) format('woff2')`,
};

const interExtraBold = {
    ...inter,
    fontweight: 800,
    src: `local('Inter-ExtraBold'), 
  url(${interRegularFontwoff}) format('woff'),
  url(${interRegularFontwoff2}) format('woff2')`,
};

export default [interLight, interRegular, interMedium, interSemiBold, interBold, interExtraBold];