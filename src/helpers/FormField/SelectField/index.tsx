import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { MenuItem, Select } from '@mui/material';
import { withStyles } from '@mui/styles';
import { SxProps } from '@mui/system';
import * as _ from 'lodash';
import React from 'react';
import { useController, UseFormReturn } from 'react-hook-form';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';

interface ITitlePage {
    data: {value: string, label: string}[],
    classes?: any,
    handleSelect?: (e: any)=> void,
    select?:string,
    style?: any,
    name: string,
    control?: UseFormReturn['control'],
}

const SelectField = React.forwardRef((props: ITitlePage, ref) => {
    const {
        classes,
        data, 
        style, 
        handleSelect, 
        select, 
        control,
        name,
        ...otherProps
    } = props;

    const {field} = useController({
        name, 
        control,
    });
    
    return (
        <Select
            id=""
            style={style}
            className={classes.boxSelect}
            IconComponent={KeyboardArrowDownIcon}
            {...field}
            {...otherProps}
        >
            {
                !_.isEmpty(data) && 
                data?.map((item: {value: string, label: string}) => (
                    <MenuItem value={item.value}
                        key={uuidv4()}>
                        <Text base2
                            color={COLORS.grey4}>{item.label}</Text>
                    </MenuItem>
                ))
            }
        </Select>
    );
});
export default withStyles(styles)(SelectField);