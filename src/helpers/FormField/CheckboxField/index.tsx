import { Box, Checkbox } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import { SxProps } from '@mui/system';
import * as React from 'react';
import styles from './styles';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import clsx from 'clsx';
import { useController, UseFormReturn } from 'react-hook-form';

interface CheckboxFieldProps {
    className?: string,
    sx?: SxProps,
    ref?: HTMLInputElement | undefined,
    control?: UseFormReturn['control'],
    name?: string,
    index?: number,
    onChange?: (event: React.MouseEvent<HTMLInputElement>) => void,
    handleCheckAllShowing?: () => void,
}


const CheckboxField = (props: CheckboxFieldProps & WithStyles<typeof styles>) => {
    const {classes, name='', control, className, sx, handleCheckAllShowing, index, ...otherProps} = props;

    const {field} = useController({
        name, 
        control,
    });


    return (
        <Box component={'label'}
            className={clsx(
                classes.boxCheckboxField, 
                className && className, 
                'checkbox-custom'
            )}
        >
            <input
                type='checkbox' 
                {...field}
                onClick={(event: React.MouseEvent<HTMLInputElement>) => {
                    field.onChange(Boolean(event.currentTarget.value));
                    if(index === 0) {
                        handleCheckAllShowing && handleCheckAllShowing();
                    }
                }}
                {...field.value === true && {
                    checked: true,
                }}
                {...otherProps}
            />
            <span></span>
        </Box>
    );
};

export default withStyles(styles)(CheckboxField);