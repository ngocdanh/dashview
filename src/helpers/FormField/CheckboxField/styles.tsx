import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    boxCheckboxField: {
        position: 'relative',
        '& span': {
            border: `2px solid ${COLORS.grey4}`,
            cursor: 'pointer',
            width: 24,
            borderRadius: '6px',
            height: 24,
            transition: 'all .2s ease',
            position: 'relative',
            display: 'inline-block',
            '&:hover': {
                boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
                '&:checked': {
                    boxShadow: 'unset',
                }
            },
            '&:before': {
                content: '""',
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 17,
                height: 12,
                transition: 'all .2s ease',
                opacity: 0,
                background: 
          'url("data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'17\' height=\'12\' viewBox=\'0 0 17 12\'%3E%3Cpath d=\'M16.707.293a1 1 0 0 1 0 1.414l-8.586 8.586a3 3 0 0 1-4.243 0L.293 6.707A1 1 0 0 1 .735 5.02a1 1 0 0 1 .973.273l3.586 3.586a1 1 0 0 0 1.414 0L15.293.293a1 1 0 0 1 1.414 0z\' fill=\'%23fcfcfc\' fill-rule=\'evenodd\'/%3E%3C/svg%3E") no-repeat 50% 50%/100% auto',
            }
        },
        '& input': {
            position: 'absolute',
            top: 0,
            left: 0,
            opacity: 0,
            outline: 'none',
            width: 0,
            height: 0,
            cursor: 'pointer',
            '&:checked + span': {
                background: COLORS.blue,
                borderColor: COLORS.blue,
            },
            '&:checked + span:before': {
                opacity: 1,
            }
        },

    },
});

export default styles;