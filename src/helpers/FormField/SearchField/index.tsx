import iconSearch from '@assets/icons/search/light.svg';
import { iconCloseLight } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import { Box, IconButton, Input, SxProps } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import _ from 'lodash';
import * as React from 'react';
import { useController, UseFormReturn } from 'react-hook-form';
import styles from './styles';

interface ISearchField {
    placeholder?: string;
    isFocused?: boolean;
    onBlur?: ()=> void;
    onChange?: ()=> void;
    onfocus?: ()=> void;
    commandF?: boolean;
    className?: any;
    style?: any;
    search?: string,
    handleSearch?: (e: any)=> void,
    sx?: SxProps,
    control?: UseFormReturn['control'],
    name?: string,
    register?: UseFormReturn['register'],
    defaultValue?: string,
}

const SearchField = React.forwardRef((props: ISearchField & WithStyles<typeof styles>, ref) => {
    const {classes, isFocused, 
        placeholder, defaultValue, search, 
        style, handleSearch, name= '', control,
        ...otherProps} = props;
    
        
    const {field} = useController({
        name,
        control,
        defaultValue,
    });

    return (
        <Box
            className={classes.searchBox}
            style={style}
        >
            <Input
                autoFocus={isFocused}
                fullWidth
                placeholder={placeholder}
                disableUnderline
                // ref={ref}
                {...field}
                {...otherProps}
            />
        </Box>
    );
});

export default withStyles(styles)(SearchField);