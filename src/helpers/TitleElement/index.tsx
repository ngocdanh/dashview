import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import { withStyles } from '@mui/styles';
import { Box } from '@mui/system';
import clsx from 'clsx';
import React, {forwardRef} from 'react';
import { Helmet } from 'react-helmet-async';
import styles from './styles';

interface ITitlePage {
    color?: string,
    bg?: string,
    children: React.ReactNode,
    classes?: any,
    className?: string,
}

const TitleElement = (props: ITitlePage) => {
    const {classes, color, bg, children, className, ...otherProps} = props;
    return (
        <Box
            className={clsx(classes.boxTitle, className, 'title-element')}>
            <Box component={'div'}
                className={classes.rock + ' custom-rock'} 
                style={{backgroundColor: bg || 'currentColor'}}
            />
            <Text titleSB1
                color={color || COLORS.grey7}>
                {children}
            </Text>
        </Box>
    );
};
export default withStyles(styles)(TitleElement);