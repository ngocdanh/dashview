import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    boxTitle: {
        display: 'flex',
        alignItems: 'center',
        [theme.breakpoints.down('sm')]: {
            marginBottom: 8,
        }
    },
    rock: {
        borderRadius: 4,
        width: 16,
        height: 32,
        marginRight: 16,
        [theme.breakpoints.down('sm')]: {
            marginRight: 8,
        }
    }
});

export default styles;