import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    editor:{
        '& .tox.tox-tinymce':{
            borderRadius: '12px'
        },
        '& .tox-toolbar__primary': {
            justifyContent: 'space-between',
            padding: '12px',
            background: `${COLORS.grey1} !important`,
        },
        '& .tox-toolbar__group': {
            border:'0 !important'
        },
        '& .tox-editor-header': {
            padding:'12px',
        },
        '& .tox-edit-area__iframe':{
            background: `${COLORS.grey2} !important`
        }
    }

});

export default styles;