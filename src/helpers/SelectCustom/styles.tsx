import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    boxSelect: {
        borderRadius: '12px !important',
        border: `2px solid ${COLORS.grey3}`,
        display: 'flex !important',
        justifyContent: 'center !important',
        alignItems: 'center !important',
        height: 40,
        padding: 8,
        outlineOffset: -2,
        borderImage: 'none',
        outline: 'transparent',
        color: `${COLORS.grey4} !important`,
        fontWeight: `${600} !important`,
        fontSize: '14px !important',
        [theme.breakpoints.down('sm')]: {
            padding: 3,
            height: 35,
        },
        '&:before': {
            borderColor: COLORS.grey3,
        },
        '&:after': {
            borderColor: COLORS.grey3,
        },
        '& .Mui-focused': {
            border: '0 !important',
        },
        '& .MuiOutlinedInput-notchedOutline': {
            border: '0 !important',
        },
        '& .MuiMenu-paper': {
            borderRadius: '12px',
            marginTop: -5
        }
    },
});

export default styles;