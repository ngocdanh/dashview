import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { MenuItem, Select } from '@mui/material';
import { withStyles } from '@mui/styles';
import { SxProps } from '@mui/system';
import clsx from 'clsx';
import * as _ from 'lodash';
import React from 'react';
import { UseFormReturn } from 'react-hook-form';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';

interface ITitlePage {
    defaultValues: string,
    data: {value: string, label: string}[],
    classes?: any,
    handleSelect?: (e: any)=> void,
    select?:string,
    style?: any,
}

const SelectCustom = React.forwardRef((props: ITitlePage,
    ref : React.RefObject<HTMLInputElement>) => {
    const {
        classes,
        defaultValues,
        data,
        style,
        handleSelect,
        select,
        ...otherProps
    } = props;

    return (
        <Select
            ref={ref}
            onChange={handleSelect}
            id=""
            style={style}
            defaultValue={defaultValues}
            value={select}
            className={clsx(classes.boxSelect, 'select-custom')}
            IconComponent={KeyboardArrowDownIcon}
            ref = {ref}
            {...otherProps}
        >
            {
                !_.isEmpty(data) && data?.map((item: {value: string, label: string}, index: number) => (
                    <MenuItem value={item.value}
                        key={uuidv4()}>
                        <Text base2
                            color={COLORS.grey4}>{item.label}</Text>
                    </MenuItem>
                ))
            }
        </Select>
    );
});
export default withStyles(styles)(SelectCustom);