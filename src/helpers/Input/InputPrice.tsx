import { COLORS } from '@constants/colors';
import { iconDollar } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import InputCustom from './index';
import styles from './styles';
const InputPrice = React.forwardRef((props: any & WithStyles<typeof styles>,
    ref:React.RefObject<HTMLInputElement>) => {
    const { classes,...otherProps } = props;
    return (
        <Box className={classes.price}>
            <InputCustom className='input-custom'
                disableIconSearch = {true}
                {...otherProps}
                ref = {ref}
            />
            <Box className='box-icon'
                style={{background:COLORS.grey2}}>
                <IconSVG icon={iconDollar}
                    width={'10px'}
                    height={'24px'}/>
            </Box>
        </Box>
    );
});

export default withStyles(styles)(InputPrice);