import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    root:{},
    ModalCustomDate:{
        position: 'absolute',
        left: '103%',
        top: '0%',
        borderRadius:16,
        opacity:0,
        visibility: 'hidden',
        backgroundColor: COLORS.white,
        transition: 'all .3s ease 0s',
        '&.showModal':{
            opacity:1,
            visibility: 'visible',
        },
        '& .box':{
            padding: '12px 19px',
        },
        '& .react-datepicker':{

            border:'none',
            '&__header':{
                backgroundColor: COLORS.white,
                border:'none',
            },
            '& __day-names':{
                paddingTop:'16px',
            },
            '&__day-name':{
                margin: 0,
                padding: 0,
                width:'36px',
                color: COLORS.shades75,
                fontWeight:700,
                fontSize:'12px'
            },
            '&__week':{

            },
            '&__day':{
                color: COLORS.grey6,
                fontWeight:600,
                fontSize:'15px',
                padding: 0,
                margin: 0,
                width: '36px',
                height: '36px',
                lineHeight: '36px',
                transition: 'all .3s ease 0s',
                '&--outside-month':{
                    opacity: '0',
                    visibility: 'hidden',
                    transition: 'all .3s ease 0s',
                },
            
                '&--selected':{
                    borderRadius:'36px',
                    backgroundColor: `${COLORS.black} !important`,
                    color: `${COLORS.white} !important`,
                },
                '&--highlighted-custom':{
                    borderRadius:'36px',
                    backgroundColor: `${COLORS.blue} !important`,
                    color: `${COLORS.white} !important`,
                },
                '&--keyboard-selected':{
                    backgroundColor:'transparent',
                },
                '&:hover':{
                    borderRadius:'36px',
                    backgroundColor:COLORS.grey3,
                    color: COLORS.grey7,
                  
                }

            },
            '& .react-datepicker__day--selected.react-datepicker__day--highlighted-custom':{
                borderRadius:'36px',
                backgroundColor: `${COLORS.black} !important`,
                color: `${COLORS.white} !important`,
            }
        },
        '& .box-button-custom':{
            display: 'flex',
            alignItems: 'center',
            justifyContent:'flex-end',
          
        }

    },
    HeaderCustom:{
        marginBottom:12,
        display:'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        '& .icon':{
            transition: 'all .3s ease',
            '&:hover':{
                backgroundColor: COLORS.white,
                fill: COLORS.blue,
            },
            '&:hover .icon-svg': {
                color: COLORS.blue,
            },
        },
    },
});

export default styles;