import { Box, IconButton } from '@mui/material';
import Popover from '@mui/material/Popover';
import { withStyles, WithStyles } from '@mui/styles';
import * as React from 'react';
import styles from './styles';
import DatePicker  from 'react-datepicker';
import IconSVG from '@helpers/IconSVG';
import { iconChevronBackward, iconChevronForward } from '@constants/imageAssets';
import { COLORS } from '@constants/colors';
import Text from '@helpers/Text';
import moment from 'moment';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import Button from '@helpers/Button';

interface DateCustomProps {
    data:any,
    handleSetDate: (e:any) => void,
    handleCloseModal: () => void,
    openModal:boolean,
}

const DateCustom = (props: DateCustomProps & WithStyles<typeof styles>) => {
    const {classes,data,handleSetDate,handleCloseModal,openModal, ...other} = props;
    const highlightWithRanges = [
        {
            'react-datepicker__day--highlighted-custom': [
                new Date()
            ]
        }
    ]; 
    return (
        <Box className={classes.ModalCustomDate + (openModal ? ' showModal':'')}>
            <Box className={'box'}>
                <Box className={'calendar-box'}>
                    <DatePicker
                        selected={data}
                        onChange={(date:any) => handleSetDate(date)}                             
                        highlightDates={highlightWithRanges}
                        renderCustomHeader={({
                            date,
                            decreaseMonth,
                            increaseMonth,
                            prevMonthButtonDisabled,
                            nextMonthButtonDisabled,
                        }) => (
                            <Box className={classes.HeaderCustom}>
                                <IconButton className='icon'
                                    onClick={decreaseMonth}
                                    disabled={prevMonthButtonDisabled}>
                                    <IconSVG icon={iconChevronBackward}
                                        width={16}
                                        height={16}
                                        fill={COLORS.grey4}/>
                                </IconButton>
                                <Text titleSB1
                                    color={COLORS.grey7}>{moment(date).format('MMMM YYYY')}</Text>
                                <IconButton className='icon'
                                    onClick={increaseMonth}
                                    disabled={nextMonthButtonDisabled}>
                                    <IconSVG icon={iconChevronForward}
                                        width={15}
                                        height={16}
                                        fill={COLORS.grey4}/>
                                </IconButton>
                            </Box>
                        )}
                        popperClassName="some-custom-start"
                        inline/>
                </Box>
                <Box className='box-button-custom'>
                    <ButtonTransparent isElement
                        onClick={() =>{
                            handleSetDate(new Date());
                        }}
                        sx={{marginRight:'16px'}}
                        className={'submit-modal'}
                    ><Text button2>Clear</Text></ButtonTransparent>
                    <Button color={'primary'}
                        onClick={handleCloseModal}
                    >Close</Button>
                </Box>
            </Box>
        </Box>
    );
};

export default withStyles(styles)(DateCustom);