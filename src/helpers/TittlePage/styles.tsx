import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    root: {
        marginTop: 100,
        marginLeft: 340,
        padding: 40,
        backgroundColor:COLORS.grey3,
    },
    titlePageCustom: {
        marginBottom: 24,
        color: COLORS.grey5
    },
    childrenCustom: {
        backgroundColor: COLORS.white,
        borderRadius: 8,
        overflow: 'hidden',
    }
});

export default styles;