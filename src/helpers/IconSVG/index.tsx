import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import { SxProps } from '@mui/system';
import clsx from 'clsx';
import * as React from 'react';
import SVG from 'react-inlinesvg';
import styles from './styles';


interface IconSVGProps {
    icon: any,
    fill?: string,
    title?: string,
    className?: string,
    width?: number | string | undefined,
    height?: number | string | undefined,
    small?: boolean,
    sx?: SxProps,
    marginRight?: number | string,
    marginLeft?: number | string,
}


const IconSVG = (props: IconSVGProps & WithStyles<typeof styles>) => {
    const {classes, icon, fill, title, className, small, sx, marginRight, marginLeft, ...otherProps} = props;
    let {width = 24, height = 24} = props;
    if(small) {
        width = 20;
        height = 20;
    }
    return (
        <Box color={fill}
            className={clsx('icon-svg', classes.boxIconSvg)}
            style={{
                width: width,
                height: height,
                marginRight: marginRight,
                marginLeft: marginLeft,
            }}
            {...otherProps}
            sx={sx}
            component='span'>
            <SVG src={icon}
                className={className}
                preProcessor={(code) => code.replace(/fill=".*?"/g, 'fill="currentColor"')}
                title={title} 
                // width={width}
                // height={height}
            />
               
        </Box>
    );
};

export default withStyles(styles)(IconSVG);