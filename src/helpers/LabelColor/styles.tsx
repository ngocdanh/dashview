import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    LabelCustom: {
        
        '& .label': {
            padding: '0 6px',
            borderRadius: '6px',
            width: 'max-content',
            marginRight: 0,
        },
        '& .box-mode': {
            backgroundColor: COLORS.white,
            height: 24,
            padding: '4px 8px',
            borderRadius: '4px',
            minWidth: 'max-content',
        },
        '& .box-after': {
            backgroundColor: COLORS.blue,
            borderRadius: '2px',
            height: 12,
        }
    }
});

export default styles;