import { Box, Checkbox } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import { SxProps } from '@mui/system';
import * as React from 'react';
import styles from './styles';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import clsx from 'clsx';
import { UseFormReturn } from 'react-hook-form';

interface CheckboxCustomProps {
    className?: string,
    sx?: SxProps,
    ref?: HTMLInputElement | undefined,
    register?: UseFormReturn['register'],
    value?: string
}


const CheckboxCustom = React.forwardRef((props: CheckboxCustomProps & WithStyles<typeof styles>,
    ref:React.RefObject<HTMLInputElement>) => {
    const {classes, className, sx, value, ...otherProps} = props;

    return (
        <Box component={'label'}
            className={clsx(
                classes.boxCheckboxCustom,
                className && className,
                'checkbox-custom'
            )}
        >
            <input ref={ref}
                type="checkbox"
                value={value}
                {...otherProps}
            />
            <span></span>
        </Box>
    );
});

export default withStyles(styles)(CheckboxCustom);