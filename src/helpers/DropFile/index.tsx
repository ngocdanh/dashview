import { iconUpLoadFilled } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import { Box } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import styles from './styles';

interface IDropFileProps{
    title: string,
}
const DropFile = React.forwardRef((props : IDropFileProps & WithStyles<typeof styles> ,
    ref: React.RefObject<HTMLInputElement>) =>{
    const {classes, title, ...otherProps} = props;
    return (
        <Box className={classes.dropFile}>
            <label htmlFor="upload-photo"><IconSVG icon={iconUpLoadFilled}/> {title}</label>
            <input type="file"
                ref={ref}
                {...otherProps}
                id="upload-photo"
                style={{opacity:'0',position:'absolute',zIndex:'-1'}} />
        </Box>
    );
});

export default withStyles(styles)(DropFile);