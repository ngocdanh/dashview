import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: any) => createStyles({
    dropFile:{
        background:COLORS.grey2,
        width:'100%',
        height:'200px',
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:'12px',
        '& label':{
            color:COLORS.grey7,
            cursor:'pointer',
            padding: '12px 20px',
            background: COLORS.grey1,
            border: `2px solid ${COLORS.grey1}`,
            boxShadow: '0px 12px 13px -6px rgba(0, 0, 0, 0.04)',
            borderRadius: '12px',
            display: 'flex',
            gap: '8px',
            '&:hover':{
                borderColor: COLORS.grey4,
            }
        }
    }
});

export default styles;