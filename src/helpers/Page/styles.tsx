import { COLORS } from '@constants/colors';
import { Theme } from '@mui/material';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';

const styles = (theme: Theme) => createStyles({
    root: {
        marginTop: 100,
        marginLeft: 340,
        padding: 40,
        backgroundColor:COLORS.grey3,
        minHeight: 'calc(100vh - 100px)',
        '@media(max-width: 1280px)': {
            marginLeft: 300,
        },
        [theme.breakpoints.down('sm')]: {
            marginLeft: 0,
            marginTop: 60,
            padding: '40px 15px',
        },
    },
    titlePageCustom: {
        marginBottom: 24,
        color: `${COLORS.grey6} !important`,
    },
    childrenCustom: {
        // overflow: 'hidden',
        width: '100%',
        minHeight:'calc(100vh - 235px)'

    },
    contentPage: {
        // display: 'table',
        margin: '0 auto',
        maxWidth: 1200,
        width: '100%',
        zIndex: 20,
        position: 'relative',
    },
    boxTitlePage: {
        display: 'table',
        margin: '0 auto',
        maxWidth: 1200,
        width: '100%',
        marginBottom: 24,
    },
    bgPageCover: {
        height: 400,
        width: 'calc(100% + 100px)',
        zIndex: 10,
        marginLeft: -50,
        overflow: 'hidden',
        borderRadius: '20px',
        marginTop: -40,
        '& img': {
            width: '100%',
            height: 'auto',
            objectFit: 'cover',
        },
    }
});

export default styles;