import { COLORS } from '@constants/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    modalBox: {
        borderRadius: '16px',
        boxShadow: '0px 0px 14px -4px rgba(0, 0, 0, 0.05), 0px 32px 48px -8px rgba(0, 0, 0, 0.1)',
        backdropFilter: 'blur(32px)',
        padding: 24,
        background: '#0F1112',
        position: 'absolute' as const,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        '& .modal-video':{
            background:'transparent',
            '&-body':{
                maxWidth:1800,
            }
        }
    }
});

export default styles;