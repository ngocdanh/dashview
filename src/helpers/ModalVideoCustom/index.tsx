import { Backdrop, Box, Fade, Modal, SxProps } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import ModalVideo from 'react-modal-video';
import 'react-modal-video/scss/modal-video.scss';
import styles from './styles';

interface IModalVideoCustom {
    open: boolean,
    onClose: () => void,
    width?: number | string,
    height?: number | string,
    videoID: string,
    style?: any,
    channel?:string,
    sx?: SxProps,
    className?: string,
    position?: DOMRect,
}


const ModalVideoCustom = (props: IModalVideoCustom & WithStyles<typeof styles>) => {
    const {
        classes, 
        className,
        channel='youtube',
        open=false,
        width='100vw',
        height='100vh',
        videoID,
        onClose,
        style, 
        sx,
        position,
        ...otherProps} = props;

    return (
        <Modal
            aria-labelledby="modal-custom"
            BackdropProps= {{
                timeout: 1000,
                classes: {
                    root: className && className,
                }
            }}
            open={open}
            onClose={onClose}
            BackdropComponent={Backdrop}
            disableScrollLock={position ? false: true}
            style={{
                ...style,
            }}
            sx={sx}
            {...otherProps}
        >
            <Fade in={open}>
                <Box component={'div'}
                    className={clsx(classes.modalBox, 'modal-custom')}
                    style={{
                        ...width && {width},
                        ...height && {minHeight: height},
                        ...position && {
                            top: position.y,
                            left: position.x - (width || 0) + 40,
                            transform: 'unset',
                        } 
                    }}
                > 
                    <ModalVideo channel={channel}
                        autoplay
                        isOpen={open}
                        videoId={videoID}
                        onClose={() => onClose()} />
                </Box> 
            </Fade>
        </Modal>
    );
};

export default withStyles(styles)(ModalVideoCustom);