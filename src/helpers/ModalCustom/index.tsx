import { COLORS } from '@constants/colors';
import { iconCloseLight } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import TitleElement from '@helpers/TitleElement';
import { Box, Modal, SxProps, IconButton, Backdrop, Fade } from '@mui/material';
import Popover from '@mui/material/Popover';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import styles from './styles';

interface IModalCustom {
    children: React.ReactNode | undefined,
    open: boolean,
    onClose: () => void,
    title?: string,
    width?: number,
    height?: number,
    style?: any,
    sx?: SxProps,
    className?: string,
    bgTitle?: string,
    position?: DOMRect,
    fullPage?: boolean,
}


const ModalCustom = (props: IModalCustom & WithStyles<typeof styles>) => {
    const {
        classes, 
        className,
        children, 
        open=false,
        title,
        width,
        height,
        onClose,
        style, 
        sx,
        bgTitle,
        position,
        fullPage,
        ...otherProps} = props;

    return (
        <Modal
            aria-labelledby={`modal-custom-${Date.parse((new Date()).toString())}`}
            BackdropProps= {{
                timeout: 300,
                classes: {
                    root: className && className,
                }
            }}
            open={open}
            onClose={onClose}
            BackdropComponent={Backdrop}
            disableScrollLock={position || fullPage  ? false: true}
            style={{
                ...style,
            }}
            sx={sx}
            {...otherProps}
        >
            <Fade in={open}>
                <Box component={'div'}
                    className={clsx(classes.modalBox, 'modal-custom', {
                        [classes.fullPage]: fullPage
                    })}
                    style={{
                        ...width && {width},
                        ...height && {minHeight: height},
                        ...position && {
                            top: position.y,
                            left: position.x - (width || 0) + 40,
                            transform: 'unset',
                        } 
                    }}
                >
                    <Box className={'box-title'}
                        display='flex'
                        justifyContent={'space-between'}>
                        <Box>
                            {title &&  <TitleElement bg={bgTitle}>{title}</TitleElement>}
                        </Box>
                        <IconButton className={clsx('icon-btn-closed', {
                            ['white']: fullPage
                        })}
                        onClick={onClose}>
                            <IconSVG icon={iconCloseLight}
                                fill={COLORS.grey5}/>
                        </IconButton>
                    </Box>
                    {children}
                </Box>
            </Fade>
        </Modal>
    );
};

export default withStyles(styles)(ModalCustom);