import { COLORS } from '@constants/colors';
import { SxProps, Tooltip, Zoom } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import * as React from 'react';
import styles from './styles';

interface TooltipCustomProps {
    children: React.ReactNode | undefined,
    title: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal,
    color?: string, 
    bg?: string, 
    className?: string,
    sx?: SxProps
}


const TooltipCustom = (props: TooltipCustomProps & WithStyles<typeof styles>)=> {
    const {classes, children, bg, color, className, sx, title} = props;
    return (
        <Tooltip sx={{
            ...color && {color: color},
            ...bg && {backgroundColor: bg},
            lineHeight: 1,
            ...sx && sx,
        }}
        arrow
        componentsProps={{
            tooltip: {
                sx: {
                    bgcolor:  COLORS.grey7,
                    '& .MuiTooltip-arrow': {
                        color: 'common.black',
                    },
                },
            },
        }}
        title={title}
        placement="top-start"
        leaveTouchDelay={1000}
        className={className}
        TransitionComponent={Zoom}
        >
            <div> {children}</div>
        </Tooltip>
    );
};

export default withStyles(styles)(TooltipCustom);