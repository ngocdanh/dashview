/* eslint-disable react/prop-types */
import { COLORS } from '@constants/colors';
import { iconArrowForward, iconBackForward } from '@constants/imageAssets';
import CheckboxCustom from '@helpers/CheckboxCustom';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, IconButton, SxProps, Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import { usePagination, useRowSelect, useTable } from 'react-table';
import { v4 as uuidv4 } from 'uuid';
import styles from './styles';
import { isMobile } from '../../utils/index';

interface TableCustomProps {
    data: any,
    checkbox?: boolean,
    columns: any,
    className?: string,
    sx?: SxProps,
    handleSelectRow?: (e:React.MouseEvent<HTMLElement>) => void,
    options?: {
        hoverRow?:boolean,
        centerHeader?: boolean,
        centerTable?: boolean,
        changePage?: boolean,
    },
}

// eslint-disable-next-line react/display-name
const IndeterminateCheckbox = 
    // eslint-disable-next-line react/prop-types
    ({ indeterminate, classes, ...rest }, ref) => {
        const defaultRef = React.useRef();
        const resolvedRef = ref || defaultRef;

        React.useEffect(() => {
            if(resolvedRef.current)
                resolvedRef.current.indeterminate = indeterminate;
        }, [resolvedRef, indeterminate]);

        return (
            <>
                <CheckboxCustom
                    {...rest} />
            </>
        );
    };



const TableCustom = (props: TableCustomProps & WithStyles<typeof styles>)=> {
    const {classes, columns, className, sx, data, checkbox, options, handleSelectRow} = props;

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        prepareRow,
        nextPage,
        selectedFlatRows,
        previousPage,
        state: { pageIndex, pageSize },
    } = useTable({
        columns,
        data,
        initialState: { pageIndex: 0 },
    },
    usePagination,
    useRowSelect,
    hooks => {
        if(checkbox && !isMobile)
            hooks.visibleColumns.push(columns => [
                // Let's make a column for selection
                {
                    id:'selection',
                    // The header can use the table's getToggleAllRowsSelectedProps method
                    // to render a checkbox
                    Header: ({ getToggleAllRowsSelectedProps }) => (
                        <>
                            <IndeterminateCheckbox classes={{
                                root: classes.checkboxRoot,
                                checked: classes.checkboxChecked
                            }}
                            {...getToggleAllRowsSelectedProps()} />
                        </>
                    ),
                    width: 20,
                    // eslint-disable-next-line react/prop-types
                    Cell: ({ row }) => (
                        <div>
                            <IndeterminateCheckbox classes={{
                                root: classes.renderCheckboxRoot,
                                checked: classes.renderCheckboxChecked
                            }}
                            // eslint-disable-next-line react/prop-types
                            {...row.getToggleRowSelectedProps()} />
                        </div>
                    ),
                },
                ...columns,
            ]);
    }
    );
    React.useEffect(
        () =>{
            const rowOriginal = selectedFlatRows.map((row:any) => row.original);
            handleSelectRow && handleSelectRow(rowOriginal);
        },[selectedFlatRows]
    );
    
    return (
        <Box className={classes.root}>
            <Box className={classes.boxTable}>
                <Table className={clsx(classes.tableCustom,className, 'table-custom')}
                    sx={sx}
                    {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup: any) => (
                            <TableRow key={uuidv4()}
                                {...headerGroup.getHeaderGroupProps()}
                                className={classes.tableRowHeader}
                            >
                                {headerGroup.headers.map((column: any) => {

                                    return (
                                        <TableCell key={uuidv4()}
                                            {...column.getHeaderProps({
                                                style: {
                                                    minWidth: column.minWidth,
                                                    maxWidth: column.maxWidth,
                                                    width: column.width
                                                },
                                            })}
                                        >
                                            {column.RenderHeader ? column.RenderHeader()  :<Text baseSB1
                                                color={COLORS.grey4}>
                                                {column.render('Header')}
                                            </Text>}

                                        </TableCell>
                                    );
                                })}
                            </TableRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {page.map(
                            (row: any, i: number) => {
                                prepareRow(row);
                                return (
                                    <TableRow
                                        {...row.getRowProps()}
                                        key={uuidv4()}
                                        className={clsx(classes.tableRowBody,
                                            {
                                                ['active']: options?.hoverRow
                                            })}>
                                        {row.cells.map((cell: any) => {
                                            return (
                                                <TableCell key={uuidv4()}
                                                    className={classes.tableCellBody}
                                                    {...cell.getCellProps({
                                                        style: {
                                                            minWidth: cell.column.minWidth,
                                                            maxWidth: cell.column.maxWidth,
                                                            width: cell.column.width,
                                                        },
                                                    })}>
                                                    {cell.render('Cell')}
                                                </TableCell>
                                            );
                                        })}
                                    </TableRow>
                                );}
                        )}
                    </TableBody>
                </Table>
            </Box>
            {options?.changePage && (<Box className={classes.boxChangePage}
                display={'flex'}
                justifyContent={'center'}>
                <IconButton disabled={pageIndex === 0}
                    className={'icon-button'}
                    onClick={previousPage}>
                    <IconSVG icon={iconBackForward}
                        fill={pageIndex === 0? COLORS.grey3: COLORS.grey4}/>
                </IconButton>
                <IconButton  disabled={pageIndex === pageSize -1}
                    className={'icon-button'}
                    onClick={nextPage}>
                    <IconSVG icon={iconArrowForward}
                        fill={pageIndex === pageSize -1 ? COLORS.grey3:COLORS.grey4}/>
                </IconButton>
            </Box>)}

        </Box>
    );
};

export default withStyles(styles)(TableCustom);