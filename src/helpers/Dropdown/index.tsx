import { Box } from '@mui/material';
import Popover from '@mui/material/Popover';
import { withStyles, WithStyles } from '@mui/styles';
import * as React from 'react';
import styles from './styles';

interface DropdownCustomProps {
    children: React.ReactNode,
    open: boolean,
    anchorEl: any,
    onClose: any,
}


const DropdownCustom = (props: DropdownCustomProps & WithStyles<typeof styles>) => {
    const {classes, children, open=false, anchorEl, onClose, ...other} = props;
  
    return (
        <React.Fragment>
            <Popover
                disableScrollLock
                open={open}
                className={classes.popverStyle}
                anchorEl={anchorEl}
                onClose={onClose}
                anchorOrigin={{
                    vertical: 50,
                    horizontal: -250,
                }}
                {...other}
            >
                <Box className={classes.childrenPopover}>
                    {children}
                </Box>
            </Popover>
        </React.Fragment>
    );
};

export default withStyles(styles)(DropdownCustom);