
import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import React, { useState } from 'react';
import { OnChangeValue } from 'react-select';
import CreatableSelect from 'react-select/creatable';
import styles from './styles';
const components = {
    DropdownIndicator: null
};
interface Option {
    label:string,
    value: string;
}
const createOption = (label: string) => ({
    label,
    value: label
});

interface State {
    optionInput: string;
    options: Option[];
}
const ReactTag = React.forwardRef((props:any & WithStyles<typeof styles> , ref: React.RefObject<HTMLInputElement>)  => {
    const {classes,setTags} = props;
    const [optionArray, setOptionArray] = useState<State>({
        optionInput: '',
        options: []
    });

    const onChangeHandle = (newOptions:any) => {
        setOptionArray({...optionArray,options: newOptions});
    };

    const onInputChangeHandle = (inputValue: string) => {
        setOptionArray({ ...optionArray, optionInput: inputValue });
    };

    const onKeyDownHandle = (event: React.KeyboardEvent<HTMLDivElement>) => {
        const { optionInput, options } = optionArray;
        if (!optionInput) return;
        switch (event.key) {
            case 'Enter':
            case 'Tab':
                // eslint-disable-next-line no-case-declarations
                let checkExisted = false;
                options.forEach((option) => {
                    if (option.value.trim() === optionInput.trim()) {
                        checkExisted = true;
                    }
                });
                if (!checkExisted && options.length < 12) {
                    setOptionArray({
                        optionInput: '',
                        options: [...options, createOption(optionInput)]
                    });
                    setTags([...options, createOption(optionInput)]);
                }
                event.preventDefault();
        }
    };
    return (
        <Box className={classes.reactTag}>
            <CreatableSelect
                ref={ref}
                components={components}
                inputValue={optionArray.optionInput}
                isClearable
                isMulti
                menuIsOpen={false}
                onChange = {onChangeHandle}
                onInputChange={onInputChangeHandle}
                onKeyDown={onKeyDownHandle}
                placeholder="Enter tags to describe your item"
                value={optionArray.options}
            />
        </Box>
    );
});

export default withStyles(styles)(ReactTag);