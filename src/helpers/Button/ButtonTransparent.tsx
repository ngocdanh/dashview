import { Box } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import Text from '../Text';
import styles from './styles';
import { SxProps } from '@mui/system';

interface IButtonTransparent {
    children: React.ReactNode,
    onClick?: ()=> void;
    className?: string;
    isElement?: boolean;
    isLoading?: boolean,
    disabled?: boolean,
    style?: any,
    disableBorder?: boolean,
    sx?: SxProps,
    iconLeft?: string,
}


const ButtonTransparent = (props: IButtonTransparent & WithStyles<typeof styles>) => {
    const {classes, 
        children, 
        className,
        isElement, 
        isLoading,
        disabled,
        style,
        disableBorder, 
        ...otherProps} = props;
    return (
        <React.Fragment>
            <Box 
                className={clsx(
                    className && className,
                    classes.ButtonTransparent, 
                    'button-transparent',
                    disableBorder && 'disableBorder',
                    disabled && 'disabled')}
                style={style}
                {...otherProps}
            >
                {isLoading && (
                    <Box className={classes.loading}>
                        <span></span>
                        <span></span>
                        <span></span>
                    </Box>
                )}
                {isElement ? children: (<Text center
                    className='button-text'>
                    {children}
                </Text>)}
                
            </Box>
        </React.Fragment>
    );
};

export default withStyles(styles)(ButtonTransparent);