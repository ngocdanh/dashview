
import { clearLocalStorage, setUser } from '@commons/storage';
import { toastOption } from '@helpers/ToastCustom';
import { initializeApp } from 'firebase/app';
import {
    createUserWithEmailAndPassword,
    FacebookAuthProvider, 
    getAuth,
    GoogleAuthProvider, 
    sendPasswordResetEmail, 
    signInWithEmailAndPassword,
    signInWithPopup, 
    signOut,
    updateProfile,
} from 'firebase/auth';
import { toast } from 'react-toastify';

const firebaseConfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId:  process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID,
    measurementId: process.env.REACT_APP_MEASUREMENT_ID
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const googleProvider = new GoogleAuthProvider();
const facebookAuthProvider = new FacebookAuthProvider();

const signInWithGoogle = async () => {
    try {
        const res = await signInWithPopup(auth, googleProvider);
        const user = res.user;
        
        await setUser(user);
        await window.location.replace('/');
        
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
};
const signInWithFacebook = async () => {
    try {
        const res = await signInWithPopup(auth, facebookAuthProvider);
        const user = res.user;
        await setUser(user);
        await window.location.replace('/');
        
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
};

const logInWithEmailAndPassword = async (data: {email: string, password: string}) => {
    const {email, password} = data;
    try {
        const res = await signInWithEmailAndPassword(auth, email, password);
        const user = res.user;
        setUser(user);
        window.location.replace('/');
        
    } catch (err) {
        console.error(err);
        
    }
};

const registerWithEmailAndPassword = async ( data: {name: string,email: string, password: string}) => {
    const {email, password, name} = data;
    try {
        const res = await createUserWithEmailAndPassword(auth, email, password);
        const user = res.user;
        await updateProfile(user, {displayName: name});
        await setUser(user);
        window.location.replace('/');
    } catch (err) {
        console.error(err);
        // alert(err.message);
        const {message} = err;
        if(message === 'Firebase: Error (auth/email-already-in-use).') {
            toast.error('Email already in use', toastOption);
        }
        return false;
    }
};

const sendPasswordReset = async (email: string) => {
    try {
        await sendPasswordResetEmail(auth, email);
        alert('Password reset link sent!');
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
};

const logout = () => {
    auth.signOut();
    clearLocalStorage();
    window.location.replace('/');
};

export {
    auth,
    signInWithGoogle,
    logInWithEmailAndPassword,
    registerWithEmailAndPassword,
    sendPasswordReset,
    signInWithFacebook,
    logout,
};
