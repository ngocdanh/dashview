import { TOKEN } from '@constants/index';
import _ from 'lodash';

export const getUser = () => {
    try {
        const data = localStorage.getItem(TOKEN);
        if(!_.isObject(data)) {
            return JSON.parse(data);
        }
        return data; 
    } catch (error) {
        return null;
    }
};

export const setUser = (data: any) => {
    localStorage.setItem(TOKEN, JSON.stringify(data));
};
export const deleteToken = () => {
    try {
        return localStorage.removeItem(TOKEN);
    } catch (error) {
        return false;
    }
};
export const clearLocalStorage = () => {
    try {
        return localStorage.clear();
    } catch (error) {
        return null;
    }
};