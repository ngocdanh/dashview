const path = (root: string, subLink: string) => {
    return `${root}${subLink}`;
};

const ROOT_PATH = '/auth';
const ROOT_DASHBOARD = '/dashboard';

export const PATH_PAGE = {

};

export const PATH_DASHBOARD = {
    home: ROOT_DASHBOARD,
    path: path(ROOT_DASHBOARD, '/home'),
    Create: {
        path: path(ROOT_DASHBOARD,'/product-add')
    },
    affiliate:{
        path: path(ROOT_DASHBOARD,'/affiliate-center')
    },
    products: {
        path: path(ROOT_DASHBOARD, '/products'),
        Dashboard: path(ROOT_DASHBOARD, '/app'),
        Drafts: path(ROOT_DASHBOARD, '/drafts'),
        Released: path(ROOT_DASHBOARD, '/released'),
        Comment: path(ROOT_DASHBOARD, '/comments'),
        Scheduled: path(ROOT_DASHBOARD, '/scheduled'),
    },
    customers: {
        path: path(ROOT_DASHBOARD, '/customers'),
        Overview: path(ROOT_DASHBOARD, '/overview'),
        CustomerList: path(ROOT_DASHBOARD, '/customer-list'),
    },
    shop: {
        path: path(ROOT_DASHBOARD, '/shop'),
    },
    Income: {
        path: path(ROOT_DASHBOARD, '/income'),
        Earning: path(ROOT_DASHBOARD, '/earning'),
        Refunds: path(ROOT_DASHBOARD, '/refunds'),
        Payouts: path(ROOT_DASHBOARD, '/payouts'),
        Statements: path(ROOT_DASHBOARD, '/statements'),
    },
    Promote: {
        path: path(ROOT_DASHBOARD, '/promote')
    }
};

