import { getUser } from '@commons/storage';
import { COLORS } from '@constants/colors';
import { iconBarChartFilled,
    iconGridLight,
    iconLeaderBoardFilled,
    iconTicketLight,
    imgUserDefault } from '@constants/imageAssets';
import Dropdown from '@helpers/Dropdown';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, Divider, Icon, List } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import { logout } from '@services/firebase';
import clsx from 'clsx';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './styles';

interface IUserInfoHeader {
    isOpenSlidebar?: boolean,
    onCloseSlidebar?: any,
}

const UserInfoHeader = (props: IUserInfoHeader & WithStyles<typeof styles>) => {
    const navigate = useNavigate();
    const {classes, isOpenSlidebar, onCloseSlidebar} = props;
    const [anchorElNotification, setAnchorElNotification] = React.useState<HTMLDivElement | null>(null);
    const userInfo = getUser();
    const handleOpenDropdownNotification = (event:React.MouseEvent<HTMLDivElement>) => {
        setAnchorElNotification(event.currentTarget);
    };
    const handleCloseDropdownUseInfo = () => {
        setAnchorElNotification(null);
    };

    const open = Boolean(anchorElNotification);

    return (
        <React.Fragment>
            <Icon
                component={'div'}
                className={classes.iconUseRight}
                onClick={handleOpenDropdownNotification}
            >

                <img src={userInfo.photoURL || imgUserDefault}
                    width={48}
                    style={{borderRadius: '50%'}}
                    height={48}/>
            </Icon>
            <Dropdown
                anchorEl={anchorElNotification}
                open={open}
                onClose={handleCloseDropdownUseInfo}
            >
                <Box
                    className={clsx(classes.dropdownList, classes.dropdownUseInfo)}
                >
                    <List>
                        <Box component={'a'}
                            marginBottom={1}
                            href='/dashboard/shop'
                            className='item-info'>
                            <Text color={COLORS.grey4}
                                baseSB1>
                            Profile
                            </Text>
                        </Box>
                        {/* <Box className='item-info'
                            component={'a'}
                            href='/dashboard/shop'>
                            <Text color={COLORS.grey4}
                                baseSB1>
                            Edit Profile
                            </Text>
                        </Box> */}
                        <Divider/>
                        <Box className='group-items'>
                            <Box className='item-info'>
                                <IconSVG icon={iconBarChartFilled}/>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Analytics
                                </Text>
                            </Box>
                            <Box className='item-info'>
                                <IconSVG icon={iconTicketLight}/>
                                <Text color={COLORS.grey4}
                                    baseSB1
                                    onClick = {() => {
                                        navigate('affiliate-center');
                                    }}>
                            Affiliate center
                                </Text>
                            </Box>
                            <Box className='item-info'>
                                <IconSVG icon={iconGridLight}/>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Explore authors
                                </Text>
                            </Box>
                        </Box>
                        <Divider/>
                        <Box className="group-items">
                            <Box component='a'
                                className='item-info violet'
                                href='/dashboard/upgrade'>
                                <IconSVG icon={iconLeaderBoardFilled}/>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Upgrade to Pro
                                </Text>
                            </Box>
                        </Box>
                        <Divider/>
                        <Box className="group-items">
                            <Box className='item-info'>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Account Settings
                                </Text>
                            </Box>
                            <Box className='item-info'
                                onClick={logout}>
                                <Text color={COLORS.grey4}
                                    baseSB1>
                            Logout
                                </Text>
                            </Box>
                        </Box>
                    </List>
                </Box>
            </Dropdown>
        </React.Fragment>
    );
};

export default withStyles(styles)(UserInfoHeader);