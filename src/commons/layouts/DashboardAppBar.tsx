import { getUser } from '@commons/storage';
import { COLORS } from '@constants/colors';
import { iconMenuLight } from '@constants/imageAssets';
import Button from '@helpers/Button';
import ButtonTransparent from '@helpers/Button/ButtonTransparent';
import IconSVG from '@helpers/IconSVG';
import { AppBar, Box, Toolbar, IconButton } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import BoxSearch from './BoxSearch';
import MessageHeader from './MessageHeader';
import NotificationHeader from './NotificationHeader';
import styles from './styles';
import UseInfoHeader from './UseInfoHeader';

interface DashboardAppBarProps {
    open?: boolean,
    search?: string,
    handleSearch?: (e: any)=> void,
    handleCloseDrawer?: any,
    scrollPage?: boolean,
    handleOpenMenuMobile: (status: boolean) => void,
    activeMenu: boolean,
}

const DashboardAppBar = (props: DashboardAppBarProps & WithStyles<typeof styles>) => {
    const {classes, search, handleSearch, handleOpenMenuMobile, activeMenu } = props;
    const isAuthencated = getUser();
    const navigate = useNavigate();
    return (
        <React.Fragment>
            <AppBar
                color={'primary'}
                className={clsx(classes.appBarStyle)}
            >
                <Toolbar
                    className={clsx(classes.toolbarStyle)}
                >
                    
                    <IconButton className='open-menu'
                        onClick={
                            () => {handleOpenMenuMobile(!activeMenu);}}>
                        <IconSVG icon={iconMenuLight}
                            fill={COLORS.grey4}/>
                    </IconButton>
                    <Box className='group-search'>
                        <BoxSearch search={search}
                            handleSearch={handleSearch}/>
                    </Box>
                    <Box
                        className={classes.boxRight}
                    >
                        {!isAuthencated ? (
                            <>
                                <ButtonTransparent
                                    disableBorder
                                    className={classes.buttonSignIn}
                                    onClick={()=>{ navigate('/sign-in');}}
                                >Sign in</ButtonTransparent>
                                <Button
                                    className='btn-signUp'
                                    color={'primary'}
                                    onClick={()=>{ navigate('/sign-up');}}
                                >Sign Up</Button>
                            </>
                        ): <>
                            <Button
                                className='button-create'
                                color={'primary'}
                                isIconBefore
                                onClick={()=>{ navigate('/dashboard/product-add');}}
                            >Create</Button>
                            <MessageHeader/>
                            <NotificationHeader/>
                            <UseInfoHeader/></>}
                        
                    </Box>
                </Toolbar>
            </AppBar>
        </React.Fragment>
    );
};
export default withStyles(styles)(DashboardAppBar);