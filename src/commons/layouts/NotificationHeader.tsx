import {Box, Button, Divider, Icon, List, ListItemAvatar, ListItemButton, ListItemText, Popover, Typography} from '@mui/material';
import styles from './styles';
import { withStyles, WithStyles } from '@mui/styles';
import * as React from 'react';
import Text from '@helpers/Text';
import { iconChevronUp,iconChevronDown, logo, IconNotification } from '@constants/imageAssets';
import { Link as RouterLink, } from 'react-router-dom';
import sidebarConfig from './sidebarConfig';
import _ from 'lodash';
import Dropdown from '@helpers/Dropdown';
import iconMoreHorizontal from '@assets/icons/more_horizontal/filled.svg';
import avatarNotification1 from '@assets/images/notification1.png';
import avatarNotification2 from '@assets/images/notification2.png';
import avatarNotification3 from '@assets/images/notification3.png';
import avatarNotification4 from '@assets/images/notification4.png';
import avatarNotification5 from '@assets/images/notification5.png';
import { COLORS } from '@constants/colors';
import ButtonCustom from '@helpers/Button';
import { v4 as uuidv4 } from 'uuid';
import IconSVG from '@helpers/IconSVG';

interface NotificationHeaderProps {
    isOpenSlidebar?: boolean,
    onCloseSlidebar?: any,
}

const NotificationHeader = (props: NotificationHeaderProps & WithStyles<typeof styles>) => {
    const {classes, isOpenSlidebar, onCloseSlidebar} = props;
    const [anchorElNotification, setAnchorElNotification] = React.useState<HTMLDivElement | null>(null);

    const handleOpenDropdownNotification = (event:React.MouseEvent<HTMLDivElement>) => {
        setAnchorElNotification(event.currentTarget);
    };
    const handleCloseDropdownNotification = () => {
        setAnchorElNotification(null);
    };

    const listNotiDemo = [
        {
            name: 'Domenica',
            username: '@domenica',
            image: avatarNotification1,
            content: 'Smiles - 3D icons',
            type: 'Comment',
            time: '1h'
        },
        {
            name: 'Jerica',
            username: '@jerica',
            image: avatarNotification2,
            content: 'Smiles - 3D icons',
            type: 'Like',
            time: '2h'
        },
        {
            name: 'Monaco',
            username: '@monaco',
            image: avatarNotification3,
            content: 'Smiles - 3D icons',
            type: 'Purchase',
            time: '2h'
        },
        {
            name: 'Jinira',
            username: '@hirika',
            image: avatarNotification4,
            content: 'Smiles - 3D icons',
            type: 'Rate',
            time: '3h'
        },
        {
            name: 'Okizawoa',
            username: '@domonico',
            image: avatarNotification5,
            content: 'Smiles - 3D icons',
            type: 'Comment',
            time: '5h'
        },
    ];

    const renderItem = (data: {
        name: string,
        image: string,
        username: string,
        type: string,
        content: string,
        time: string,
    }, idx: number) => {
        const {image, name, username, type, content, time}= data;
        
        return (
            <Box component={'div'}
                key={uuidv4()}>
                <ListItemButton
                    className={classes.listItemButton}>
                    <Box className={classes.itemNoti}>
                        <Box component={'div'}
                            className={classes.dFlex}>
                            <ListItemAvatar>
                                <img src={image}
                                    width={48}
                                    height={48}/>  
                            </ListItemAvatar>
                            <Box component={'div'}>
                                <ListItemText primary={
                                    <div>
                                        <Text base1
                                            className={classes.itemName}>{name}</Text> 
                                        <Text base1
                                            color={COLORS.grey4}
                                            className={classes.itemUsername}>{username}</Text> 
                                    </div>
                                }/>
                                <ListItemText primary={
                                    <div>
                                        <Text bodyM1
                                            color={COLORS.grey4}
                                            className={classes.itemName}>{type}</Text> 
                                        <Text base1
                                            className={classes.itemUsername}>{content}</Text> 
                                    </div>
                                }/>
                            </Box>
                        </Box>
                        <Box component={'div'}
                            className={classes.boxUnread}>
                            <Text caption1
                                color={COLORS.grey4}
                                className={classes.itemItem}>{time}</Text>
                            <Box className={classes.dotUnread}/>
                        </Box>
                    </Box>
                    
                </ListItemButton>
                {idx !== listNotiDemo.length -1 && <Divider/>}
            </Box>
        );
    };

    const open = Boolean(anchorElNotification);
  
    return (
        <React.Fragment>
            <Icon
                component={'div'}
                className={classes.iconRight}
                onClick={handleOpenDropdownNotification}
            >
                <IconSVG icon={IconNotification}
                    fill={COLORS.grey4}
                    width={24}
                    height={24}/>
                  
                <Box
                    className={classes.dotNotification}
                />
            </Icon>
            <Dropdown
                anchorEl={anchorElNotification}
                open={open}
                onClose={handleCloseDropdownNotification}
            >
                <Box
                    className={classes.dropdownList}
                >
                    <Box component={'div'}
                        className={classes.boxHeader}
                    >
                        <Text titleSB1
                            className={classes.titleDropDown}>
                    Notification
                        </Text>
                        <div className={classes.boxIconMore}>
                            <img src={iconMoreHorizontal}
                                width={24}
                                height={24}/>
                        </div>
                    </Box>
                    <List>
                        { !_.isEmpty(listNotiDemo) && listNotiDemo.map((item, index)=> 
                            renderItem(item, index))}
                    </List>
                    <ButtonCustom color={'primary'}
                        className={classes.buttonAll}>
                    See all notifications
                    </ButtonCustom>
                </Box>
            </Dropdown>
        </React.Fragment>
    );
};

export default withStyles(styles)(NotificationHeader);