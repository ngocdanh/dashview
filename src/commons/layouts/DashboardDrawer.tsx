import iconAdd from '@assets/icons/add/light.svg';
import { COLORS } from '@constants/colors';
import { iconChevronDown, iconChevronForward, iconChevronUp, iconCloseLight, logo } from '@constants/imageAssets';
import IconSVG from '@helpers/IconSVG';
import Text from '@helpers/Text';
import { Box, Collapse, Drawer, IconButton, List, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import _ from 'lodash';
import * as React from 'react';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import sidebarConfig from './sidebarConfig';
import styles from './styles';
import { isMobile } from '../../utils/index';

interface DashboardDrawerProps {
    isOpenSlidebar?: boolean,
    onCloseSlidebar?: any,
    activeMenu: boolean,
    handleOpenMenuMobile: (status: boolean) => void,
}

interface ISlideBar {
    subHeader: string,
    icon: string,
    path: string,
    addProduct?: boolean,
    items: Array<{
        title: string,
        path: string,
        notification?: number,
        colorNoti?: string
    }>,
    
}

const DRAWER_WIDTH = 340;

const DashboardDrawer = React.memo((props: DashboardDrawerProps & WithStyles<typeof styles>) => {
    const {classes, 
        isOpenSlidebar,
        onCloseSlidebar,
        activeMenu,
        handleOpenMenuMobile
    } = props;
    const location = useLocation();
    const navigate = useNavigate();
    const [selectedIndex, setSelectedIndex] = React.useState<number>(-1);
    const [selectSubIndex, setSelectSubIndex] = React.useState<Array<number>>([-1, -1]);
   
    React.useEffect(() => {
        for(let i = 0; i<sidebarConfig.length; i++) {
            const subHeader = sidebarConfig[i].subHeader.toLowerCase();
            const items = sidebarConfig[i].items;
            if(location.pathname.indexOf(subHeader) !== -1) {
                setSelectedIndex(i);
                return;
            } else {
                for(let j = 0; j< items.length; j++) {
                    const subItemHeader = items[j].path.toLowerCase();
                    if(location.pathname.indexOf(subItemHeader) !== -1) {
                        setSelectedIndex(i);
                        setSelectSubIndex([i,j]);
                        return;
                    }
                }
            }
        }
    }, [location]);

    const handleChangeTab = (value: number) => {
        if(selectedIndex === value) {
            setSelectedIndex(-1);
            return;
        }
        setSelectedIndex(value);
        if(_.isEmpty(sidebarConfig[value]?.items))
            isMobile && handleOpenMenuMobile(false);
    };
    const handleChangeSubTab = (value: number, indexParent: number) => {
        setSelectedIndex(indexParent);
        setSelectSubIndex([indexParent,value]);
        isMobile && handleOpenMenuMobile(false);
    };

    const renderChevron = (status: boolean) => {
        if(status)
            return <img src={iconChevronUp}
                width={24}
                style={{marginLeft: 20}}
                height={24}/>;
        else
            return <img src={iconChevronDown}
                width={24}
                style={{marginLeft: 20}}
                height={24}/>;
    };

    const renderChildren = (status: boolean, data: any, indexParent: number) => {
           
        return (
            <Collapse in={status}
                timeout='auto'
                unmountOnExit>
                <List component={'div'}
                    className={classes.listChildren}
                    disablePadding>
                    {!_.isEmpty(data) && data.map((subMenu: any, index: number)=> {
                        const selectedSubMenu = selectSubIndex[1] === index && selectSubIndex[0] === indexParent;  
                        return (
                            <ListItemButton 
                                key={uuidv4()} 
                                component={RouterLink}
                                to={subMenu.path}
                                onClick={()=> handleChangeSubTab(index, indexParent)}
                                selected={selectedSubMenu}
                            >
                                <ListItemText primary={
                                    <Text base1
                                        color={selectedSubMenu ? COLORS.grey7: COLORS.grey4}
                                        className={classes.textButton}>{subMenu.title}</Text>
                                }/>
                                {subMenu?.notification && <Box component={'div'} 
                                    className={classes.notification}
                                    sx={{backgroundColor: subMenu?.colorNoti}}
                                ><Text base1>{subMenu?.notification}</Text></Box>}
                                {!subMenu?.notification && selectedSubMenu && (
                                    <IconSVG icon={iconChevronForward}
                                        fill={COLORS.grey7}
                                        width={24}
                                        height={24}/>
                                )}
                            </ListItemButton>
                        );
                    })}
                    
                </List>
            </Collapse>
        );
    };

    const renderAddProduct = () => {
        return (
            <Box
                component={'div'}
                className={classes.boxIconAdd}
                onClick={(event: React.MouseEvent<HTMLElement>) =>{
                    event.stopPropagation();
                    navigate('/dashboard/product-add');
                    isMobile && handleOpenMenuMobile(false);
                }}
            >
                <img src={iconAdd}
                    width={13}
                    height={13}/>
            </Box>
        );
    };

    const renderSlidebar = () => {
        return (
            <Box className={classes.slidebar}>
                <Box component={RouterLink}
                    to={'/'}
                    className='logo'
                    sx={{display: 'inline-block'}}>
                    <img src={logo}
                        className={classes.styleLogo}
                        alt="" />
                </Box>
                <List
                    sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
                    component='nav'
                    disablePadding
                    aria-labelledby="nested-list-subheader"
                    className={classes.listMenu}
                >
                    {
                        sidebarConfig.map((item: ISlideBar, index)=> (
                            <React.Fragment key={uuidv4()}>
                                {
                                    !_.isEmpty(item.items) ? (
                                        <ListItemButton
                                            onClick={() => handleChangeTab(index)}
                                            selected={selectedIndex === index && _.isEmpty(item.items)}
                                        >
                                            <ListItemIcon>
                                                <IconSVG icon={item.icon}
                                                    className={classes.iconButton}
                                                    fill={selectedIndex === index && _.isEmpty(item.items) 
                                                        ? COLORS.grey7 : COLORS.grey4}
                                                    title="" />
                                            </ListItemIcon>
                                            <ListItemText disableTypography
                                                primary={<Text base1
                                                    color={selectedIndex === index && _.isEmpty(item.items) 
                                                        ? COLORS.grey7 : COLORS.grey4}
                                                    className={classes.textButton}>{item.subHeader}</Text>}  />
                                            {item?.addProduct && (
                                                renderAddProduct()
                                            )}
                                            {
                                                !_.isEmpty(item.items) && (
                                                    renderChevron(selectedIndex === index)
                                                )
                                            }
                                        </ListItemButton>
                                    ): (
                                        <ListItemButton
                                            component={RouterLink}
                                            to={item.path}
                                            onClick={() => handleChangeTab(index)}
                                            selected={selectedIndex === index && _.isEmpty(item.items)}
                                        >
                                            <ListItemIcon>
                                                <IconSVG icon={item.icon}
                                                    className={classes.iconButton}
                                                    fill={selectedIndex === index && _.isEmpty(item.items) 
                                                        ? COLORS.grey7 : COLORS.grey4}
                                                    title="" />
                                            </ListItemIcon>
                                            <ListItemText disableTypography
                                                primary={<Text base1
                                                    color={selectedIndex === index && _.isEmpty(item.items) 
                                                        ? COLORS.grey7 : COLORS.grey4}
                                                    className={classes.textButton}>{item.subHeader}</Text>}  />
                                            {
                                                !_.isEmpty(item.items) && (
                                                    renderChevron(selectedIndex === index)
                                                )
                                            }
                                        </ListItemButton>
                                    )
                                }
                                {renderChildren(selectedIndex === index, item.items, index)}
                            </React.Fragment>
                        ))
                    }
                </List>
            </Box>
        );
    };


    return (
        <Drawer
            open={isOpenSlidebar}
            onClose={onCloseSlidebar}
            variant="permanent"
            className={clsx(classes.drawerContainer, {
                ['active-menu']: activeMenu
            })}
            PaperProps={{
                sx: {
                    width: DRAWER_WIDTH,
          
                }
            }}
        >
            {renderSlidebar()}
            <IconButton className='icon-closed'
                onClick={() => handleOpenMenuMobile(!activeMenu)}>
                <IconSVG icon={iconCloseLight}
                    fill={COLORS.grey7}/>
            </IconButton>
        </Drawer>
    );
});

export default withStyles(styles)(DashboardDrawer);