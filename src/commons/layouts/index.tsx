import _ from 'lodash';
import * as React from 'react';
import {Outlet} from 'react-router-dom';
import DashboardAppBar from './DashboardAppBar';
import DashboardDrawer from './DashboardDrawer';

const DashboardLayout = () => {
    const [open, setOpen] = React.useState(true);
    const [search, setSearch] = React.useState<string>('');
    const [activeMenu, setActiveMenu] = React.useState<boolean>(false);
    
    const handleSearch = (e: any)=> {
        if(!_.isEmpty(e))
            setSearch(e.target.value);
        else  setSearch('');
    };

    const handleOpenMenuMobile = (status: boolean) => {
        setActiveMenu(status);
    };

    const onCloseSlidebar = () => {
        setOpen(!open);
    };
    return (
        <div>
            <DashboardDrawer
                isOpenSlidebar={open}
                activeMenu={activeMenu}
                onCloseSlidebar={onCloseSlidebar}
                handleOpenMenuMobile={handleOpenMenuMobile}
            />
            <DashboardAppBar search={search}
                handleOpenMenuMobile={handleOpenMenuMobile}
                activeMenu={activeMenu}
                handleSearch={handleSearch}/>
                
            <Outlet/>
        </div>
    );
};

export default DashboardLayout;