import {Box, Button, Divider, Icon, List, ListItemAvatar, ListItemButton, ListItemText, Popover, Typography} from '@mui/material';
import styles from './styles';
import { withStyles, WithStyles } from '@mui/styles';
import * as React from 'react';
import Text from '@helpers/Text';
import { iconChevronUp,iconChevronDown, logo, IconNotification, iconMessage } from '@constants/imageAssets';
import { Link as RouterLink, } from 'react-router-dom';
import sidebarConfig from './sidebarConfig';
import _ from 'lodash';
import Dropdown from '@helpers/Dropdown';
import iconMoreHorizontal from '@assets/icons/more_horizontal/filled.svg';
import { COLORS } from '@constants/colors';
import messageNotification1 from '@assets/images/message1.png';
import messageNotification2 from '@assets/images/message2.png';
import messageNotification3 from '@assets/images/message3.png';
import messageNotification4 from '@assets/images/message4.png';
import messageNotification5 from '@assets/images/message5.png';
import clsx from 'clsx';
import ButtonCustom from '@helpers/Button';
import { v4 as uuidv4 } from 'uuid';
import IconSVG from '@helpers/IconSVG';
import { faker } from '@faker-js/faker';
import { truncateString } from '@utils/index';
import moment from 'moment';

interface NotificationHeaderProps {
    isOpenSlidebar?: boolean,
    onCloseSlidebar?: any,
}

const MessageHeader = (props: NotificationHeaderProps & WithStyles<typeof styles>) => {
    const {classes, isOpenSlidebar, onCloseSlidebar} = props;
    const [anchorElNotification, setAnchorElNotification] = React.useState<HTMLDivElement | null>(null);

    const handleOpenDropdownMessage = (event:React.MouseEvent<HTMLDivElement>) => {
        setAnchorElNotification(event.currentTarget);
    };
    const handleCloseDropdownMessage = () => {
        setAnchorElNotification(null);
    };

    const iconNotis = 
    [messageNotification1,
        messageNotification2, 
        messageNotification3, 
        messageNotification4, 
        messageNotification5];
    const status = ['online', 'offline'];
    
    const listMessDemo = React.useMemo(()=> [
        {
            username: faker.name.firstName() + ' ' + faker.name.lastName(),
            image: faker.helpers.arrayElement(iconNotis),
            message: truncateString(faker.lorem.paragraph(1), 30),
            status: faker.helpers.arrayElement(status),
            read: faker.helpers.arrayElement(status) === 'online',
            time: moment(faker.date.future()).format('HH:mm A'),
        },
        {
            username: faker.name.firstName() + ' ' + faker.name.lastName(),
            image: faker.helpers.arrayElement(iconNotis),
            message: truncateString(faker.lorem.paragraph(1), 30),
            status: faker.helpers.arrayElement(status),
            read: faker.helpers.arrayElement(status) === 'online',
            time: moment(faker.date.future()).format('HH:mm A'),
        },
        {
            username: faker.name.firstName() + ' ' + faker.name.lastName(),
            image: faker.helpers.arrayElement(iconNotis),
            message: truncateString(faker.lorem.paragraph(1), 30),
            status: faker.helpers.arrayElement(status),
            read: faker.helpers.arrayElement(status) === 'online',
            time: moment(faker.date.future()).format('HH:mm A'),
        },
        {
            username: faker.name.firstName() + ' ' + faker.name.lastName(),
            image: faker.helpers.arrayElement(iconNotis),
            message: truncateString(faker.lorem.paragraph(1), 30),
            status: faker.helpers.arrayElement(status),
            read: faker.helpers.arrayElement(status) === 'online',
            time: moment(faker.date.future()).format('HH:mm A'),
        },
        {
            username: faker.name.firstName() + ' ' + faker.name.lastName(),
            image: faker.helpers.arrayElement(iconNotis),
            message: truncateString(faker.lorem.paragraph(1), 30),
            status: faker.helpers.arrayElement(status),
            read: faker.helpers.arrayElement(status) === 'online',
            time: moment(faker.date.future()).format('HH:mm A'),
        },
    ], []);

    const renderItem = (data: {
        username: string,
        image: string,
        message: string,
        read: boolean,
        status: string,
        time: string,
    }, idx: number) => {
        const {image, username, message, read, status, time}= data;
        return (
            <Box component={'div'}
                key={uuidv4()}>
                <ListItemButton
                    className={classes.listItemButton}>
                    <Box className={classes.itemNoti}>
                        <Box component={'div'}
                            className={classes.dFlex}>
                            <ListItemAvatar
                                className={classes.itemAvatar}
                            >
                                <img src={image}
                                    width={48}
                                    height={48}/> 
                                <Box
                                    component={'div'}
                                    className={clsx({
                                        [classes.statusOnline]: status === 'online',
                                        [classes.statusOffline]: status === 'offline',
                                    })}
                                />                                    
                            </ListItemAvatar>
                            <Box component={'div'}>
                                <ListItemText primary={
                                    <div>
                                        <Text base1
                                            className={classes.itemName}>{username}</Text> 
                                    </div>
                                }/>
                                <ListItemText primary={
                                    <Text bodyM1
                                        color={!read ? COLORS.grey7: COLORS.grey4}
                                        className={classes.itemUsername}>{message}</Text> 
                                }/>
                            </Box>
                        </Box>
                        <Box component={'div'}
                            className={classes.boxUnread}>
                            <Text caption1
                                color={COLORS.grey4}
                                className={classes.itemUsername}>{time}</Text>
                            <Box className={clsx({
                                [classes.dotUnread]: read === false,
                                [classes.dotRead]: read === true,
                            })}/>
                        </Box>
                    </Box>
                    
                </ListItemButton>
                {idx !== listMessDemo.length -1 && <Divider/>}
            </Box>
        );
    };

    const open = Boolean(anchorElNotification);
  
    return (
        <React.Fragment>
            <Icon
                component={'div'}
                className={classes.iconRight}
                onClick={handleOpenDropdownMessage}
            >
                <IconSVG icon={iconMessage}
                    fill={COLORS.grey4}
                    width={24}
                    height={24}/>
                <Box
                    className={classes.dotNotification}
                />
            </Icon>
            <Dropdown
                anchorEl={anchorElNotification}
                open={open}
                onClose={handleCloseDropdownMessage}
            >
                <Box
                    className={classes.dropdownList}
                >
                    <Box component={'div'}
                        className={classes.boxHeader}
                    >
                        <Text titleSB1
                            className={classes.titleDropDown}>
                    Message
                        </Text>
                        <div className={classes.boxIconMore}>
                            <img src={iconMoreHorizontal}
                                width={24}
                                height={24}/>
                        </div>
                    </Box>
                    <List>
                        { !_.isEmpty(listMessDemo) && listMessDemo.map((item, index)=> 
                            renderItem(item, index))}
                    </List>
                    <ButtonCustom color={'primary'}
                        className={classes.buttonAll}>
                    View in message center
                    </ButtonCustom>
                </Box>
            </Dropdown>
        </React.Fragment>
    );
};

export default withStyles(styles)(MessageHeader);