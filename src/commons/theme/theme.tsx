import {createTheme, responsiveFontSizes} from '@mui/material/styles';
import * as fonts from '@assets/fonts/fonts';
import { COLORS } from '@constants/colors';
const theme: any = createTheme({
    typography: {
        fontFamily: ['inter', 'sans-serif'].join(','),
    },
    palette: {},
    components: {
        MuiCssBaseline: {
            '@global': {
                '@font-face': [...fonts],
            }
        },
        MuiAppBar: {
            styleOverrides: {
                colorPrimary: {
                    backgroundColor: COLORS.white,
                }
            }
        }
    }
});

export default responsiveFontSizes(theme);