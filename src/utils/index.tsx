import moment from 'moment';
import { toast } from 'react-toastify';

export const truncateString = (string = '', maxLength = 50) => 
    string.length > maxLength 
        ? `${string.substring(0, maxLength)}…`
        : string;

export const formatMoney = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
});

export const createFakerList =( singleData: any,total = 10) => {
    return Array(total)
        .fill(null)
        .map(singleData);
};

export const generateRandomListFromANumber = (max:number, count: number) => {
    const randomBetween = (min:number, max: number) => {
        return Math.floor(Math.random()*(max-min+1)+min);
    };

    const listRandom = [];
    let currsum = 0;
    for(let i=0; i<count-1; i++) {
        listRandom[i] = randomBetween(1, max-(count-i-1)-currsum);
        currsum += listRandom[i];
    }
    listRandom[count-1] = max - currsum;
    return listRandom;
};
// eslint-disable-next-line max-len
export  const formatDate = (date: Date, commaBeforeDate = true ,commaBeforeYear = false, haveDayofWeek=true,haveDay=true,haveMonth=true,haveYear=false,haveTime=false) =>{
    const ListDateOfWeeks =['Sun','Mon','Tue','Wed','Thu','Fri','Sat',];
    const ListMonths =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    // eslint-disable-next-line max-len
    return `${haveDayofWeek ? ' '+ListDateOfWeeks[date.getDay()]:''}${commaBeforeDate ? ',':''}${haveMonth ? ' '+ ListMonths[date.getMonth()]:''}${haveDay ? ' '+date.getDate():''}${commaBeforeYear ? ',':''}${haveYear ? ' '+ date.getFullYear():''}${haveTime ? ' at '+moment(date).format('LT') :''}`;
};

export const isMobile = window.screen.availWidth < 600;

