import { combineReducers } from 'redux';
import homeReducer from './home'
const rootReducers = combineReducers({
  home: homeReducer,
})

export default rootReducers;