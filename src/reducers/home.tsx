import * as _ from 'lodash';

const initialState = {
  item: [],
  errors: {},
  status: {
    isSuccess: false,
    isLoading: true,
  }
}

const initialStatus = {
  isSuccess: false,
  isLoading: true,
}

const reducer = (state: any = initialState, action: {type: any, payload: {data?: any, error?: any}}) => {
  switch(action.type) {
    case 'FETCH_LIST': {
      return {
        ...state,
      }
    }
    case 'FETCH_LIST_SUCCESS': {
      return {
        ...state,
        status: {
          ...initialStatus,
          isLoading: false,
          isSuccess: true,
        }
      }
    }
    case 'FETCH_LIST_FAILED': {
      const {error} = action.payload;
      return {
        ...state,
        errors: error,
        status: {
          ...initialStatus,
          isLoading: false,
          isSuccess: true,
        }
      }
    }
    default: {
      return state;
    }
  }
}

export default reducer;